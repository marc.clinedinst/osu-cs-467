#!/bin/sh

HEROKU_DOCKER_IMAGE_ID=$(docker inspect registry.heroku.com/${HEROKU_APPLICATION_NAME}/web --format={{.Id}})
REQUEST_DATA='{"updates": [{"docker_image": "'"$HEROKU_DOCKER_IMAGE_ID"'", "type": "web"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APPLICATION_NAME/formation \
  -d "${REQUEST_DATA}" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer ${HEROKU_AUTH_TOKEN}" \
  -H "Content-Type: application/json" \
