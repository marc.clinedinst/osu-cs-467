#!/bin/sh

docker-compose exec server python manage.py flush --noinput
docker-compose exec server python manage.py loaddata users
docker-compose exec server python manage.py loaddata animal_dispositions
docker-compose exec server python manage.py loaddata animals
docker-compose exec server python manage.py loaddata animal_photographs
docker-compose exec server python manage.py loaddata animal_status_updates

npx cypress run --spec "cypress/integration/*.spec.js"