describe("Login Dialog Errors", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("shows errors for all fields when the user clicks login without providing data", () => {
    cy
      .get("[data-testid='banner-login-button']")
      .click()
      .get("[data-testid='login-dialog-login-button']")
      .click()
      .get("p.Mui-error")
      .then(errors => {
        expect(errors[0]).to.contain.text("Username is required.");
        expect(errors[1]).to.contain.text("Password is required.");
      })
  });

  it("shows an error when the user passes invalid credentials", () => {
    const username = "not_a_real_username";
    const password = "not_a_real_password"

    cy
      .get("[data-testid='banner-login-button']")
      .click()
      .get("input[name='username']")
      .type(username)
      .get("input[name='password']")
      .type(password)
      .get("[data-testid='login-dialog-login-button']")
      .click()
      .get("[data-testid='alert-message']")
      .should("be.visible")
      .contains("Please enter valid credentials");
  });
});