describe("Animal Search", () => {
  beforeEach(() => {
    cy
      .visit("/")
      .get("[data-testid='banner-login-button']")
      .click()
      .get("input[name='username']")
      .type("pet_seeker")
      .get("input[name='password']")
      .type("PetSeekerPassword1!")
      .get("[data-testid='login-dialog-login-button']")
      .click()
      .url()
      .should("contain", "/search")
  });

  describe("browser tab", () => {
    it("has the expected title in the browser tab", () => {
      cy
        .get("title")
        .contains("Paw Swipe");
    });
  });

  describe("navigation bar", () => {
    it("has the expected title in the navigation bar", () => {
      cy
        .get("[data-testid='navbar-title']")
        .should("be.visible")
        .contains("Paw Swipe");
    });

    it("has a filter menu in the navigation bar", () => {
      cy
        .get("[data-testid='navbar-filter']")
        .should("be.visible");
    })

    it("has a hamburger menu in the navigation bar", () => {
      cy
        .get("[data-testid='navbar-hamburger-menu']")
        .should("be.visible");
    });
  });

  describe("filters modal", () => {
    [
      {
        expectedNames: ["Tricia", "Huntress", "Hilda", "Hooligan"],
        kind: "Cat"
      },
      {
        expectedNames: ["Meatball", "Bear", "Daisy", "Chance", "Duke", "Tiny", "Fancy"],
        kind: "Dog"
      },
      {
        expectedNames: ["Rogue", "Daffodil", "Cowboy", "Diva"],
        kind: "Other"
      }
    ].forEach(configuration => {
      it(`can filter by kind - "${configuration.kind}"`, () => {
        cy
          .get("[data-testid='navbar-filter']")
          .click()
          .get("[aria-labelledby='kind']")
          .click()
          .get(`[data-value="${configuration.kind}"]`)
          .click()
          .get("[data-testid='search-dialog-search-button']")
          .click()
          .get("[data-testid='animal-card-name']")
          .should(names => {
            for (let index = 0; index < names.length; index++) {
              expect(names.eq(index)).to.contain(configuration.expectedNames[index])
            }
          });
      });
    });

    it("can filter by breed", () => {
      cy
        .get("[data-testid='navbar-filter']")
        .click()
        .get("[data-testid='breed-field']")
        .type("Terrier")
        .get("[data-testid='search-dialog-search-button']")
        .click()
        .get("[data-testid='animal-card-name']")
        .should(names => {
          expect(names).to.have.length(1);
          expect(names.eq(0)).to.contain("Meatball");
        });
    });

    [
      {
        expectedNames: ["Daisy", "Tricia", "Daffodil", "Hilda", "Huntress", "Diva", "Fancy"],
        gender: "Female"
      },
      {
        expectedNames: ["Meatball", "Bear", "Rogue", "Chance", "Duke", "Tiny", "Cowboy", "Hooligan"],
        gender: "Male"
      },
    ].forEach(configuration => {
      it(`can filter by gender - "${configuration.gender}"`, () => {
        cy
          .get("[data-testid='navbar-filter']")
          .click()
          .get("[aria-labelledby='gender']")
          .click()
          .get(`[data-value="${configuration.gender}"]`)
          .click()
          .get("[data-testid='search-dialog-search-button']")
          .click()
          .get("[data-testid='animal-card-name']")
          .should(names => {
            for (let index = 0; index < names.length; index++) {
              expect(names.eq(index)).to.contain(configuration.expectedNames[index])
            }
          });
      });
    });

    [
      {
        expectedNames: ["Meatball", "Tiny"],
        status: "Adopted"
      },
      {
        expectedNames: ["Bear", "Daisy", "Rogue", "Chance", "Daffodil", "Huntress", "Cowboy", "Hooligan", "Diva", "Fancy"],
        status: "Available"
      },
      {
        expectedNames: ["Tricia", "Duke"],
        status: "In Foster"
      },
      {
        expectedNames: ["Hilda"],
        status: "Pending"
      }
    ].forEach(configuration => {
      it(`can filter by status - "${configuration.status}"`, () => {
        cy
          .get("[data-testid='navbar-filter']")
          .click()
          .get("[aria-labelledby='status']")
          .click()
          .get(`[data-value="${configuration.status}"]`)
          .click()
          .get("[data-testid='search-dialog-search-button']")
          .click()
          .get("[data-testid='animal-card-name']")
          .should(names => {
            for (let index = 0; index < names.length; index++) {
              expect(names.eq(index)).to.contain(configuration.expectedNames[index])
            }
          });
      });
    });
  });
});