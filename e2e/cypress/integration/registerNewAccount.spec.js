import faker from "faker";

describe("Register New Account", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("new user can register an account", () => {
    const email = faker.internet.email();
    const username = faker.internet.userName();
    const password = faker.internet.password();

    cy
      .get("[data-testid='banner-register-button']")
      .click()
      .get("input[name='email']")
      .type(email)
      .get("input[name='username']")
      .type(username)
      .get("input[name='password']")
      .type(password)
      .get("input[name='confirmPassword']")
      .type(password)
      .get("[data-testid='register-dialog-register-button']")
      .click()
      .url()
      .should("contain", "/search")
      .get("[data-testid='navbar-hamburger-menu']")
      .should("be.visible")
      .click()
      .get("[data-testid='nav-drawer-logout']")
      .should("be.visible")
      .click()
      .get("[data-testid='banner-login-button']")
      .should("be.visible")
      .contains("Login");
  });
});