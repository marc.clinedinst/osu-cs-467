describe("Animal Favorites", () => {
  beforeEach(() => {
    cy
      .visit("/")
      .get("[data-testid='banner-login-button']")
      .click()
      .get("input[name='username']")
      .type("pet_seeker")
      .get("input[name='password']")
      .type("PetSeekerPassword1!")
      .get("[data-testid='login-dialog-login-button']")
      .click()
      .url()
      .should("contain", "/search")
  });

  it("allows the user to favorite and unfavorite an animal", () => {
    cy
      .get("[data-testid='navbar-hamburger-menu']")
      .click()
      .get("[data-testid='nav-drawer-my-favorites']")
      .click()
      .get("[data-testid='favorite-list-no-results-alert']")
      .should("contain", "There are no animals in your Favorite List.")
      .should("contain", "Search for animals and favorite the ones you want to adopt.")
      .visit("/search")
      .get("[data-testid='animal-card-view']")
      .first()
      .click()
      .get("[data-testid='favorite-button']")
      .click()
      .get("[data-testid='navbar-hamburger-menu']")
      .click()
      .get("[data-testid='nav-drawer-my-favorites']")
      .click()
      .get("[data-testid='favorite-item-name']")
      .should("contain", "Meatball")
      .get("[data-testid='favorite-item-toggle-button']")
      .click()
      .get("[data-testid='favorite-list-no-results-alert']")
      .should("contain", "There are no animals in your Favorite List.")
      .should("contain", "Search for animals and favorite the ones you want to adopt.")
  });
});