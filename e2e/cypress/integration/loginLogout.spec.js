describe("Login and Logout", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  [
    {username: 'admin', password: 'AdminPassword1!'},
    {username: 'site_admin', password: 'SiteAdminPassword1!'},
    {username: 'pet_seeker', password: 'PetSeekerPassword1!'}
  ].forEach(credentials => {
    it(`${credentials.username} can log in and log out`, () => {
      cy
        .get("[data-testid='banner-login-button']")
        .click()
        .get("input[name='username']")
        .type(credentials.username)
        .get("input[name='password']")
        .type(credentials.password)
        .get("[data-testid='login-dialog-login-button']")
        .click()
        .url()
        .should("contain", "/search")
        .get("[data-testid='navbar-hamburger-menu']")
        .should("be.visible")
        .click()
        .get("[data-testid='nav-drawer-logout']")
        .should("be.visible")
        .click()
        .get("[data-testid='banner-login-button']")
        .should("be.visible")
        .contains("Login");
    });
  });
});