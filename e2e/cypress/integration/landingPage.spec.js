describe("Landing Page", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  describe("browser tab", () => {
    it("has the expected title in the browser tab", () => {
      cy
        .get("title")
        .contains("Paw Swipe");
    });
  });

  describe("navigation bar", () => {
    it("has a hamburger menu in the navigation bar", () => {
      cy
        .get("[data-testid='navbar-hamburger-menu']")
        .should("be.visible");
    });

    it("has the expected title in the navigation bar", () => {
      cy
        .get("[data-testid='navbar-title']")
        .should("be.visible")
        .contains("Paw Swipe");
    });
  });

  describe("banner", () => {
    it("has the expected title", () => {
      cy
        .get("[data-testid='banner-title']")
        .should("be.visible")
        .contains("Paw Swipe");
    });

    it("has the expected subtitle", () => {
      cy
        .get("[data-testid='banner-subtitle']")
        .should("be.visible")
        .contains("A dating app for animal adpotion");
    });

    it("has a 'Login' button that opens a 'Login' dialog", () => {
      cy
        .get("[data-testid='banner-login-button']")
        .should("be.visible")
        .contains("Login")
        .click()
        .get("[data-testid='login-dialog-header']")
        .should("be.visible")
        .contains("Login")
        .get("input[name='username']")
        .should("be.visible")
        .get("input[name='password']")
        .should("be.visible")
        .get("[data-testid='login-dialog-login-button']")
        .should("be.visible")
        .contains("Login");
    });

    it("has a 'I\'m New Here' button that opens the 'Register' dialog", () => {
      cy
        .get("[data-testid='banner-register-button']")
        .should("be.visible")
        .contains("I'm New Here")
        .click()
        .get("[data-testid='register-dialog-header']")
        .should("be.visible")
        .contains("Register")
        .get("input[name='username']")
        .should("be.visible")
        .get("input[name='password']")
        .should("be.visible")
        .get("[data-testid='register-dialog-register-button']")
        .should("be.visible")
        .contains("Register");
    });
  });
});