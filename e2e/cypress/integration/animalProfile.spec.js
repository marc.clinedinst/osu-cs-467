describe("Animal Profile", () => {
  beforeEach(() => {
    cy
      .visit("/")
      .get("[data-testid='banner-login-button']")
      .click()
      .get("input[name='username']")
      .type("pet_seeker")
      .get("input[name='password']")
      .type("PetSeekerPassword1!")
      .get("[data-testid='login-dialog-login-button']")
      .click()
      .url()
      .should("contain", "/search")
  });

  it("displays an animal's information", () => {
    cy
      .get("[data-testid='animal-card-view']")
      .first()
      .click()
      .get("[data-testid='animal-profile-name']")
      .should("contain", "Meatball")
      .get("[data-testid='animal-profile-kind-breed-gender-dob']")
      .should("contain", "Male, Terrier Dog")
      .get("[data-testid='animal-profile-dob']")
      .should("contain", "about 3 years old")
      .get("[data-testid='animal-profile-description']")
      .should("contain", "Meatball is a friendly dog who loves treats and naps.")
  });
});