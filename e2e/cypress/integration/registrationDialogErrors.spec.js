import faker from "faker";

describe("Register Dialog Errors", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("shows errors for all fields when the user clicks register without providing data", () => {
    cy
      .get("[data-testid='banner-register-button']")
      .click()
      .get("[data-testid='register-dialog-register-button']")
      .click()
      .get("p.Mui-error")
      .then(errors => {
        expect(errors[0]).to.contain.text("Email is required.");
        expect(errors[1]).to.contain.text("Username is required.");
        expect(errors[2]).to.contain.text("Password is required.");
        expect(errors[3]).to.contain.text("Confirm Password is required.");
      })
  });

  it("shows an error when the email address is invalid", () => {
    const email = "invalid_email@email";
    const username = faker.internet.userName();
    const password = faker.internet.password();

    cy
      .get("[data-testid='banner-register-button']")
      .click()
      .get("input[name='email']")
      .type(email)
      .get("input[name='username']")
      .type(username)
      .get("input[name='password']")
      .type(password)
      .get("input[name='confirmPassword']")
      .type(password)
      .get("[data-testid='register-dialog-register-button']")
      .click()
      .get("[data-testid='alert-message']")
      .should("be.visible")
      .contains(`The provided email '${email}' is invalid.`);
  });

  it("shows an error when the username already exists", () => {
    const email = faker.internet.email();
    const username = "admin";
    const password = faker.internet.password();

    cy
      .get("[data-testid='banner-register-button']")
      .click()
      .get("input[name='email']")
      .type(email)
      .get("input[name='username']")
      .type(username)
      .get("input[name='password']")
      .type(password)
      .get("input[name='confirmPassword']")
      .type(password)
      .get("[data-testid='register-dialog-register-button']")
      .click()
      .get("[data-testid='alert-message']")
      .should("be.visible")
      .contains("A user with the username 'admin' already exists.");
  });

  it("shows an error message when passwords do not message", () => {
    const email = faker.internet.email();
    const username = faker.internet.userName();
    const password = faker.internet.password();

    cy
      .get("[data-testid='banner-register-button']")
      .click()
      .get("input[name='email']")
      .type(email)
      .get("input[name='username']")
      .type(username)
      .get("input[name='password']")
      .type(password)
      .get("input[name='confirmPassword']")
      .type("does_not_match")
      .get("[data-testid='register-dialog-register-button']")
      .click()
      .get("[data-testid='alert-message']")
      .should("be.visible")
      .contains("Passwords must match.");
  });
});