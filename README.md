# Overview
This repository contains the code for the final project for OSU 467 for the following students:

- Marc Clinedinst
- Jose R Garay Jr.
- Robert Jones

Specifically, it contains the implementation of a [pet adoption app](https://tinyurl.com/y6lxjcgq)
web application built using Django, PostgreSQL, and React.

* [Team Standards](https://docs.google.com/document/d/1bvttD6QYOHAawht_2kFCpNQFVwMQWq58LYZdLsa8GWI/edit)
* [Project Plan](https://docs.google.com/document/d/1RiyoMTi6Ss7nUopjz02KuzF4ucnJd9AwhMz1auZUCb0/edit)
* [Mid-point Check](https://docs.google.com/document/d/1Uk5JdEq2M82FngLqPnWRwBGkNMmrKkwwRUcFlYSIMWE/edit)
* [Demonstrate Project](https://docs.google.com/document/d/1BdzBNie8lWNQrcHIiUzZkIu_hXlSY3iEff2owrSwJ6U/edit)
* [Final Report](https://docs.google.com/document/d/1us-D-zbsV0v9TNyXmxfSC8lkf55Kt0oma4juTDDOKss/edit)


# Contents
- [Local Development](#local-development)
  - [Software Requirements](#software-requirements)
  - [Starting the Development Environment](#starting-the-development-environment)
  - [Making Code Changes](#making-code-changes)
  - [Client](#client)
    - [Executing npm Scripts](#executing-npm-scripts)
    - [Running Client Tests](#running-client-tests)
    - [Code Formatting and Linting](#code-formatting-and-linting)
  - [Server](#server)
    - [Executing Django Commands](#executing-django-commands)
    - [Creating a Super User](#creating-a-super-user)
    - [Running Tests](#running-server-tests)
    - [Linting](#linting)
    - [Automatic Code Formatting](#automatic-code-formatting)
    - [Accessing the Database](#accessing-the-database)
  - [End-to-End Tests](#end-to-end-tests)


# Local Development
## Software Requirements
In order to run this application locally, you must have the following software installed on your
computer:

* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [Node.js](https://nodejs.org/en/)

## Setting Up Required Environment Variables
In order for file uploads to work as expected during local development, you need to create an `.env.dev`
file within the `server` directory (the path should be `server/.env.dev`). Populate it with the following
information:

```
AWS_ACCESS_KEY_ID=aws_access_key_id_goes_here
AWS_CUSTOM_DOMAIN=aws_custom_domain_goes_here
AWS_SECRET_ACCESS_KEY=aws_secret_access_key_goes_here
AWS_STORAGE_BUCKET_NAME=aws_storage_bucket_name_goes_here
SENDGRID_API_KEY=sendgrid_api_key_goes_here
```

You can find the actual values for each of the environment variables within the CI/CD settings on
[GitLab](https://gitlab.com/marc.clinedinst/osu-cs-467/-/settings/ci_cd).

## Starting the Development Environment
This project largely relies on [Docker](https://docs.docker.com/get-started/overview/) for local 
development. Before launching the project using Docker, however, you must first install the
dependencies for the client using the following series of commands:

```
cd client
npm install
```

After the local installation of the client dependencies complete, you can launch the entire app
infrastructure by executing these commands:

```
cd ..
docker-compose up --build
```

Docker will download and install all the required software and spin up the development environment.
You will know that the `client` is ready when you see the following message:

`Compiled successfully! You can now view client in the browser.`

You will know that the `server` is ready when you se the following message:

`Watching for file changes with StatReloader.` 

Once you see these messages, you can access the following URLs:

- [http://localhost:3001/](http://localhost:3001/) - This URL displays the client.
- [http://localhost:8000/](http://localhost:8000/) - This URL displays the API root. This currently 
displays a simple "Hello, World!" message to verify that the API is working correctly.
- [http://localhost:8000/admin/](http://localhost:8000/admin/) - This URL displays the
[Django Admin](https://docs.djangoproject.com/en/3.1/ref/contrib/admin/). You will need to 
[create a super user account](#creating-a-super-user) in order to access this part of the 
application.
- [http://localhost:8000/graphql/](http://localhost:8000/graphql/) - This URL displays the
GraphiQL API browser.

## Making Code Changes
Both the client and the server have hot-reloading enabled; any changes that you make to the code
will cause the application to reload and reflect the changes.  __This does not apply to updates
to dependencies.__ If you add a new dependency to either the client or the server, you must
terminate the Docker process using CONTROL-C or the following command if you are running in 
detached mode:

`docker-compose down`

You must then re-run the build command:

`docker-compose up --build`

## Client
The core technologies used in the development of the application client are:

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [TypeScript](https://www.typescriptlang.org/)

### Executing npm Scripts
Since we are using Docker Compose for local development, you must adjust your use of
[npm scripts](https://docs.npmjs.com/cli/v6/using-npm/scripts) so that they are prefixed
with the appropriate `docker-compose` command. Normally, the command to execute the npm
test script is:

`npm test`

With Docker Compose, you must use the following command in the context of the project:

`docker-compose exec client npm test`

It is also worth noting that the Docker containers must be up to execute this command. If you
receive an error, execute the following command:

`docker-compose up --build`

### Running Client Tests
This project uses [Jest](https://jestjs.io/) for executing unit and integration tests for the
client-side code. You can execute tests with simple command line output using the command below:

`docker-compose exec client npm test`

You can execute tests and get coverage information using the following command:

`docker-compose exec client npm run test:coverage`

### Code Formatting and Linting
This project uses [eslint](https://eslint.org/) from CRA for linting. No configuration is 
necessary. Eslint warnings and errors will appear in the browser console as well as your
terminal console. Check one of the two before pushing code.

[Prettier](https://prettier.io/) is used for code formatting for this project. A prettierrc
file was created at the root of the client folder so all configuration should be uniform
among contributors assuming that prettier is installed.

__Tip:__ If using VSCode, turn on format on save for prettier to auto-format upon save.

## Server
The core technologies used in the development of the application server are:

- [Django](https://www.djangoproject.com/)
- [Django REST Framework](https://www.django-rest-framework.org/)
- [PostgreSQL](https://www.postgresql.org/)

### Executing Django Commands
Since we are using Docker Compose for local development, you must adjust your use of 
[Django commands](https://docs.djangoproject.com/en/3.1/ref/django-admin/) so that they are 
prefixed with the appropriate `docker-compose` command. Normally, the command to perform a
database migration is:

`python manage.py migrate`

With Docker Compose, you must use the following command in the context of the project:

`docker-compose exec server python manage.py migrate`

It is also worth noting that the Docker containers must be up to execute this command. If you 
receive an error, execute the following command:

`docker-compose up --build`

### Creating a Super User
In order to access the [Django Admin](https://docs.djangoproject.com/en/3.1/ref/contrib/admin/)
site available at [http://localhost:8000/admin/](http://localhost:8000/admin/), you will need to
create a super user account with the following commands:

`docker-compose exec server python manage.py createsuperuser`

You will be prompted to enter a username, email address, password, and password confirmation. If
you see a success message, you should be able to log into the Django Admin.
 
### Running Server Tests
This project uses [pytest](https://docs.pytest.org/en/stable/) for executing unit and integration
tests for the server-side code. We also use the following pytest plugins for additional 
functionality:

- [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/): Provides utilities for measuring 
test code coverage.
- [pytest-django](https://pytest-django.readthedocs.io/en/latest/): Provides utilities for testing
Django applications
with pytest.
- [pytest-html](https://pypi.org/project/pytest-html/): Provides utilities for generating HTML test
reports after test runs complete.
  
Configuration for pytest is available in the `server/pytest.ini` file. Below are some example 
commands for executing tests in a variety of ways.
  
__Simple command-line output:__

`docker-compose exec server pytest -p no:warnings`

__Simple command-line output with HTML report:__

`docker-compose exec server pytest -p no:warnings --html=reports/report.html`

With the above command, the HTML report will be accessible at `/server/reports/report.html`.

__Simple command-line output with coverage details:__

`docker-compose exec server pytest -p no:warnings --cov=.`

__Simple command-line output with coverage details and HTML report:__

`docker-compose exec server pytest -p no:warnings --cov=. --cov-report html`

With the above command, the HTML report will be accessible at `/server/htmlcov/index.html`

### Linting
This project uses [flake8](https://flake8.pycqa.org/en/latest/) for linting and style enforcement. 
Configuration for flake8 is stored in the `server/setup.cfg` file. To lint your code, which is 
recommended before pushing any changes, run the following command:

`docker-compose exec server flake8 .`

Resolve any errors identified by the linter before pushing your code.

### Automatic Code Formatting
This project uses [black](https://black.readthedocs.io/en/stable/) for automatic code formatting.
To automatically format your code, which is recommended before pushing any changes, run the
following command:

`docker-compose exec server black --exclude="/(migrations|venv)/" .`

### Accessing the Database
After running `docker-compose up --build` from the root directory, you can access the database from
the command line using the following command:

`docker-compose exec database psql --username=postgres_user --dbname=postgres_db`

If you prefer to use a PostgreSQL GUI, the access information for the database is below:

- __Database__: `postgres_db`
- __Host:__ `localhost`
- __Password:__ `postgres_password`
- __Port:__ `5432`
- __User:__ `postgres_user`

## Initial Data
This project uses [Django fixtures](https://docs.djangoproject.com/en/3.1/howto/initial-data/) to
automatically populate the databasse when the project is build. This includes both users that can 
access various parts of the system.

### Users
__Super User__
This is a [super user](https://docs.djangoproject.com/en/3.1/ref/django-admin/#createsuperuser) account
that has access to all parts of the system. This account is most useful for accessing the
[administration interface](http://localhost:8000/admin/).

- `Username`: admin
- `Password`: AdminPassword1!

__Site Admin User__
This is a site admin account that has the ability to add other site admins to the site. This account also
has access to the [administration interface](http://localhost:8000/admin/), but with reduced permissions.
This user is useful for testing the administrative functionality on the front end of the web application.

- `Username`: site_admin
- `Password`: SiteAdminPassword1!

__Pet Seeker User__
This is a normal user who can access only the end-user facing portions of the website. This user is useful
for testing the front end of the web application.

- `Username`: pet_seeker
- `Password`: PetSeekerPassword1!

### Animals
When the project is first build, 15 animals are also available in the database. These can be used to test
all the animal-related functionality on the front end of the application.

## End-to-End Tests
This project uses [Cypress](https://www.cypress.io/) for end-to-end tests. In order to execute
these tests, you must first install the dependencies with these commands:

```
cd e2e
npm install
```

After successfully installing the dependencies, you can run the tests with the following sequence of commands:

```
chmod +x run-tests.sh
./run-tests.sh
```

Tests will run in the background and log results to the console. Videos of each test suite will appear in the 
`e2e/cypress/videos` directory. If a test fails, a screenshot of the failure will appear in `e2e/cypress/screenshots`.