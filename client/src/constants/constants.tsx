import AnimalEditDialogContent from 'components/common/AnimalEditDialogContent'
import FiltersDialogContent from 'components/common/FiltersDialogContent'
import LoginDialogContent from 'components/common/LoginDialogContent'
import RegistrationDialogContent from 'components/common/RegistrationDialogContent'
import {
    DialogComponents,
    DropdownOptions,
    FilterFormValues,
    MultiselectDropdownOptions,
} from 'components/common/types'

export const DEFAULT_DROPDOWN_OPTIONS: DropdownOptions = [
    { option: 'All', optionId: 'all' },
]

export const KIND_DROPDOWN_OPTIONS: DropdownOptions = [
    { option: 'Dog', optionId: 'Dog' },
    { option: 'Cat', optionId: 'Cat' },
    { option: 'Other', optionId: 'Other' },
]

export const GENDER_DROPDOWN_OPTIONS: DropdownOptions = [
    { option: 'Male', optionId: 'Male' },
    { option: 'Female', optionId: 'Female' },
]

export const DEFAULT_MULTISELECT_DROPDOWN_OPTIONS: MultiselectDropdownOptions = [
    { value: 1, label: 'Aggressive' },
    { value: 2, label: 'Bad with children' },
]

export const DATERANGE_OPTIONS: DropdownOptions = [
    { option: 'Today', optionId: '1' },
    { option: 'Last 7 days', optionId: '7' },
    { option: 'Last 30 days', optionId: '30' },
    { option: 'Last 90 days', optionId: '90' },
    { option: 'Last 365 days', optionId: '365' },
]

export const STATUS_OPTIONS: DropdownOptions = [
    { option: 'Available', optionId: 'Available' },
    { option: 'Pending', optionId: 'Pending' },
    { option: 'Adopted', optionId: 'Adopted' },
    { option: 'In Foster', optionId: 'In Foster' },
]

export const DIALOG_COMPONENTS: DialogComponents = {
    login: LoginDialogContent,
    register: RegistrationDialogContent,
    filter: FiltersDialogContent,
    animalEdit: AnimalEditDialogContent,
}

type ScreenSizes = {
    small: string
    medium: string
    large: string
    xLarge: string
}

export const SCREEN_SIZE: ScreenSizes = {
    small: '576px',
    medium: '768px',
    large: '992px',
    xLarge: '1200px',
}

export const DEFAULT_IMAGE_PAW_PRINT =
    'https://static8.depositphotos.com/1543657/1020/i/600/depositphotos_10202682-stock-photo-paw-print.jpg'

export const FILTER_FORM_DEFAULT_VALUES: FilterFormValues = {
    kind: 'all',
    breed: '',
    gender: 'all',
    daysBeforeToday: '-1',
    dispositions: [],
    status: 'all',
}
