import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { FilterFormValues } from 'components/common/types'

interface FiltersState extends FilterFormValues {}

const initialState = {
    kind: 'all',
    breed: '',
    gender: 'all',
    daysBeforeToday: '-1',
    dispositions: [],
    status: 'all',
} as FiltersState

const filtersSlice = createSlice({
    name: 'customDialog',
    initialState,
    reducers: {
        saveFilters: (state, action: PayloadAction<FilterFormValues>) =>
            action.payload,
        clearFilters: state => initialState,
    },
})

export const { saveFilters, clearFilters } = filtersSlice.actions

export default filtersSlice.reducer
