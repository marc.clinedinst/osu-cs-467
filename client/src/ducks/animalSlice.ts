import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AnimalType, StatusUpdateType } from 'gql/types'

export interface AnimalState extends AnimalType {}

const initialState = {} as AnimalState

const animalSlice = createSlice({
    name: 'animal',
    initialState,
    reducers: {
        setAnimal(state, action: PayloadAction<AnimalState>) {
            const {
                breed,
                dateOfBirth,
                dateAdded,
                description,
                dispositions,
                photographs,
                statusUpdates,
                gender,
                kind,
                name,
                status,
                id,
            } = action.payload
            state.breed = breed
            state.dateOfBirth = dateOfBirth
            state.dateAdded = dateAdded
            state.description = description
            state.dispositions = dispositions
            state.photographs = photographs
            state.statusUpdates = statusUpdates
            state.gender = gender
            state.kind = kind
            state.name = name
            state.status = status
            state.id = id
        },
        addAnimalStatusUpdate: (
            state,
            action: PayloadAction<Omit<StatusUpdateType, 'animal'>>
        ) => {
            state.statusUpdates = state.statusUpdates.concat(action.payload)
        },
        updateAnimalStatusUpdate: (
            state,
            { payload }: PayloadAction<Omit<StatusUpdateType, 'animal'>>
        ) => {
            state.statusUpdates = state.statusUpdates.map(statusUpdate =>
                statusUpdate.id === payload.id ? payload : statusUpdate
            )
        },
        deleteAnimalStatusUpdate: (state, action: PayloadAction<number>) => {
            state.statusUpdates = state.statusUpdates.filter(
                ({ id }) => id !== action.payload
            )
        },
        clearAnimal: () => initialState,
    },
})

export const {
    setAnimal,
    clearAnimal,
    addAnimalStatusUpdate,
    deleteAnimalStatusUpdate,
    updateAnimalStatusUpdate,
} = animalSlice.actions

export default animalSlice.reducer
