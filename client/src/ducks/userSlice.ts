import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AnimalType } from 'gql/types'

interface UserState {
    username: string
    id: number
    isStaff: boolean
    emailOptIn: boolean
    favorites: AnimalType[]
}

type SetUserState = UserState

const initialState = {} as UserState

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser(
            state,
            {
                payload: { username, id, isStaff, favorites, emailOptIn },
            }: PayloadAction<SetUserState>
        ) {
            state.username = username
            state.id = id
            state.isStaff = isStaff
            state.favorites = favorites
            state.emailOptIn = emailOptIn
        },
        setFavorites(state, action: PayloadAction<AnimalType[]>) {
            state.favorites = action.payload
        },
        clearUser: () => initialState,
    },
})

export const { setUser, clearUser, setFavorites } = userSlice.actions

export default userSlice.reducer
