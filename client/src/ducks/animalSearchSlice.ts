import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AnimalType } from 'gql/types'

export type AnimalSearchState = AnimalType[] | null

const initialState = null as AnimalSearchState

const animalSearchSlice = createSlice({
    name: 'animalSearch',
    initialState,
    reducers: {
        setAnimals: (state, action: PayloadAction<AnimalType[]>) =>
            action.payload,
        updateAnimalInAnimals: (state, action: PayloadAction<AnimalType>) =>
            state?.map(animal =>
                animal.id === action.payload.id ? action.payload : animal
            ),
        clearAnimals: () => initialState,
    },
})

export const {
    clearAnimals,
    setAnimals,
    updateAnimalInAnimals,
} = animalSearchSlice.actions

export default animalSearchSlice.reducer
