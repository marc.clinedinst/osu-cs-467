import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { DialogComponentTypes } from 'components/common/types'

interface CustomDialogState {
    type: DialogComponentTypes | null
    isFullscreen: boolean
}

const initialState = { type: null, isFullscreen: false } as CustomDialogState
type OpenDialogTypes = CustomDialogState | DialogComponentTypes

const customDialogSlice = createSlice({
    name: 'customDialog',
    initialState,
    reducers: {
        openDialog(state, action: PayloadAction<OpenDialogTypes>) {
            if (
                action.payload !== 'filter' &&
                action.payload !== 'login' &&
                action.payload !== 'animalEdit' &&
                action.payload !== 'register'
            )
                return action.payload
            else state.type = action.payload
        },
        closeDialog: state => initialState,
    },
})

export const { openDialog, closeDialog } = customDialogSlice.actions

export default customDialogSlice.reducer
