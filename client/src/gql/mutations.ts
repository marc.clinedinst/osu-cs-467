import { gql } from '@apollo/client'

const USER = `
    user {
        id
        username
        isStaff
        emailOptIn
        favoriteAnimals {
            id
            breed
            dateAdded
            dateOfBirth
            description
            dispositions {
                id
                description
            }
            gender
            kind
            name
            status
            photographs {
                id
                url
            }
            statusUpdates {
                id
                created
                statusText
            }
        }
    }
`

export const TOKEN_AUTH = gql`
    mutation tokenAuth($username: String!, $password: String!) {
        tokenAuth(username: $username, password: $password) {
            token
            payload
            refreshExpiresIn
            ${USER}
        }
    }
`

export const REGISTER_USER = gql`
    mutation registerUser(
        $confirmPassword: String!
        $email: String!
        $password: String!
        $username: String!
    ) {
        registerUser(
            confirmPassword: $confirmPassword
            email: $email
            password: $password
            username: $username
        ) {
            ${USER}
            token
        }
    }
`

export const UPDATE_ANIMAL = gql`
    mutation updateAnimal(
        $breed: String!
        $dateOfBirth: Date!
        $description: String!
        $id: ID!
        $dispositions: [ID]!
        $gender: String!
        $kind: String!
        $name: String!
        $status: String!
    ) {
        updateAnimal(
            breed: $breed
            dateOfBirth: $dateOfBirth
            description: $description
            dispositions: $dispositions
            gender: $gender
            kind: $kind
            name: $name
            status: $status
            id: $id
        ) {
            animal {
                id
                breed
                dateAdded
                dateOfBirth
                description
                dispositions {
                    id
                    description
                }
                gender
                kind
                name
                status
                photographs {
                    id
                    url
                }
                statusUpdates {
                    id
                    created
                    statusText
                }
            }
        }
    }
`
export const CREATE_ANIMAL = gql`
    mutation createAnimal(
        $breed: String!
        $dateOfBirth: Date!
        $description: String!
        $dispositions: [ID]!
        $gender: String!
        $kind: String!
        $name: String!
        $status: String!
    ) {
        createAnimal(
            breed: $breed
            dateOfBirth: $dateOfBirth
            description: $description
            dispositions: $dispositions
            gender: $gender
            kind: $kind
            name: $name
            status: $status
        ) {
            animal {
                id
                breed
                dateAdded
                dateOfBirth
                description
                dispositions {
                    id
                    description
                }
                gender
                kind
                name
                status
                photographs {
                    id
                    url
                }
                statusUpdates {
                    id
                    created
                    statusText
                }
            }
        }
    }
`

export const UPLOAD_ANIMAL_PHOTOGRAPH = gql`
    mutation uploadAnimalPhotograph($file: Upload!, $id: ID!) {
        uploadAnimalPhotograph(file: $file, id: $id) {
            url
        }
    }
`
export const DELETE_ANIMAL = gql`
    mutation deleteAnimal($id: ID!) {
        deleteAnimal(id: $id) {
            id
        }
    }
`

export const DELETE_ANIMAL_PHOTOGRAPH = gql`
    mutation deleteAnimalPhotograph($id: ID!) {
        deleteAnimalPhotograph(id: $id) {
            id
        }
    }
`

export const TOGGLE_FAVORITE_ANIMAL = gql`
    mutation ToggleFavoriteAnimal($id: ID!) {
        toggleFavoriteAnimal(id: $id) {
            id
        }
    }
`

export const ADD_ANIMAL_STATUS_UPDATE = gql`
    mutation addAnimalStatusUpdate($id: ID!, $statusText: String!) {
        addAnimalStatusUpdate(id: $id, statusText: $statusText) {
            statusUpdate {
                created
                id
                statusText
            }
        }
    }
`
export const DELETE_ANIMAL_STATUS_UPDATE = gql`
    mutation deleteAnimalStatusUpdate($id: ID!) {
        deleteAnimalStatusUpdate(id: $id) {
            id
        }
    }
`

export const UPDATE_ANIMAL_STATUS_UPDATE = gql`
    mutation updateAnimalStatusUpdate($id: ID!, $statusText: String!) {
        updateAnimalStatusUpdate(id: $id, statusText: $statusText) {
            statusUpdate {
                created
                id
                statusText
                animal {
                    id
                    breed
                    dateAdded
                    dateOfBirth
                    description
                    dispositions {
                        id
                        description
                    }
                    gender
                    kind
                    name
                    status
                    photographs {
                        id
                        url
                    }
                    statusUpdates {
                        id
                        created
                        statusText
                    }
                }
            }
        }
    }
`

export const TOGGLE_EMAIL_OPT_IN = gql`
    mutation ToggleEmailOptIn {
        toggleEmailOptIn {
            optedIn
        }
    }
`
