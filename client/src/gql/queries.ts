import { gql } from '@apollo/client'

export const USER_BY_USERNAME = gql`
    query userByToken($token: String!) {
        userByToken(token: $token) {
            id
            username
            isStaff
            emailOptIn
            favoriteAnimals {
                id
                breed
                dateAdded
                dateOfBirth
                description
                dispositions {
                    id
                    description
                }
                gender
                kind
                name
                status
                photographs {
                    id
                    url
                }
                statusUpdates {
                    id
                    created
                    statusText
                }
            }
        }
    }
`

export const ANIMALS = gql`
    query GetAnimals(
        $breed: String
        $daysBeforeToday: Int
        $dispositions: [ID]
        $gender: String
        $kind: String
        $status: String
    ) {
        animals(
            breed: $breed
            daysBeforeToday: $daysBeforeToday
            dispositions: $dispositions
            gender: $gender
            kind: $kind
            status: $status
        ) {
            breed
            id
            name
            dateAdded
            dateOfBirth
            description
            dispositions {
                id
                description
            }
            gender
            kind
            status
            statusUpdates {
                id
                statusText
                created
            }
            photographs {
                id
                url
            }
        }
    }
`

export const ANIMAL_BY_ID = gql`
    query animalById($id: ID!) {
        animalById(id: $id) {
            id
            breed
            dateAdded
            dateOfBirth
            description
            dispositions {
                id
                description
            }
            gender
            kind
            name
            status
            statusUpdates {
                id
                statusText
                created
            }
            photographs {
                id
                url
            }
        }
    }
`

export const ANIMAL_DISPOSITIONS = gql`
    query animalDispositions {
        animalDispositions {
            id
            description
        }
    }
`
export const FAVORITE_ANIMALS = gql`
    query GetFavoriteAnimals {
        favoriteAnimals {
            breed
            id
            name
            dateAdded
            dateOfBirth
            description
            dispositions {
                id
                description
            }
            gender
            kind
            photographs {
                id
                url
            }
            status
            statusUpdates {
                id
                statusText
                created
            }
        }
    }
`
