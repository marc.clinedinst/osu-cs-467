// ******************************************
//              Database Models
// ******************************************

interface User {
    id: number
    username: string
    isStaff: boolean
    isSuperuser: boolean
    firstName: string
    lastName: string
    email: string
    isActive: boolean
    favoriteAnimals: AnimalType[]
    emailOptIn: boolean
}

export type StatusType = 'Adopted' | 'Available' | 'In Foster' | 'Pending'
export type KindType = 'Cat' | 'Dog' | 'Other'
export type DispositionType = {
    id: number
    description: string
}
export type DispositionResponse = {
    animalDispositions: DispositionType[]
}
export type GenderType = 'Male' | 'Female'

export type PhotographType = {
    id: number
    url: string
    animal: AnimalType
}

export type StatusUpdateType = {
    id: number
    animal: AnimalType
    created: string
    statusText: string
}

export interface AnimalType {
    id: number
    breed: string
    dateAdded: string
    dateOfBirth: string
    description: string
    gender: GenderType
    dispositions: DispositionType[]
    kind: KindType
    name: string
    status: StatusType
    photographs: Omit<PhotographType, 'animal'>[]
    statusUpdates: Omit<StatusUpdateType, 'animal'>[]
    favoriteAnimals: User[]
}

export type DecodedToken = {
    exp: number
    origIat: number
    username: string
}

// ******************************************
//             Mutations
// ******************************************
export type TokenAuthOutput = {
    tokenAuth: {
        token: string
        payload: DecodedToken
        refreshExpiresIn: number
        user: User
    }
}

export type RegisterUserType = {
    registerUser: {
        user: User
        token: string
    }
}

export interface AnimalsResponse {
    animals: AnimalType[]
}

export interface AnimalByIdResponse {
    animalById: AnimalType
}

export interface CreateAnimalInput
    extends Omit<
        AnimalType,
        | 'id'
        | 'photographs'
        | 'statusUpdates'
        | 'dateAdded'
        | 'dispositions'
        | 'favoriteAnimals'
    > {
    dispositions: number[]
}

export interface CreateAnimalResponse {
    createAnimal: {
        animal: AnimalType
    }
}

export interface UploadAnimalInput {
    file: any
    id: number
}

export interface UploadAnimalResponse {
    url: string
}

export interface AnimalsInput {
    breed?: string
    daysBeforeToday?: number
    dispositions?: number[]
    gender?: string
    kind?: string
    status?: string
}
export interface AddAnimalStatusUpdateResponse {
    addAnimalStatusUpdate: {
        statusUpdate: Omit<StatusUpdateType, 'animal'>
    }
}

export interface AddAnimalStatusUpdateInput {
    id: number
    statusText: string
}

export interface DeleteAnimalStatusUpdateInput {
    id: number
}

export interface DeleteAnimalStatusUpdateResponse {
    deleteAnimalStatusUpdate: {
        id: number
    }
}
export interface UpdateAnimalStatusUpdateInput {
    id: number
    statusText: string
}

export interface UpdateAnimalStatusUpdateResponse {
    updateAnimalStatusUpdate: {
        statusUpdate: StatusUpdateType
    }
}

export interface UpdateAnimalInput
    extends Omit<
        AnimalType,
        | 'dateAdded'
        | 'photographs'
        | 'statusUpdates'
        | 'favoriteAnimals'
        | 'dispositions'
    > {
    dispositions: number[]
}

export interface UpdateAnimalResponse extends AnimalType {}

export interface DeleteAnimalPhotographInput {
    id: number
}
export interface DeleteAnimalPhotographResponse {
    id: number
}

// ******************************************
//                  Queries
// ******************************************
export interface UserByTokenInput {
    token: string
}

export interface UserByTokenOutput {
    userByToken: User
}

export interface FavoriteAnimalsResponse {
    favoriteAnimals: AnimalType[]
}

export interface ToggleFavoriteAnimalReponse {
    toggleFavoriteAnimal: {
        id: Number
    }
}
