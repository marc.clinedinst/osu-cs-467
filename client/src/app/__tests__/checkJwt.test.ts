import checkJwt from 'app/checkJwt'
import store from 'app/store'
import { clearUser } from 'ducks/userSlice'
import client from 'app/client'
jest.mock('app/client')
const mockQuery = client.query as jest.MockedFunction<typeof client.query>

describe('checkJwt Tests', () => {
    afterEach(() => {
        store.dispatch(clearUser())
        jest.unmock('app/client')
    })
    test('testing checkJwt function', () => {
        global.localStorage.setItem(
            'jwtToken',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjEyMDUwNTYzLCJvcmlnSWF0IjoxNjEyMDUwMjYzfQ.OX93RjZ0wmP3M6c2xGgjJTxRk6sdijrgTU8xkh57vps'
        )
        mockQuery.mockReturnValue(
            new Promise(resolve => {
                resolve({
                    networkStatus: 7,
                    loading: false,
                    data: {
                        userByToken: {
                            username: 'test',
                            id: 1,
                            isStaff: false,
                        },
                    },
                })
            })
        )
        checkJwt()
        expect(mockQuery).toBeCalled()
        global.localStorage.clear()
    })

    test('testing checkJwt function without a jwt token', () => {
        checkJwt()
        expect(store.getState().user.username).toBe(undefined)
    })
})
