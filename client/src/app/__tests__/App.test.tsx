import React from 'react'
import { render, screen, act } from 'app/testing-utils'
import App from 'app/App'
import store from 'app/store'
import { clearUser, setUser } from 'ducks/userSlice'
import history from 'app/history'

const mockUser = {
    username: 'test',
    id: 1,
    isStaff: false,
    favorites: [],
    emailOptIn: false,
}

describe('App Component Tests', () => {
    beforeEach(() => render(<App />))
    afterEach(() => store.dispatch(clearUser()))
    test('render root App component and check for landing page components', () => {
        const app = screen.getByTestId('app')
        const navbar = screen.getByTestId('navbar')
        const banner = screen.getByTestId('banner')
        expect(app).toBeInTheDocument()
        expect(navbar).toBeInTheDocument()
        expect(banner).toBeInTheDocument()
    })

    test('render App component with user and check for redirect to the animal search page', () => {
        store.dispatch(setUser(mockUser))
        const app = screen.getByTestId('app')
        const navbar = screen.getByTestId('navbar')
        const animalSearchContainer = screen.getByTestId(
            'animal-search-container'
        )
        expect(app).toBeInTheDocument()
        expect(navbar).toBeInTheDocument()
        expect(animalSearchContainer).toBeInTheDocument()
    })

    test('try to access search route while not logged in.', async () => {
        history.push('/search')
        const animalSearchContainer = screen.queryByTestId(
            'animal-search-container'
        )
        expect(animalSearchContainer).not.toBeInTheDocument()
    })

    test('Non-admin user trying to access admin-only route.', async () => {
        store.dispatch(setUser(mockUser))
        history.push('/add-animal')
        expect(screen.queryByText('Restricted access')).toBeInTheDocument()
    })
})
