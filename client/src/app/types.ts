import React from 'react'
import { queries, RenderOptions, RenderResult } from '@testing-library/react'
import rootReducer from 'app/rootReducer'
import { MockedResponse } from '@apollo/client/testing'
import store from './store'

export type RootState = ReturnType<typeof rootReducer>

export type WrapperOpts =
    | Pick<
          RenderOptions<typeof queries>,
          'container' | 'baseElement' | 'hydrate' | 'wrapper'
      >
    | undefined

export type CustomRenderer = (
    ui: React.ReactElement,
    options?: WrapperOpts
) => RenderResult

export type CustomMockRenderer = (
    ui: React.ReactElement,
    mocks: MockedResponse<Record<string, any>>[],
    options?: WrapperOpts
) => RenderResult

export type AppDispatch = typeof store.dispatch
