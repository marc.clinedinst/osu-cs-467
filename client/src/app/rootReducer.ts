import { combineReducers } from 'redux'
import customDialog from 'ducks/customDialogSlice'
import user from 'ducks/userSlice'
import notification from 'ducks/notificationSlice'
import animal from 'ducks/animalSlice'
import animals from 'ducks/animalSearchSlice'
import filters from 'ducks/filtersSlice'

const rootReducer = combineReducers({
    animal,
    animals,
    customDialog,
    filters,
    notification,
    user,
})

export default rootReducer
