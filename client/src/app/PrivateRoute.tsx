import React from 'react'
import { Route, Redirect, RouteProps } from 'react-router-dom'
import { useSelector } from 'app/store'

const PrivateRoute = (props: RouteProps & { adminOnly?: boolean }) => {
    const { component: Component, adminOnly = false, ...rest } = props
    const { username, isStaff } = useSelector(state => state.user)

    // Check if the user is logged in, the route is admin access only, and the user is staff.
    if (username && adminOnly && !isStaff) return <div>Restricted access</div>

    return (
        <Route
            {...rest}
            render={props =>
                username && Component ? (
                    <Component {...props} />
                ) : (
                    <Redirect to='/' />
                )
            }
        />
    )
}

export default PrivateRoute
