import { setUser } from 'ducks/userSlice'
import { USER_BY_USERNAME } from 'gql/queries'
import { UserByTokenInput, UserByTokenOutput } from 'gql/types'
import client from 'app/client'
import store from './store'

const checkJwt = async () => {
    const token = localStorage.getItem('jwtToken')
    if (token) {
        try {
            // Fetch user data.
            const { data } = await client.query<
                UserByTokenOutput,
                UserByTokenInput
            >({
                query: USER_BY_USERNAME,
                variables: { token },
            })

            // Once returned, set user data in the store.
            const {
                id,
                username,
                isStaff,
                favoriteAnimals: favorites,
                emailOptIn,
            } = data.userByToken
            store.dispatch(
                setUser({ id, username, isStaff, favorites, emailOptIn })
            )
            // If error, open notification.
        } catch (err) {}
    }
}

export default checkJwt
