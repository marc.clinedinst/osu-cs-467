import { ApolloClient, InMemoryCache } from '@apollo/client'
import { createUploadLink } from 'apollo-upload-client'
import { setContext } from '@apollo/client/link/context'

const uploadLink = createUploadLink({
    uri: process.env.REACT_APP_GRAPHQL_URI ?? 'http://localhost:8000/graphql/',
})

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('jwtToken')
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `JWT ${token}` : '',
        },
    }
})

const client = new ApolloClient({
    link: authLink.concat(uploadLink),
    cache: new InMemoryCache({
        typePolicies: {
            AnimalType: {
                fields: {
                    statusUpdates: {
                        merge: false,
                    },
                    photographs: {
                        merge: false,
                    },
                },
                keyFields: ['id'],
            },
        },
    }),
})

export default client
