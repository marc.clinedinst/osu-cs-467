import React, { FunctionComponent } from 'react'
import { Router, Redirect, Route, Switch } from 'react-router-dom'
import AnimalSearch from 'components/routes/animalSearch/AnimalSearch'
import Landing from 'components/routes/landing/Landing'
import NavBar from 'components/common/NavBar'
import CustomDialog from 'components/common/CustomDialog'
import history from 'app/history'
import { useSelector } from './store'
import Notification from 'components/common/Notification'
import AnimalProfile from 'components/routes/animalProfile/AnimalProfile'
import PrivateRoute from './PrivateRoute'
import AddAnimal from 'components/routes/addAnimal/AddAnimal'
import Favorites from 'components/routes/favorites/Favorites'

const App: FunctionComponent = () => {
    const { username } = useSelector(state => state.user)

    return (
        <div data-testid='app'>
            <Router history={history}>
                <NavBar />
                <Switch>
                    <Route exact path='/'>
                        {username ? <Redirect to={'/search'} /> : <Landing />}
                    </Route>
                    <PrivateRoute
                        exact
                        path='/animal/:id'
                        component={AnimalProfile}
                    />
                    <PrivateRoute
                        exact
                        path='/search'
                        component={AnimalSearch}
                    />
                    <PrivateRoute
                        exact
                        adminOnly
                        path='/add-animal'
                        component={AddAnimal}
                    />
                    <PrivateRoute
                        exact
                        path='/favorites'
                        component={Favorites}
                    />
                </Switch>
                <CustomDialog />
                <Notification />
            </Router>
        </div>
    )
}

export default App
