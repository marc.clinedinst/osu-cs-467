import { FunctionComponent } from 'react'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import { MockedProvider, MockedResponse } from '@apollo/client/testing'
import store from 'app/store'
import { CustomRenderer, WrapperOpts, CustomMockRenderer } from './types'
import { TOKEN_AUTH } from 'gql/mutations'
import { GraphQLError } from 'graphql'
import { BrowserRouter, Router } from 'react-router-dom'
import { ANIMALS } from 'gql/queries'
import history from './history'

const tokenAuth = {
    refreshExpiresIn: 1,
    token: 'token',
    payload: { exp: 1, origIat: 1, username: 'test' },
    user: {
        username: 'test',
        isStaff: false,
        id: 1,
    },
}

const mocks: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOKEN_AUTH,
            variables: {
                username: 'test',
                password: 'asdf',
            },
        },
        result: { data: { tokenAuth } },
    },
    {
        request: {
            query: TOKEN_AUTH,
            variables: {
                username: 'a',
                password: 'a',
            },
        },
        result: {
            errors: [new GraphQLError('invalid test')],
            data: {},
        },
    },
]

// https://testing-library.com/docs/react-testing-library/setup#custom-render

// Because we are using Redux and Apollo Client, we need to have these
// providers included in testing to be able to test successfully.
// When including new providers, they should be added here.
const wrapper: FunctionComponent = ({ children }) => (
    <MockedProvider addTypename={false} mocks={mocks}>
        <Provider store={store}>
            <Router history={history}>{children}</Router>
        </Provider>
    </MockedProvider>
)
const wrapperWithRouter: FunctionComponent = ({ children }) => (
    <MockedProvider addTypename={false} mocks={mocks}>
        <Provider store={store}>
            <BrowserRouter>{children}</BrowserRouter>
        </Provider>
    </MockedProvider>
)
const wrapperWithMockProvider: (
    apolloMocks: MockedResponse<Record<string, any>>[]
) => FunctionComponent = apolloMocks => ({ children }) => (
    <MockedProvider addTypename={false} mocks={apolloMocks}>
        <Provider store={store}>
            <BrowserRouter>{children}</BrowserRouter>
        </Provider>
    </MockedProvider>
)

// Creating a custom renderer that will package all the providers necessary
// for the app to function.
const customRender: CustomRenderer = (ui, options) => {
    const renderOpts: WrapperOpts = { wrapper, ...options }
    return render(ui, renderOpts)
}
const customRenderWithRouter: CustomRenderer = (ui, options) => {
    const renderOpts: WrapperOpts = { wrapper: wrapperWithRouter, ...options }
    return render(ui, renderOpts)
}

const customRenderWithMockProvider: CustomMockRenderer = (
    ui,
    mocks,
    options
) => {
    const renderOpts: WrapperOpts = {
        wrapper: wrapperWithMockProvider(mocks),
        ...options,
    }
    return render(ui, renderOpts)
}

// re-export everything
export * from '@testing-library/react'

// override render method
export {
    customRender as render,
    customRenderWithRouter as renderWithRouter,
    customRenderWithMockProvider as renderWithMockProvider,
}
