import {
    AnimalEditFormValues,
    FilterFormValueKeys,
    FilterFormValues,
    MultiselectDropdownOptions,
} from 'components/common/types'
import { AddAnimalFormValues } from 'components/routes/addAnimal/types'
import { FILTER_FORM_DEFAULT_VALUES } from 'constants/constants'
import { AnimalsInput, DispositionType, UpdateAnimalInput } from 'gql/types'
export const getDispositionIds = (dispositions: MultiselectDropdownOptions) =>
    dispositions?.map(({ value }) => value)

/**
 * A function that returns an AnimalsInput object from the FilterFormValues object.
 * Sets default values to undefined and converts values to the correct format/data type.
 */
export const sanitizeFilterFormValues: (
    arg: FilterFormValues
) => AnimalsInput = formValues => {
    const animalInput: AnimalsInput = {}
    const keys = Object.keys(formValues) as FilterFormValueKeys[]

    keys.forEach(key => {
        if (formValues[key] !== FILTER_FORM_DEFAULT_VALUES[key]) {
            switch (key) {
                case 'daysBeforeToday':
                    animalInput.daysBeforeToday = parseInt(
                        formValues.daysBeforeToday
                    )
                    break
                case 'dispositions':
                    if (formValues?.dispositions?.length)
                        animalInput.dispositions = getDispositionIds(
                            formValues.dispositions
                        )
                    break
                default:
                    animalInput[key] = formValues[key]
                    break
            }
        }
    })
    return animalInput
}

export const sanitizeAddAnimalFormValues = (
    formValues: AddAnimalFormValues
) => {
    const { dispositions: dispositionObjects, ...rest } = formValues
    return {
        dispositions: getDispositionIds(dispositionObjects),
        ...rest,
    }
}

export const convertToMultiSelectOptions = (
    dispositions: DispositionType[]
) => {
    return dispositions.map(d => {
        return {
            value: d.id,
            label: d.description,
        }
    })
}

export const sanitizeAnimalEditFormValues: (
    formValues: AnimalEditFormValues,
    id: number
) => UpdateAnimalInput = (formValues, id) => {
    const { dispositions: dispositionObjects, ...rest } = formValues
    return {
        dispositions: getDispositionIds(dispositionObjects),
        ...rest,
        id,
    }
}
