import { useQuery } from '@apollo/client'
import { useDispatch } from 'app/store'
import { openNotification } from 'ducks/notificationSlice'
import { ANIMAL_DISPOSITIONS } from 'gql/queries'
import { DispositionResponse } from 'gql/types'
/**
 * Returns an array of disposition values. On error, a snackbar notification is
 * opened to let the user know that an error has occurred.
 */
const useDispositions = () => {
    const dispatch = useDispatch()
    const { data } = useQuery<DispositionResponse>(ANIMAL_DISPOSITIONS, {
        onError: error =>
            dispatch(
                openNotification({ type: 'error', message: error.message })
            ),
    })
    return data
}

export default useDispositions
