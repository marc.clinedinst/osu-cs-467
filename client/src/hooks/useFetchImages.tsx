import { useState, useEffect } from 'react'
import { FetchImagesHook } from 'hooks/types'
import axios from 'axios'

export const useFetchImages: FetchImagesHook = number => {
    const [images, setImages] = useState<Array<string>>([])
    useEffect(() => {
        const cancelToken = axios.CancelToken
        const source = cancelToken.source()
        const fetchImages = async () => {
            try {
                const response = await axios.get(
                    `https://dog.ceo/api/breeds/image/random/${number}`,
                    { cancelToken: source.token }
                )
                const data = response.data
                setImages(data.message)
            } catch (err) {
                if (axios.isCancel(err)) {
                    return 'Fetch images cancelled.'
                }
                console.error(err)
            }
        }
        fetchImages()
        return () => {
            source.cancel('Fetch images interrupted')
        }
    }, [number])
    return images
}
