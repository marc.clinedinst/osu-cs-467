import { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'app/store'
import { useLazyQuery } from '@apollo/client'

import { UseParamsValues } from 'components/routes/animalProfile/types'

import { setAnimal } from 'ducks/animalSlice'
import { openNotification } from 'ducks/notificationSlice'

import { ANIMAL_BY_ID } from 'gql/queries'
import { AnimalByIdResponse } from 'gql/types'
import { FetechAnimalInfoHook } from 'hooks/types'
/**
 * Get animal information by id and set state. Returns an array with loading and
 * error states.
 */
export const useFetchAnimalInfo: FetechAnimalInfoHook = () => {
    const dispatch = useDispatch()
    const animal = useSelector(state => state.animal)
    const { id: routeAnimalId } = useParams<UseParamsValues>()

    const [getAnimal, { loading, error }] = useLazyQuery<AnimalByIdResponse>(
        ANIMAL_BY_ID,
        {
            fetchPolicy: 'network-only',
            onCompleted: data => dispatch(setAnimal(data.animalById)),
            onError: err =>
                dispatch(
                    openNotification({ type: 'error', message: err.message })
                ),
        }
    )
    // If there is no animal already set in state, ie clicking back button to
    // a profile or linked directly to a profile, get the id from the params
    // and get the animal data.
    useEffect(() => {
        if (!animal.id && routeAnimalId) {
            getAnimal({ variables: { id: parseInt(routeAnimalId) } })
        }
    }, [getAnimal, animal, routeAnimalId])
    return [loading, error]
}
