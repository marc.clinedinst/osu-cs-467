import { ApolloError } from '@apollo/client'

export type FetchImagesHook = (number: number) => Array<string>
export type FetechAnimalInfoHook = () => [boolean, ApolloError | undefined]
