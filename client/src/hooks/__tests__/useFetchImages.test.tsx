import { useFetchImages } from 'hooks/useFetchImages'
import { renderHook, act } from '@testing-library/react-hooks'

jest.mock('axios', () => ({ get: () => ({ data: ['1'] }) }))

test('Testing useFetchImages Hook', async () => {
    await act(async () => {
        await renderHook(() => {
            useFetchImages(1)
        })
    })
})
