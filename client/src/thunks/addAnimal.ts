import { createAsyncThunk } from '@reduxjs/toolkit'
import client from 'app/client'
import { openNotification } from 'ducks/notificationSlice'
import { CREATE_ANIMAL } from 'gql/mutations'
import { CreateAnimalInput, CreateAnimalResponse } from 'gql/types'
import { AddAnimalThunkInput, AsyncDispatchState } from './types'
import uploadPhoto from 'thunks/uploadPhoto'
import getAnimals from './getAnimals'

const addAnimal = createAsyncThunk<
    boolean,
    AddAnimalThunkInput,
    AsyncDispatchState
>(
    'animal/create',
    async ({ createAnimal, photographs }, { dispatch, getState }) => {
        try {
            const { data } = await client.mutate<
                CreateAnimalResponse,
                CreateAnimalInput
            >({
                mutation: CREATE_ANIMAL,
                variables: { ...createAnimal },
            })
            const animalID = data?.createAnimal?.animal.id

            if (!animalID) throw Error('Unable to create animal.')

            dispatch(uploadPhoto({ id: animalID, photographs }))
            dispatch(getAnimals())
            return true
        } catch (err) {
            dispatch(openNotification({ type: 'error', message: err.message }))
            return false
        }
    }
)

export default addAnimal
