import store from 'app/store'
import { RootState } from 'app/types'
import { ImageFile } from 'components/routes/addAnimal/types'
import { CreateAnimalInput, UpdateAnimalInput } from 'gql/types'

export interface AsyncDispatchState {
    dispatch: typeof store.dispatch
    state: RootState
}

export interface AddAnimalThunkInput {
    photographs: ImageFile[]
    createAnimal: CreateAnimalInput
}
export interface UpdateAnimalThunkInput {
    updateAnimal: UpdateAnimalInput
    photosToDelete: number[]
    photosToUpload: ImageFile[]
}
export interface UploadPhotoThunkInput {
    id: number
    photographs: ImageFile[]
}
