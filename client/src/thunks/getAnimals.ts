import { createAsyncThunk } from '@reduxjs/toolkit'
import { AnimalsInput, AnimalsResponse } from 'gql/types'
import client from 'app/client'
import { setAnimals } from 'ducks/animalSearchSlice'
import { ANIMALS } from 'gql/queries'
import { openNotification } from 'ducks/notificationSlice'
import { sanitizeFilterFormValues } from 'utils/sanitize'
import { AsyncDispatchState } from 'thunks/types'
import { setAnimal } from 'ducks/animalSlice'

const getAnimals = createAsyncThunk<void, void, AsyncDispatchState>(
    'animals/animalSearch',
    async (_, { dispatch, getState }) => {
        try {
            const { data } = await client.query<AnimalsResponse, AnimalsInput>({
                query: ANIMALS,
                variables: sanitizeFilterFormValues(getState().filters),
                fetchPolicy: 'network-only',
            })
            dispatch(setAnimals(data.animals))

            // Check if there is an animal in the animal slice that we need to update.
            const animalId = getState().animal.id
            if (animalId) {
                const allAnimalIds = data.animals.map(({ id }) => id)
                const animalInfoIndex = allAnimalIds.indexOf(animalId)
                if (animalInfoIndex !== -1) {
                    dispatch(setAnimal(data.animals[animalInfoIndex]))
                }
            }
        } catch (error) {
            dispatch(
                openNotification({ type: 'error', message: error.message })
            )
        }
    }
)

export default getAnimals
