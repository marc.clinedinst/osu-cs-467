import { createAsyncThunk } from '@reduxjs/toolkit'
import { batch } from 'react-redux'
import client from 'app/client'

import { clearAnimal } from 'ducks/animalSlice'
import { closeDialog } from 'ducks/customDialogSlice'
import { openNotification } from 'ducks/notificationSlice'
import getAnimals from 'thunks/getAnimals'
import uploadPhoto from 'thunks/uploadPhoto'

import { DELETE_ANIMAL_PHOTOGRAPH, UPDATE_ANIMAL } from 'gql/mutations'
import {
    DeleteAnimalPhotographInput,
    DeleteAnimalPhotographResponse,
    UpdateAnimalInput,
    UpdateAnimalResponse,
} from 'gql/types'
import { AsyncDispatchState, UpdateAnimalThunkInput } from 'thunks/types'

/**
 * Handles updating animal information.
 * 1. Starts uploading new animal photos
 * 2. Updates animal information
 * 3. Deletes animal photos.
 * 4. Waits for 2 and 3, then clears animal information, refetches animals, and closes the dialog.
 */
const updateAnimal = createAsyncThunk<
    void,
    UpdateAnimalThunkInput,
    AsyncDispatchState
>(
    'animal/update',
    async ({ updateAnimal, photosToDelete, photosToUpload }, { dispatch }) => {
        try {
            dispatch(
                uploadPhoto({
                    photographs: photosToUpload,
                    id: updateAnimal.id,
                })
            )
            await client.mutate<UpdateAnimalResponse, UpdateAnimalInput>({
                mutation: UPDATE_ANIMAL,
                variables: updateAnimal,
            })
            const photoDeletePromises = photosToDelete.map(id =>
                client.mutate<
                    DeleteAnimalPhotographResponse,
                    DeleteAnimalPhotographInput
                >({ mutation: DELETE_ANIMAL_PHOTOGRAPH, variables: { id } })
            )
            await Promise.all(photoDeletePromises)

            batch(() => {
                dispatch(clearAnimal())
                dispatch(getAnimals())
                dispatch(closeDialog())
            })
        } catch (err) {
            dispatch(openNotification({ type: 'error', message: err.message }))
        }
    }
)

export default updateAnimal
