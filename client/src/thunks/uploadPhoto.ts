import { createAsyncThunk } from '@reduxjs/toolkit'
import client from 'app/client'
import { openNotification } from 'ducks/notificationSlice'
import { UPLOAD_ANIMAL_PHOTOGRAPH } from 'gql/mutations'
import { UploadAnimalInput, UploadAnimalResponse } from 'gql/types'
import getAnimals from 'thunks/getAnimals'
import { AsyncDispatchState, UploadPhotoThunkInput } from 'thunks/types'

const uploadPhoto = createAsyncThunk<
    void,
    UploadPhotoThunkInput,
    AsyncDispatchState
>('animal/uploadPhoto', async ({ photographs, id }, { dispatch }) => {
    try {
        if (photographs.length) {
            const photographPromises = photographs.map(photograph => {
                return client.mutate<UploadAnimalResponse, UploadAnimalInput>({
                    mutation: UPLOAD_ANIMAL_PHOTOGRAPH,
                    variables: {
                        file: photograph,
                        id,
                    },
                })
            })
            await Promise.all(photographPromises)
            dispatch(getAnimals())
        }
    } catch (err) {
        dispatch(openNotification({ type: 'error', message: err.message }))
    }
})

export default uploadPhoto
