import { createAsyncThunk } from '@reduxjs/toolkit'
import { FavoriteAnimalsResponse } from 'gql/types'
import client from 'app/client'
import { setFavorites } from 'ducks/userSlice'
import { FAVORITE_ANIMALS } from 'gql/queries'
import { openNotification } from 'ducks/notificationSlice'
import { AsyncDispatchState } from './types'

const getFavorites = createAsyncThunk<
    // Return type of the payload creator
    void,
    // First argument to the payload creator
    void,
    AsyncDispatchState
>('animals/animalSearch', async (_, { dispatch }) => {
    try {
        const { data } = await client.query<FavoriteAnimalsResponse>({
            query: FAVORITE_ANIMALS,
            fetchPolicy: 'no-cache',
        })
        dispatch(setFavorites(data.favoriteAnimals))
    } catch (error) {
        dispatch(openNotification({ type: 'error', message: error.message }))
    }
})

export default getFavorites
