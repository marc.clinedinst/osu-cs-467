import React, { FC, useEffect } from 'react'
import { useDropzone } from 'react-dropzone'
import styled from 'styled-components'

import { ImageDropProps } from 'components/routes/addAnimal/types'
import Typography from 'components/common/StyledTypography'

const ThumbsContainer = styled.aside`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-top: 16px;
    padding: 20px;
`

const Thumb = styled.div`
    position: relative;
    border-radius: 2px;
    border: 1px solid #eaeaea;
    margin-bottom: 8px;
    margin-right: 8px;
    width: 100px;
    height: 100%;
    padding: 4px;
    box-sizing: border-box;
`

const InnerThumbnail = styled.div`
    display: flex;
    min-width: 0;
    overflow: hidden;
`
const Image = styled.img`
    display: block;
    width: 100px;
    height: 100px;
    object-fit: cover;
`

const ThumbnailButton = styled.div`
    position: absolute;
    right: 10px;
    bottom: 10px;
    background: rgba(0, 0, 0, 0.8);
    color: #fff;
    border: 0;
    border-radius: 0.325em;
    cursor: pointer;
`

const Dropzone = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    padding: 20px;
    border-width: 2px;
    border-radius: 2px;
    border-color: #eeeeee;
    border-style: dashed;
    background-color: #fafafa;
    color: #bdbdbd;
    outline: none;
    transition: border 0.24s ease-in-out;
`

const ImageDrop: FC<ImageDropProps> = ({
    className,
    text,
    files,
    setFiles,
}) => {
    const { getRootProps, getInputProps, fileRejections } = useDropzone({
        accept: ['image/png', 'image/gif', 'image/jpeg'],
        validator: file => {
            if (file.size > 5000000)
                return {
                    code: 'file-too-large',
                    message: 'File too large. Must be under 5MB',
                }
            return null
        },
        onDrop: (acceptedFiles, rejectedFiles) => {
            const newFiles = acceptedFiles.map(file => {
                return Object.assign(file, {
                    preview: URL.createObjectURL(file),
                })
            })
            setFiles(files.concat(newFiles))
            rejectedFiles = []
        },
    })

    const handleDelete = (fileIndex: number) => () =>
        setFiles(files.filter((file, index) => fileIndex !== index))

    const thumbs = files.map((file, index) => (
        <Thumb data-testid='image-drop-thumbnail' key={`${file.name} ${index}`}>
            <InnerThumbnail>
                <Image
                    data-testid='image-drop-thumbnail-image'
                    src={file.preview}
                    alt=''
                />
            </InnerThumbnail>
            <ThumbnailButton
                data-testid='image-drop-thumbnail-delete-button'
                onClick={handleDelete(index)}
            >
                delete
            </ThumbnailButton>
        </Thumb>
    ))

    // Make sure to revoke the data uris to avoid memory leaks
    useEffect(
        () => () => {
            files.forEach(file => URL.revokeObjectURL(file.preview))
        },
        [files]
    )

    return (
        <div className={className}>
            <Dropzone data-testid='image-drop-dropzone' {...getRootProps()}>
                <input
                    data-testid='image-drop-input'
                    type='file'
                    {...getInputProps()}
                />
                <p>{text}</p>
            </Dropzone>
            {fileRejections[0] && (
                <Typography
                    data-testid='image-drop-error'
                    variant='subtitle2'
                    $textColor='red'
                >
                    {fileRejections[0].errors[0].message}
                </Typography>
            )}
            <ThumbsContainer data-testid='image-drop-thumbnail-container'>
                {thumbs}
            </ThumbsContainer>
        </div>
    )
}

export default ImageDrop
