import React from 'react'
import { MockedResponse } from '@apollo/client/testing'
import { ANIMAL_DISPOSITIONS } from 'gql/queries'
import {
    fireEvent,
    screen,
    act,
    renderWithMockProvider,
} from 'app/testing-utils'
import AddAnimal from 'components/routes/addAnimal/AddAnimal'
import client from 'app/client'
import store from 'app/store'
import { setUser } from 'ducks/userSlice'
import selectEvent from 'react-select-event'
import { GraphQLError } from 'graphql'
jest.mock('app/client', () => {
    return {
        mutate: () => ({
            data: {
                createAnimal: {
                    animal: {
                        id: 1,
                    },
                },
            },
        }),
    }
})
const mockUser = {
    username: 'test',
    id: 1,
    isStaff: true,
    emailOptIn: false,
    favorites: [],
}
const mockSuccess: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ANIMAL_DISPOSITIONS,
            variables: {},
        },
        result: {
            data: {
                animalDispositions: [
                    { id: 1, description: 'Aggressive' },
                    { id: 2, description: 'Bad with children' },
                ],
            },
        },
    },
]

const mockFailure: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ANIMAL_DISPOSITIONS,
            variables: {},
        },
        result: {
            errors: [new GraphQLError('Something went wrong.')],
        },
    },
]

describe('AddAnimal Component Tests', () => {
    afterEach(() => {})

    test('render AddAnimal Component and confirm it has rendered', () => {
        renderWithMockProvider(<AddAnimal />, mockSuccess)
        const addAnimalContainer = screen.queryByTestId('add-animal-container')
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const dropzone = screen.queryByTestId('image-drop-dropzone')
        const thumbnailContainer = screen.queryByTestId(
            'image-drop-thumbnail-container'
        )
        expect(addAnimalContainer).toBeInTheDocument()
        expect(nameField).toBeInTheDocument()
        expect(descriptionField).toBeInTheDocument()
        expect(dropzone).toBeInTheDocument()
        expect(thumbnailContainer).toBeInTheDocument()
    })

    test('render AddAnimal Component with error from getting dispositions', async () => {
        await act(async () => {
            await renderWithMockProvider(<AddAnimal />, mockFailure)
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        const addAnimalContainer = screen.queryByTestId('add-animal-container')
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const dropzone = screen.queryByTestId('image-drop-dropzone')
        const thumbnailContainer = screen.queryByTestId(
            'image-drop-thumbnail-container'
        )
        expect(addAnimalContainer).toBeInTheDocument()
        expect(nameField).toBeInTheDocument()
        expect(descriptionField).toBeInTheDocument()
        expect(dropzone).toBeInTheDocument()
        expect(thumbnailContainer).toBeInTheDocument()

        expect(store.getState().notification.type).toBe('error')
        expect(store.getState().notification.message).toBe(
            'Something went wrong.'
        )
    })

    test('Fill out field, add image, and submit', async () => {
        client.mutate = jest
            .fn()
            .mockImplementationOnce(() => {
                return {
                    data: {
                        createAnimal: {
                            animal: {
                                id: 1,
                            },
                        },
                    },
                }
            })
            .mockImplementationOnce(() => {
                return {
                    data: {},
                }
            })

        client.query = jest
            .fn()
            .mockImplementationOnce(() => {
                return {
                    data: {
                        animals: [
                            {
                                id: 1,
                                name: 'test',
                                description: 'test',
                                breed: 'Test',
                                dateAdded: '2020-02-02',
                                dateOfBirth: '2020-02-02',
                                gender: 'Male',
                                dispositions: [],
                                kind: 'Dog',
                                status: 'Adopted',
                                photographs: [],
                                statusUpdates: [],
                            },
                        ],
                    },
                }
            })
            .mockImplementationOnce(() => {
                return {
                    data: {},
                }
            })
        await act(async () => {
            await renderWithMockProvider(<AddAnimal />, mockSuccess)
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        store.dispatch(setUser(mockUser))
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const birthdayField = screen.queryByTestId('dateOfBirth-field')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const kindField = screen.queryByTestId('dropdown-kind')
        const statusField = screen.queryByTestId('dropdown-status')
        const dispositionsField = screen.queryByText('Dispositions')
        const imageDropInput = screen.queryByTestId('image-drop-input')

        const submitButton = screen.queryByTestId('add-animal-submit-button')

        window.URL.createObjectURL = jest.fn().mockImplementation(() => 'url')
        window.URL.revokeObjectURL = jest
            .fn()
            .mockImplementation(() => 'revoke')

        const file = new File(['file'], 'ping.png', {
            type: 'image/png',
        })
        Object.defineProperty(imageDropInput, 'files', {
            value: [file],
        })
        if (
            nameField &&
            descriptionField &&
            submitButton &&
            imageDropInput &&
            breedField &&
            genderField &&
            kindField &&
            statusField &&
            dispositionsField &&
            birthdayField
        ) {
            await act(async () => {
                await fireEvent.change(nameField, { target: { value: 'test' } })
                await fireEvent.change(descriptionField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(breedField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(birthdayField, {
                    target: { value: '2020-02-02' },
                })
                await fireEvent.change(statusField, {
                    target: { value: 'Adopted' },
                })
                await fireEvent.change(genderField, {
                    target: { value: 'Male' },
                })
                await fireEvent.change(kindField, {
                    target: { value: 'Other' },
                })
                await selectEvent.select(dispositionsField, 'Aggressive')
                await fireEvent.drop(imageDropInput)
                await fireEvent.submit(submitButton)
            })
        }
        expect(store.getState().animals?.length).toBe(1)
    })

    test('Fill out field, add image, and submit with error uploading image', async () => {
        client.mutate = jest
            .fn()
            .mockImplementationOnce(() => {
                return {
                    data: {
                        createAnimal: {
                            animal: {
                                id: 1,
                            },
                        },
                    },
                }
            })
            .mockImplementationOnce(() => {
                throw new Error()
            })
        renderWithMockProvider(<AddAnimal />, mockSuccess)
        store.dispatch(setUser(mockUser))
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const birthdayField = screen.queryByTestId('dateOfBirth-field')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const kindField = screen.queryByTestId('dropdown-kind')
        const statusField = screen.queryByTestId('dropdown-status')
        const dispositionsField = screen.queryByText('Dispositions')
        const imageDropInput = screen.queryByTestId('image-drop-input')
        const submitButton = screen.queryByTestId('add-animal-submit-button')

        window.URL.createObjectURL = jest.fn().mockImplementation(() => 'url')
        window.URL.revokeObjectURL = jest
            .fn()
            .mockImplementation(() => 'revoke')

        const file = new File(['file'], 'ping.png', {
            type: 'image/png',
        })
        Object.defineProperty(imageDropInput, 'files', {
            value: [file],
        })
        if (
            nameField &&
            descriptionField &&
            submitButton &&
            imageDropInput &&
            breedField &&
            genderField &&
            kindField &&
            statusField &&
            dispositionsField &&
            birthdayField
        ) {
            await act(async () => {
                await fireEvent.change(nameField, { target: { value: 'test' } })
                await fireEvent.change(descriptionField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(breedField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(birthdayField, {
                    target: { value: '2020-02-02' },
                })
                await fireEvent.change(statusField, {
                    target: { value: 'Adopted' },
                })
                await fireEvent.change(genderField, {
                    target: { value: 'Male' },
                })
                await fireEvent.change(kindField, {
                    target: { value: 'Other' },
                })
                await selectEvent.select(dispositionsField, 'Aggressive')
                await fireEvent.drop(imageDropInput)
                await fireEvent.submit(submitButton)
            })
        }
        expect(store.getState().notification.type).toBe('error')
    })

    test('Fill out fields and submit, errors with no response from create animal mutation', async () => {
        renderWithMockProvider(<AddAnimal />, mockSuccess)
        client.mutate = jest.fn()
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const birthdayField = screen.queryByTestId('dateOfBirth-field')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const kindField = screen.queryByTestId('dropdown-kind')
        const statusField = screen.queryByTestId('dropdown-status')
        const dispositionsField = screen.queryByText('Dispositions')
        const imageDropInput = screen.queryByTestId('image-drop-input')
        const submitButton = screen.queryByTestId('add-animal-submit-button')

        if (
            nameField &&
            descriptionField &&
            submitButton &&
            imageDropInput &&
            breedField &&
            genderField &&
            kindField &&
            statusField &&
            dispositionsField &&
            birthdayField
        ) {
            await act(async () => {
                await fireEvent.change(nameField, { target: { value: 'test' } })
                await fireEvent.change(descriptionField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(breedField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(birthdayField, {
                    target: { value: '2020-02-02' },
                })
                await fireEvent.change(statusField, {
                    target: { value: 'Adopted' },
                })
                await fireEvent.change(genderField, {
                    target: { value: 'Male' },
                })
                await fireEvent.change(kindField, {
                    target: { value: 'Other' },
                })
                await selectEvent.select(dispositionsField, 'Aggressive')
                await fireEvent.drop(imageDropInput)
                await fireEvent.submit(submitButton)
            })
        }
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().notification.type).toBe('error')
    })

    test('Fill out fields and submit, errors with no animal id', async () => {
        renderWithMockProvider(<AddAnimal />, mockSuccess)
        client.mutate = jest.fn(() => new Promise(resolve => resolve({})))
        const nameField = screen.queryByTestId('name-field')
        const descriptionField = screen.queryByTestId('description-field')
        const birthdayField = screen.queryByTestId('dateOfBirth-field')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const kindField = screen.queryByTestId('dropdown-kind')
        const statusField = screen.queryByTestId('dropdown-status')
        const dispositionsField = screen.queryByText('Dispositions')
        const imageDropInput = screen.queryByTestId('image-drop-input')
        const submitButton = screen.queryByTestId('add-animal-submit-button')

        if (
            nameField &&
            descriptionField &&
            submitButton &&
            imageDropInput &&
            breedField &&
            genderField &&
            kindField &&
            statusField &&
            dispositionsField &&
            birthdayField
        ) {
            await act(async () => {
                await fireEvent.change(nameField, { target: { value: 'test' } })
                await fireEvent.change(descriptionField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(breedField, {
                    target: { value: 'test' },
                })
                await fireEvent.change(birthdayField, {
                    target: { value: '2020-02-02' },
                })
                await fireEvent.change(statusField, {
                    target: { value: 'Adopted' },
                })
                await fireEvent.change(genderField, {
                    target: { value: 'Male' },
                })
                await fireEvent.change(kindField, {
                    target: { value: 'Other' },
                })
                await selectEvent.select(dispositionsField, 'Aggressive')
                await fireEvent.drop(imageDropInput)
                await fireEvent.submit(submitButton)
            })
        }
        expect(store.getState().notification.type).toBe('error')
    })
    test('Click submit without inputting values and show validation errors', async () => {
        await act(async () => {
            await renderWithMockProvider(<AddAnimal />, mockSuccess)
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        const submitButton = screen.getByTestId('add-animal-submit-button')
        await fireEvent.submit(submitButton)
        expect((await screen.findAllByText(/required/)).length).toBe(8)
    })
})
