import React from 'react'
import { fireEvent, render, screen, act } from 'app/testing-utils'
import ImageDrop from 'components/routes/addAnimal/ImageDrop'
import { ImageFile } from 'components/routes/addAnimal/types'

const createFile: (
    name: string,
    size: number,
    type: string
) => { name: string; path: string; size: number; type: string } = (
    name,
    size,
    type
) => ({
    name,
    path: name,
    size,
    type,
})

const files: ImageFile[] = [new File([], 'test') as ImageFile]
const setFiles = jest.fn(newFiles => {
    files.push(newFiles)
})
describe('ImageDrop Component Tests', () => {
    beforeEach(() => {
        global.URL.createObjectURL = jest.fn()
        global.URL.revokeObjectURL = jest.fn()
        render(<ImageDrop files={files} setFiles={setFiles} text='a string' />)
    })
    afterEach(() => {})

    test('add files to the dropzone and check if state was set.', async () => {
        const files = [
            createFile('foo.png', 200, 'image/png'),
            createFile('bar.jpg', 200, 'image/jpeg'),
        ]
        const imageDropInput = screen.queryByTestId('image-drop-input')
        if (imageDropInput) {
            await act(async () => {
                await fireEvent.change(imageDropInput, { target: { files } })
            })
        }
        expect(setFiles).toHaveBeenCalled()
    })

    test('add a file that is over the size limit and cause an error', async () => {
        expect(screen.queryByTestId('image-drop-error')).not.toBeInTheDocument()

        const files = [createFile('foo.png', 200000000000, 'image/png')]
        const imageDropInput = screen.queryByTestId('image-drop-input')
        if (imageDropInput) {
            await act(async () => {
                await fireEvent.change(imageDropInput, { target: { files } })
            })
        }
        expect(screen.queryByTestId('image-drop-error')).toBeInTheDocument()
    })
    test('add a file that is over the size limit and cause an error', async () => {
        expect(setFiles).not.toHaveBeenCalled()
        const deleteButton = screen.queryByTestId(
            'image-drop-thumbnail-delete-button'
        )
        if (deleteButton) {
            await act(async () => {
                await fireEvent.click(deleteButton)
            })
        }
        expect(setFiles).toHaveBeenCalled()
    })
})
