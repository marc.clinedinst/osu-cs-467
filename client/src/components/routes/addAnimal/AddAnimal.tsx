import React, { FC, useState } from 'react'
import { useDispatch } from 'app/store'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { SubmitHandler, useForm } from 'react-hook-form'
import {
    convertToMultiSelectOptions,
    sanitizeAddAnimalFormValues,
} from 'utils/sanitize'
import addAnimal from 'thunks/addAnimal'

import ImageDrop from 'components/routes/addAnimal/ImageDrop'
import Dropdown from 'components/common/Dropdown'
import ReactSelect from 'components/common/MultiSelect'
import Typography from 'components/common/StyledTypography'
import ControlledText from 'components/common/ControlledText'
import {
    Button,
    CircularProgress,
    Backdrop as MuiBackDrop,
    Paper,
} from '@material-ui/core'

import {
    AddAnimalFormValues,
    ImageFile,
} from 'components/routes/addAnimal/types'
import {
    GENDER_DROPDOWN_OPTIONS,
    KIND_DROPDOWN_OPTIONS,
    STATUS_OPTIONS,
} from 'constants/constants'
import useDispositions from 'hooks/useDispositions'

const Backdrop = styled(MuiBackDrop)`
    && {
        z-index: 2;
    }
`

const Container = styled.div`
    display: flex;
    padding: 20px;
    justify-content: center;
`

const StyledImageDrop = styled(ImageDrop)`
    && {
    }
`
const MultiSelect = styled(ReactSelect)`
    && {
        width: 100%;
    }
`
const StyledButton = styled(Button)`
    && {
        margin-top: 10px;
    }
`

const StyledPaper = styled(Paper)`
    display: grid;
    padding: 30px 60px;

    grid-template-columns: 325px;
    @media (min-width: 576px) {
        grid-template-columns: 500px;
    }
    @media (min-width: 768px) {
        grid-template-columns: 650px;
    }
    @media (min-width: 992px) {
        grid-template-columns: 800px;
        column-gap: 30px;
    }
    @media (min-width: 1200px) {
        grid-template-columns: 1200px;
    }
`

const AddAnimal: FC = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const [open, setOpen] = useState<boolean>(false)
    const [files, setFiles] = useState<ImageFile[]>([])
    const data = useDispositions()
    const {
        register,
        errors,
        handleSubmit,
        control,
    } = useForm<AddAnimalFormValues>()
    const onSubmit: SubmitHandler<AddAnimalFormValues> = async formValues => {
        setOpen(true)
        const { payload: wasSuccessful } = await dispatch(
            addAnimal({
                createAnimal: sanitizeAddAnimalFormValues(formValues),
                photographs: files,
            })
        )
        setOpen(false)
        if (wasSuccessful) history.push('/search')
    }

    return (
        <Container data-testid='add-animal-container'>
            <StyledPaper>
                <Typography variant='h4'>Add Animal</Typography>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <StyledImageDrop
                        text='(Optional) Drag and drop images here.'
                        files={files}
                        setFiles={setFiles}
                    />
                    <ControlledText
                        errors={errors}
                        register={() => register({ required: true })}
                        label='Name'
                        name='name'
                    />
                    <ControlledText
                        errors={errors}
                        register={() => register({ required: true })}
                        label='Description'
                        name='description'
                        textFieldProps={{ multiline: true, rows: 5 }}
                    />
                    <ControlledText
                        errors={errors}
                        register={() => register({ required: true })}
                        label='Birthday'
                        name='dateOfBirth'
                        textFieldProps={{
                            type: 'date',
                            InputLabelProps: { shrink: true },
                        }}
                    />
                    <ControlledText
                        errors={errors}
                        register={() => register({ required: true })}
                        label='Breed'
                        name='breed'
                    />
                    <Dropdown
                        control={control}
                        errors={errors}
                        outlined
                        labelId='gender'
                        label='Gender'
                        options={GENDER_DROPDOWN_OPTIONS}
                        selectProps={{ variant: 'outlined' }}
                    />
                    <Dropdown
                        control={control}
                        errors={errors}
                        outlined
                        labelId='status'
                        label='Status'
                        options={STATUS_OPTIONS}
                        selectProps={{ variant: 'outlined' }}
                    />
                    <Dropdown
                        control={control}
                        errors={errors}
                        outlined
                        labelId='kind'
                        label='Kind'
                        options={KIND_DROPDOWN_OPTIONS}
                        selectProps={{ variant: 'outlined' }}
                    />
                    <MultiSelect
                        control={control}
                        errors={errors}
                        required
                        native
                        noLabel
                        placeholder='Dispositions'
                        label='Dispositions'
                        labelId='dispositions'
                        options={convertToMultiSelectOptions(
                            data?.animalDispositions ?? []
                        )}
                    />
                    <StyledButton
                        data-testid='add-animal-submit-button'
                        variant='contained'
                        type='submit'
                    >
                        Submit
                    </StyledButton>
                </form>
                <Backdrop open={open}>
                    <CircularProgress color='inherit' />
                </Backdrop>
            </StyledPaper>
        </Container>
    )
}

export default AddAnimal
