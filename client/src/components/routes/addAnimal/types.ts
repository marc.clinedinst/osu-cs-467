import { MultiselectDropdownOptions } from 'components/common/types'
import { GenderType, KindType, StatusType } from 'gql/types'

export interface ImageFile extends File {
    preview: string
}

export interface ImageDropProps {
    className?: string
    files: ImageFile[]
    setFiles: Function
    text: string
}

export interface AddAnimalFormValues {
    name: string
    description: string
    dateOfBirth: string
    breed: string
    gender: GenderType
    status: StatusType
    kind: KindType
    dispositions: MultiselectDropdownOptions
}
