import React, { FunctionComponent } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { useDispatch } from 'app/store'
import {
    Avatar,
    Chip,
    IconButton,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
} from '@material-ui/core'
import { FavoriteItemProps } from 'components/routes/favorites/types'
import { setAnimal, AnimalState } from 'ducks/animalSlice'
import Typography from 'components/common/StyledTypography'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import { useMutation } from '@apollo/client'
import { ToggleFavoriteAnimalReponse } from 'gql/types'
import { TOGGLE_FAVORITE_ANIMAL } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'
import getFavorites from '../../../thunks/getFavorites'
import { DEFAULT_IMAGE_PAW_PRINT } from 'constants/constants'

const StyledListItemText = styled(ListItemText)`
    && {
        cursor: pointer;
    }
`

const StyledChip = styled(Chip)`
    && {
        margin-left: 10px;
        margin-bottom: 10px;
        height: 20px;
        font-size: 0.75rem;
        border: 1px solid;
    }

    &&.Available {
        background-color: green;
    }
    &&.Adopted {
        background-color: gray;
    }
    &&.InFoster {
        background-color: yellow;
        border-color: black;
        color: black;
    }
    &&.Pending {
        background-color: yellow;
        border-color: black;
        color: black;
    }
`

const FavoriteItem: FunctionComponent<FavoriteItemProps> = props => {
    const dispatch = useDispatch()
    const history = useHistory()

    const { breed, gender, description, name, status, id, photographs } = props

    const handleClickViewProfile = () => {
        const { children, ...rest } = props
        const animal: AnimalState = {
            ...rest,
        }
        dispatch(setAnimal(animal))
        history.push(`/animal/${rest.id}`)
    }

    const [submitToggleFavorite] = useMutation<ToggleFavoriteAnimalReponse>(
        TOGGLE_FAVORITE_ANIMAL,
        {
            onCompleted: () => {
                dispatch(getFavorites())
            },
            onError: err =>
                dispatch(
                    openNotification({ type: 'error', message: err.message })
                ),
        }
    )

    const handleClickRemoveFavorite = () => {
        submitToggleFavorite({
            variables: {
                id,
            },
        })
    }
    return (
        <ListItem
            data-testid='favorite-item'
            alignItems='flex-start'
            divider={true}
            key={id.toString()}
        >
            {photographs && photographs?.length > 0 ? (
                <ListItemAvatar>
                    <Avatar alt={name} src={photographs[0].url} />
                </ListItemAvatar>
            ) : (
                <ListItemAvatar>
                    <Avatar alt={name} src={DEFAULT_IMAGE_PAW_PRINT} />
                </ListItemAvatar>
            )}
            <StyledListItemText
                onClick={handleClickViewProfile}
                data-testid='favorite-item-text'
            >
                <Typography
                    color='primary'
                    data-testid='favorite-item-name'
                    gutterBottom
                    variant='h5'
                    display='inline'
                >
                    {name}
                </Typography>
                <StyledChip
                    color='primary'
                    size='small'
                    label={status}
                    className={status.replace(/\s/g, '')}
                />

                <Typography
                    variant='subtitle2'
                    color='textSecondary'
                    data-testid='favorite-item-sex-breed'
                >
                    {`${gender}, ${breed}`}
                </Typography>
                <Typography
                    data-testid='favorite-item-description'
                    variant='body2'
                >
                    {description}
                </Typography>
            </StyledListItemText>
            <ListItemSecondaryAction>
                <IconButton
                    data-testid='favorite-item-toggle-button'
                    edge='end'
                    aria-label='delete'
                    onClick={handleClickRemoveFavorite}
                >
                    <DeleteForeverIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}

export default FavoriteItem
