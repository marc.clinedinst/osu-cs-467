import React from 'react'
import {
    act,
    fireEvent,
    renderWithMockProvider,
    screen,
} from 'app/testing-utils'
import FavoriteItem from 'components/routes/favorites/FavoriteItem'
import { FavoriteItemProps } from '../types'
import store from 'app/store'
import { clearUser, setUser } from 'ducks/userSlice'
import { MockedResponse } from '@apollo/client/testing'
import { TOGGLE_FAVORITE_ANIMAL } from 'gql/mutations'
import { GraphQLError } from 'graphql'
import client from 'app/client'
jest.mock('app/client')

const mocks: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_FAVORITE_ANIMAL,
            variables: {
                id: 1,
            },
        },
        result: { data: {} },
    },
    {
        request: {
            query: TOGGLE_FAVORITE_ANIMAL,
            variables: {
                id: 2,
            },
        },
        result: { errors: [new GraphQLError('error')] },
    },
]

const animal: FavoriteItemProps = {
    breed: 'Corgi',
    photographs: [],
    statusUpdates: [],
    favoriteAnimals: [],
    dateOfBirth: '01-01-2020',
    dateAdded: '01-01-2020',
    description: 'A cute thing',
    dispositions: [
        {
            id: 1,
            description: 'Happy',
        },
    ],
    gender: 'Female',
    kind: 'Dog',
    name: 'Boys',
    status: 'Pending',
    id: 1,
}
const favorites = [animal]
const user = {
    id: 1,
    username: 'test',
    isStaff: true,
    favorites,
}
describe('FavoriteItem Component Test', () => {
    beforeEach(async () => {
        await act(async () => {
            store.dispatch(setUser(user))
        })
    })
    afterEach(() => store.dispatch(clearUser()))

    test('render component as not staff user', async () => {
        await act(async () => {
            renderWithMockProvider(
                <FavoriteItem
                    {...animal}
                    photographs={[{ id: 1, url: 'http://image.png' }]}
                />,
                mocks
            )
        })
        const favoriteItem = screen.queryByTestId('favorite-item')
        const favoriteItemName = screen.queryByTestId('favorite-item-name')
        const toggleIconButton = screen.queryByTestId(
            'favorite-item-toggle-button'
        )
        expect(toggleIconButton).toBeInTheDocument()
        expect(favoriteItem).toBeInTheDocument()
        expect(favoriteItemName).toBeInTheDocument()
    })

    test('click on the item to view the animals profile', async () => {
        await act(async () => {
            renderWithMockProvider(<FavoriteItem {...animal} />, mocks)
        })
        const cardLink = screen.queryByTestId('favorite-item-text')
        if (cardLink) {
            await fireEvent.click(cardLink)
            expect(store.getState().animal.id).toBe(1)
        }
    })
    test('click on remove favorite button - success', async () => {
        await act(async () => {
            renderWithMockProvider(<FavoriteItem {...animal} />, mocks)
        })
        client.query = jest
            .fn()
            .mockReturnValue({ data: { favoriteAnimals: [] } })
        const button = screen.getByTestId('favorite-item-toggle-button')
        await act(async () => {
            fireEvent.click(button)
        })
        expect(store.getState().user.favorites?.length).toBe(0)
    })

    test('click on remove favorite button - fail', async () => {
        await act(async () => {
            renderWithMockProvider(<FavoriteItem {...animal} id={2} />, mocks)
        })
        client.query = jest
            .fn()
            .mockReturnValue({ data: { favoriteAnimals: [] } })
        const button = screen.getByTestId('favorite-item-toggle-button')
        await act(async () => {
            fireEvent.click(button)
        })
        expect(store.getState().user.favorites?.length).toBe(1)
    })
})
