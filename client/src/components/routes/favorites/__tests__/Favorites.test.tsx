import React from 'react'
import { screen, render } from 'app/testing-utils'

import store from 'app/store'
import { clearUser } from 'ducks/userSlice'
import { clearNotification } from 'ducks/notificationSlice'
import Favorites from 'components/routes/favorites/Favorites'
import { setFavorites } from 'ducks/userSlice'

jest.mock('app/client')

test('renders the Favorites component and confirm it has rendered', async () => {
    render(<Favorites />)
    store.dispatch(setFavorites([]))
    const favoritesContainer = screen.getByTestId('favorite-container')
    const favoritesList = screen.getByTestId('favorite-list')
    const favoriteItems = screen.queryAllByTestId('favorite-item')
    expect(favoritesContainer).toBeInTheDocument()
    expect(favoritesList).toBeInTheDocument()
    expect(favoriteItems.length).toEqual(0)
    store.dispatch(clearUser())

    jest.clearAllMocks()
})

test('renders the Favorites component with no results', async () => {
    render(<Favorites />)
    store.dispatch(setFavorites([]))

    const favoritesContainer = screen.getByTestId('favorite-container')
    const favoritesList = screen.getByTestId('favorite-list')
    const favoriteItems = screen.queryByTestId('favorite-item')
    const favoriteNoResults = screen.getByTestId(
        'favorite-list-no-results-alert'
    )
    const favoriteResultsEndText = screen.queryByTestId(
        'favorite-list-end-text'
    )
    expect(favoritesContainer).toBeInTheDocument()
    expect(favoritesList).toBeInTheDocument()
    expect(favoriteItems).toBeNull()
    expect(favoriteNoResults).toBeInTheDocument()
    expect(favoriteResultsEndText).not.toBeInTheDocument()
    store.dispatch(clearUser())

    jest.clearAllMocks()
})

test('renders the Favorites component with results', async () => {
    store.dispatch(
        setFavorites([
            {
                description: 'description',
                name: 'name',
                gender: 'Male',
                statusUpdates: [],
                favoriteAnimals: [],
                photographs: [],
                breed: 'breed',
                dateOfBirth: '01-01-2020',
                dateAdded: '01-01-2020',
                dispositions: [],
                kind: 'Dog',
                status: 'Adopted',
                id: 1,
            },
        ])
    )
    render(<Favorites />)
    const favoriteItems = screen.queryAllByTestId('favorite-item')
    expect(favoriteItems).not.toBeNull()
    store.dispatch(clearUser())
    store.dispatch(clearNotification())
})
