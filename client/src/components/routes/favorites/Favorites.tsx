import React, { FunctionComponent } from 'react'
import styled from 'styled-components'
import { useSelector } from 'app/store'
import { Alert } from '@material-ui/lab'
import FavoriteItem from './FavoriteItem'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'

const ContentGridContainer = styled.div`
    display: grid;
    justify-content: center;
    grid-template-columns: 325px;
    row-gap: 30px;
    padding: 60px 0px;
    @media (min-width: 576px) {
        grid-template-columns: 500px;
    }
    @media (min-width: 768px) {
        grid-template-columns: 650px;
    }
`

const StyledAlert = styled(Alert)`
    && {
        height: 48px;
        width: 100%;
        align-items: center;
    }
`

const Favorites: FunctionComponent = () => {
    const favorites = useSelector(state => state.user.favorites)

    return (
        <ContentGridContainer data-testid='favorite-container'>
            <Typography variant='h5'>Favorites</Typography>
            <List data-testid='favorite-list' key='favoritesList'>
                {favorites?.length !== 0 ? (
                    <>
                        {favorites?.map(props => (
                            <FavoriteItem key={props.id} {...props} />
                        ))}
                        <Typography
                            color='textSecondary'
                            display='block'
                            variant='caption'
                            align='center'
                            key='0'
                            data-testid='favorite-list-end-text'
                        >
                            End of list
                        </Typography>
                    </>
                ) : (
                    <StyledAlert
                        data-testid='favorite-list-no-results-alert'
                        severity='info'
                        key='none-found'
                    >
                        There are no animals in your Favorite List.
                        <br />
                        Search for animals and favorite the ones you want to
                        adopt.
                    </StyledAlert>
                )}
            </List>
        </ContentGridContainer>
    )
}

export default Favorites
