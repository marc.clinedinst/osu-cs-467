import React, { FC } from 'react'
import styled from 'styled-components'
import { useSelector } from 'app/store'
import { Chip } from '@material-ui/core'
import LazyLoad from 'components/common/LazyLoad'
import Typography from 'components/common/StyledTypography'
import { AnimalProfileDetailsProps } from 'components/routes/animalProfile/types'
import { formatDistance } from 'date-fns'

const StyledChip = styled(Chip)`
    && {
        width: fit-content;
    }
`

const BoldNewTypography = styled(Typography)`
    && {
        font-weight: bold;
    }
`

const CapitalizedNewTypography = styled(Typography)`
    &&::first-letter {
        text-transform: capitalize;
    }
`

const AnimalProfileDetails: FC<AnimalProfileDetailsProps> = ({ loading }) => {
    const animal = useSelector(state => state.animal)
    return (
        <>
            <LazyLoad
                loading={loading}
                skeletonProps={{ variant: 'text', width: '30%' }}
            >
                <BoldNewTypography
                    data-testid='animal-profile-name'
                    variant='h4'
                >
                    {animal.name}
                </BoldNewTypography>
            </LazyLoad>
            <LazyLoad
                loading={loading}
                skeletonProps={{ variant: 'text', width: '30%' }}
            >
                <Typography
                    data-testid='animal-profile-kind-breed-gender-dob'
                    variant='h6'
                >
                    {animal.gender}, {animal.breed} {animal.kind}
                </Typography>
            </LazyLoad>
            <LazyLoad
                loading={loading}
                skeletonProps={{ variant: 'text', width: '30%' }}
            >
                <CapitalizedNewTypography
                    data-testid='animal-profile-dob'
                    variant='h6'
                >
                    {animal.dateOfBirth
                        ? formatDistance(
                              new Date(animal.dateOfBirth),
                              new Date()
                          )
                        : ''}{' '}
                    old
                </CapitalizedNewTypography>
            </LazyLoad>
            {animal?.dispositions?.map(({ id, description }) => {
                return (
                    <StyledChip
                        data-testid={`animal-profile-chip`}
                        color='secondary'
                        size='small'
                        key={`${id}-${description}`}
                        label={description}
                    />
                )
            })}
            <LazyLoad
                loading={loading}
                numberOfSkeletons={4}
                skeletonProps={{ variant: 'text', width: '100%' }}
            >
                <Typography data-testid='animal-profile-description'>
                    {animal.description}
                </Typography>
            </LazyLoad>
            <LazyLoad
                loading={loading}
                skeletonProps={{ variant: 'text', width: '30%' }}
            >
                <Typography data-testid='animal-profile-date-added'>
                    I've been waiting{' '}
                    {animal.dateAdded
                        ? formatDistance(new Date(animal.dateAdded), new Date())
                        : ''}
                    !
                </Typography>
            </LazyLoad>
        </>
    )
}

export default AnimalProfileDetails
