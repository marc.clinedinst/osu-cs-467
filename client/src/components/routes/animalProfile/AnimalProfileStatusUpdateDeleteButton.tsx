import React, { FC } from 'react'
import { useDispatch } from 'app/store'
import styled from 'styled-components'

import { IconButton as MuiIconButton } from '@material-ui/core'
import { Delete as DeleteIcon } from '@material-ui/icons'
import { useMutation } from '@apollo/client'
import {
    DeleteAnimalStatusUpdateInput,
    DeleteAnimalStatusUpdateResponse,
} from 'gql/types'
import { DELETE_ANIMAL_STATUS_UPDATE } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'
import getAnimals from 'thunks/getAnimals'
import { deleteAnimalStatusUpdate } from 'ducks/animalSlice'
import { AnimalProfileStatusUpdateDeleteButtonProps } from './types'

const IconButton = styled(MuiIconButton)`
    && {
        :hover {
            background-color: rgba(0, 0, 0, 0);
        }
    }
`

const AnimalProfileStatusUpdateDeleteButton: FC<AnimalProfileStatusUpdateDeleteButtonProps> = ({
    id,
}) => {
    const dispatch = useDispatch()

    const [deleteStatusUpdate] = useMutation<
        DeleteAnimalStatusUpdateResponse,
        DeleteAnimalStatusUpdateInput
    >(DELETE_ANIMAL_STATUS_UPDATE, {
        variables: { id },
        onCompleted: ({ deleteAnimalStatusUpdate: statusUpdate }) => {
            dispatch(deleteAnimalStatusUpdate(statusUpdate.id))
            dispatch(getAnimals())
        },
        onError: err =>
            dispatch(openNotification({ type: 'error', message: err.message })),
    })

    return (
        <IconButton
            data-testid='animal-profile-delete-button'
            onClick={() => deleteStatusUpdate()}
            edge='end'
            aria-label='delete'
        >
            <DeleteIcon />
        </IconButton>
    )
}
export default AnimalProfileStatusUpdateDeleteButton
