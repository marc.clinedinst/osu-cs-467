import React, { FC, useEffect, useState } from 'react'
import styled from 'styled-components'

import { IconButton } from '@material-ui/core'
import { ArrowBack, ArrowForward, Close } from '@material-ui/icons'

import { ImageViewerProps } from 'components/routes/animalProfile/types'
import { DEFAULT_IMAGE_PAW_PRINT } from 'constants/constants'
import { useSelector } from 'app/store'
import { PhotographType } from 'gql/types'

const Outer = styled.div`
    display: flex;
    justify-content: center;
`
const StyledIconButton = styled(IconButton)`
    && {
    }
`

const ButtonBar = styled.div`
    background-color: rgb(255, 255, 255, 0.35);
    position: absolute;
    z-index: 0.2;
    box-shadow: 2px 2px 2px rgba();
`

const Image = styled.img`
    min-width: 325px;
    object-fit: cover;
    width: 100%;
    height: 100%;
`
const Inner = styled.div`
    width: 100%;
`

const setNext: (
    itemCount: number
) => (state: number) => number = itemCount => state => (state + 1) % itemCount

const setPrev: (
    itemCount: number
) => (state: number) => number = itemCount => state =>
    state === 0 ? itemCount - 1 : state - 1

const ImageViewer: FC<ImageViewerProps> = ({
    images,
    isEditable = false,
    onDelete,
}) => {
    const [activeImage, setActiveImage] = useState<number>(0)
    const [deletedImageIds, setDeletedImageIds] = useState<number[]>([])
    const [photos, setPhotos] = useState<Omit<PhotographType, 'animal'>[]>(
        images ?? []
    )
    const showCloseButton = useSelector(
        state => state.user.isStaff && isEditable && photos.length > 0
    )

    const handleOnDelete = async () => {
        const currentPhotoId = photos[activeImage].id
        // Add the id to the the delete list.
        setDeletedImageIds(prev => prev.concat(currentPhotoId))
        // Delete the photo from state.
        setPhotos(pictures =>
            pictures.filter(({ id }) => id !== currentPhotoId)
        )
        // Change the active photo to the previous photo.
        setActiveImage(state => (state > 0 ? --state : 0))
        // Call the custom delete function. The "deletedImageIds" value here is still from
        // the previous render. Must manually add the current photo id to submit all photo
        // ids.
        onDelete && onDelete(deletedImageIds.concat(currentPhotoId))
    }

    // On mount, list to the store for changes and set local photos state.
    useEffect(() => {
        setPhotos(images ?? [])
        setActiveImage(0)
    }, [images])
    return (
        <Outer data-testid='image-viewer'>
            <Inner>
                <ButtonBar data-testid='image-viewer-button-bar'>
                    {showCloseButton && (
                        <StyledIconButton
                            data-testid='image-viewer-delete'
                            onClick={handleOnDelete}
                            size='medium'
                        >
                            <Close />
                        </StyledIconButton>
                    )}
                    {photos.length > 0 && (
                        <>
                            <StyledIconButton
                                data-testid='image-viewer-back'
                                onClick={e => {
                                    e.preventDefault()
                                    setActiveImage(setPrev(photos.length))
                                }}
                                size='medium'
                            >
                                <ArrowBack />
                            </StyledIconButton>
                            <StyledIconButton
                                data-testid='image-viewer-forward'
                                onClick={e => {
                                    e.preventDefault()
                                    setActiveImage(setNext(photos.length))
                                }}
                            >
                                <ArrowForward />
                            </StyledIconButton>
                        </>
                    )}
                </ButtonBar>
                {photos.length !== 0 ? (
                    photos.map((photo, index) => {
                        return (
                            <Image
                                data-testid='image-viewer-image'
                                key={photo.id}
                                hidden={index !== activeImage}
                                src={photo.url}
                            />
                        )
                    })
                ) : (
                    <Image
                        data-testid='image-viewer-image-default'
                        hidden={false}
                        src={DEFAULT_IMAGE_PAW_PRINT}
                    />
                )}
            </Inner>
        </Outer>
    )
}

export default ImageViewer
