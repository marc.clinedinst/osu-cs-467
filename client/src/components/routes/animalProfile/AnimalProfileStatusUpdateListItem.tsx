import React, { FC, useState } from 'react'
import { format } from 'date-fns'
import { useDispatch, useSelector } from 'app/store'
import { useForm, SubmitHandler } from 'react-hook-form'
import styled from 'styled-components'

import {
    Divider,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from '@material-ui/core'

import AnimalProfileStatusUpdateDeleteButton from './AnimalProfileStatusUpdateDeleteButton'
import { IconButton as MuiIconButton } from '@material-ui/core'
import { Cancel, Check, Edit as EditIcon } from '@material-ui/icons'
import { useMutation } from '@apollo/client'
import {
    UpdateAnimalStatusUpdateInput,
    UpdateAnimalStatusUpdateResponse,
} from 'gql/types'
import { UPDATE_ANIMAL_STATUS_UPDATE } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'
import ControlledText from 'components/common/ControlledText'
import { updateAnimalStatusUpdate as updateAnimalStatusUpdateSlice } from 'ducks/animalSlice'
import {
    AnimalProfileStatusUpdateListItemProps,
    UpdateAnimalStatusFormValues,
} from 'components/routes/animalProfile/types'
import { updateAnimalInAnimals } from 'ducks/animalSearchSlice'

const IconButton = styled(MuiIconButton)`
    && {
        :hover {
            background-color: rgba(0, 0, 0, 0);
        }
    }
`
const Form = styled.form`
    width: 100%;
    padding-right: 24px;
`
const StyledListItem: FC<{ $isStaff: boolean }> = styled(ListItem)<{
    $isStaff: boolean
}>`
    && {
        ${({ $isStaff }) => $isStaff && 'padding-right: 96px;'}
    }
`

const AnimalProfileStatusUpdateListItem: FC<AnimalProfileStatusUpdateListItemProps> = ({
    created,
    statusText,
    id,
}) => {
    const dispatch = useDispatch()
    const { isStaff } = useSelector(state => state.user)
    const [isEditing, setIsEditing] = useState<boolean>(false)
    const { register, errors, handleSubmit, reset } = useForm({
        defaultValues: { statusText },
    })

    const [updateAnimalStatusUpdateMutation] = useMutation<
        UpdateAnimalStatusUpdateResponse,
        UpdateAnimalStatusUpdateInput
    >(UPDATE_ANIMAL_STATUS_UPDATE, {
        onCompleted: ({ updateAnimalStatusUpdate: { statusUpdate } }) => {
            const { animal, ...rest } = statusUpdate
            dispatch(updateAnimalStatusUpdateSlice(rest))
            dispatch(updateAnimalInAnimals(animal))
            reset({ statusText: statusUpdate.statusText })
        },
        onError: err => {
            dispatch(openNotification({ type: 'error', message: err.message }))
        },
    })

    const onSubmit: SubmitHandler<UpdateAnimalStatusFormValues> = ({
        statusText: newStatusText,
    }) => {
        updateAnimalStatusUpdateMutation({
            variables: { id, statusText: newStatusText },
        })
        setIsEditing(false)
    }

    const editItem = (
        <Form data-testid='list-item-form' onSubmit={handleSubmit(onSubmit)}>
            <ControlledText
                errors={errors}
                label='Status Update'
                name='statusText'
                register={() => register({ required: true })}
                textFieldProps={{ fullWidth: true }}
            />
            <ListItemSecondaryAction>
                <IconButton
                    data-testid='animal-profile-submit-button'
                    type='submit'
                    aria-label='approve'
                >
                    <Check />
                </IconButton>
                <IconButton
                    data-testid='animal-profile-cancel-button'
                    onClick={() => setIsEditing(false)}
                    aria-label='cancel'
                >
                    <Cancel />
                </IconButton>
            </ListItemSecondaryAction>
        </Form>
    )

    const listItem = (
        <>
            <ListItemText
                data-testid='list-item-text'
                primary={statusText}
                secondary={
                    <em>
                        @{format(new Date(created), 'PP')}-
                        {format(new Date(created), 'p')}
                    </em>
                }
            />

            {isStaff && (
                <ListItemSecondaryAction>
                    <IconButton
                        data-testid='animal-profile-edit-button'
                        onClick={() => setIsEditing(true)}
                        aria-label='edit'
                    >
                        <EditIcon />
                    </IconButton>
                    <AnimalProfileStatusUpdateDeleteButton id={id} />
                </ListItemSecondaryAction>
            )}
        </>
    )

    return (
        <>
            <StyledListItem $isStaff={isStaff ?? false}>
                {isEditing ? editItem : listItem}
            </StyledListItem>
            <Divider />
        </>
    )
}
export default AnimalProfileStatusUpdateListItem
