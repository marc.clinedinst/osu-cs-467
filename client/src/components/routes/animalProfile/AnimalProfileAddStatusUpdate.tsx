import React, { FC } from 'react'
import { useDispatch, useSelector } from 'app/store'
import { useForm } from 'react-hook-form'
import styled from 'styled-components'

import { addAnimalStatusUpdate } from 'ducks/animalSlice'

import { InputAdornment, IconButton as MuiIconButton } from '@material-ui/core'
import { Send } from '@material-ui/icons'
import ControlledText from 'components/common/ControlledText'
import { useMutation } from '@apollo/client'
import {
    AddAnimalStatusUpdateInput,
    AddAnimalStatusUpdateResponse,
} from 'gql/types'
import { ADD_ANIMAL_STATUS_UPDATE } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'
import getAnimals from 'thunks/getAnimals'

const IconButton = styled(MuiIconButton)`
    && {
        :hover {
            background-color: rgba(0, 0, 0, 0);
        }
    }
`

const UpdateBar = styled.form`
    margin: 0 10px;
    display: grid;
    grid-template-columns: 1fr;
`

const StyledTextField = styled(ControlledText)`
    && {
        margin: 8px 0;
    }
`

const AnimalProfileAddStatusUpdate: FC = () => {
    const dispatch = useDispatch()
    const { id } = useSelector(state => state.animal)
    const {
        register,
        handleSubmit,
        formState: { isValid, isDirty },
        reset,
    } = useForm({
        mode: 'all',
    })

    const [addStatusUpdate] = useMutation<
        AddAnimalStatusUpdateResponse,
        AddAnimalStatusUpdateInput
    >(ADD_ANIMAL_STATUS_UPDATE, {
        onCompleted: ({ addAnimalStatusUpdate: { statusUpdate } }) => {
            dispatch(addAnimalStatusUpdate(statusUpdate))
            dispatch(getAnimals())
            reset()
        },
        onError: err =>
            dispatch(openNotification({ type: 'error', message: err.message })),
    })

    const onSubmit = ({ statusUpdate }: { statusUpdate: string }) => {
        addStatusUpdate({ variables: { statusText: statusUpdate, id } })
    }

    return (
        <UpdateBar
            data-testid='new-status-update-form'
            onSubmit={handleSubmit(onSubmit)}
        >
            <StyledTextField
                register={() =>
                    register({
                        validate: value => {
                            if (!value)
                                return 'Add a status update before submitting'
                        },
                    })
                }
                label='Status Update'
                name='statusUpdate'
                textFieldProps={{
                    placeholder: 'Add new status update',
                    fullWidth: undefined,
                    InputProps: {
                        endAdornment: (
                            <InputAdornment position='end'>
                                <IconButton
                                    data-testid='new-status-update-form-submit-button'
                                    type='submit'
                                    disableFocusRipple
                                    disableTouchRipple
                                    disabled={!isDirty || !isValid}
                                    disableRipple
                                    edge='end'
                                >
                                    <Send />
                                </IconButton>
                            </InputAdornment>
                        ),
                    },
                    label: '',
                    rowsMax: 4,
                    multiline: true,
                }}
            />
        </UpdateBar>
    )
}
export default AnimalProfileAddStatusUpdate
