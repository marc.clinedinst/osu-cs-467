import React, { FC, useEffect } from 'react'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'app/store'
import { Paper } from '@material-ui/core'
import Container from 'components/common/Container'
import ImageViewer from 'components/routes/animalProfile/ImageViewer'
import AnimalProfileButtonBar from 'components/routes/animalProfile/AnimalProfileButtonBar'
import AnimalProfileDetails from 'components/routes/animalProfile/AnimalProfileDetails'
import { clearAnimal } from 'ducks/animalSlice'
import { useFetchAnimalInfo } from 'hooks/useFetchAnimalInfo'
import AnimalProfileStatusUpdateList from './AnimalProfileStatusUpdateList'
import AnimalProfileAddStatusUpdate from 'components/routes/animalProfile/AnimalProfileAddStatusUpdate'

const ProfileContainer = styled(Container)`
    padding: 40px 0;
    box-shadow: inset 0 0 0 1000px rgb(36 39 42 / 40%);
    align-items: center;
    justify-content: initial;
`

const ContentContainer = styled.div`
    padding: 18px 20px 30px 20px;
    && > * {
        margin-bottom: 10px;
    }
`

const StyledPaper = styled(Paper)`
    display: grid;
    grid-template-columns: 325px;
    @media (min-width: 576px) {
        grid-template-columns: 500px;
    }
    @media (min-width: 768px) {
        grid-template-columns: 650px;
    }
    @media (min-width: 992px) {
        grid-template-columns: 520px 375px;
        column-gap: 30px;
    }
    @media (min-width: 1200px) {
        grid-template-columns: 600px 375px;
    }
`

const AnimalProfileStatusContainer = styled(Paper)`
    && {
        width: 325px;
        margin-top: 10px;
        @media (min-width: 576px) {
            width: 500px;
        }
        @media (min-width: 768px) {
            width: 650px;
        }
        @media (min-width: 992px) {
            width: 925px;
        }
        @media (min-width: 1200px) {
            width: 1004px;
        }
    }
`

const AnimalProfile: FC = () => {
    const dispatch = useDispatch()
    const photographs = useSelector(state => state.animal.photographs)
    const isStaff = useSelector(state => state.user.isStaff)
    const [loading, error] = useFetchAnimalInfo()
    // Clear out the animal state when navigating away from this page.
    useEffect(() => {
        return () => {
            dispatch(clearAnimal())
        }
    }, [dispatch])

    if (error) return <div>Hey, you're not supposed to be here!</div>

    return (
        <ProfileContainer data-testid='animal-profile-container'>
            <StyledPaper elevation={3} square>
                <ImageViewer images={photographs} />
                <ContentContainer data-testid='animal-profile-content-container'>
                    <AnimalProfileButtonBar />
                    <AnimalProfileDetails loading={loading} />
                </ContentContainer>
            </StyledPaper>
            <AnimalProfileStatusContainer>
                <AnimalProfileStatusUpdateList />
                {isStaff && <AnimalProfileAddStatusUpdate />}
            </AnimalProfileStatusContainer>
        </ProfileContainer>
    )
}

export default AnimalProfile
