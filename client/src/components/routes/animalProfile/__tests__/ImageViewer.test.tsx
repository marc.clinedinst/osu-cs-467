import React from 'react'
import { fireEvent, render, screen } from 'app/testing-utils'
import ImageViewer from 'components/routes/animalProfile/ImageViewer'
import { ImageViewerProps } from '../types'

describe('ImageViewer Component Test', () => {
    beforeEach(() => {
        const images: ImageViewerProps['images'] = [
            { id: 1, url: 'https://dog.ceo/api/breeds/image/random' },
            { id: 2, url: 'https://dog.ceo/api/breeds/image/random' },
            { id: 3, url: 'https://dog.ceo/api/breeds/image/random' },
            { id: 4, url: 'https://dog.ceo/api/breeds/image/random' },
        ]

        render(<ImageViewer images={images} />)
    })

    test('render ImageViewer Component and confirm it has rendered correctly', () => {
        const imageViewer = screen.queryByTestId('image-viewer')
        const buttonBar = screen.queryByTestId('image-viewer-button-bar')
        const forward = screen.queryByTestId('image-viewer-forward')
        const back = screen.queryByTestId('image-viewer-back')
        const image = screen.queryAllByTestId('image-viewer-image')
        expect(imageViewer).toBeInTheDocument()
        expect(buttonBar).toBeInTheDocument()
        expect(forward).toBeInTheDocument()
        expect(back).toBeInTheDocument()
        expect(image.length).toBe(4)
        expect(image[0]).not.toHaveAttribute('hidden')
        expect(image[1]).toHaveAttribute('hidden')
        expect(image[2]).toHaveAttribute('hidden')
        expect(image[3]).toHaveAttribute('hidden')
    })

    test('click the forward button and check that things rendered correctly', () => {
        const forward = screen.queryByTestId('image-viewer-forward')
        const image = screen.queryAllByTestId('image-viewer-image')
        if (forward) fireEvent.click(forward)

        expect(image[0]).toHaveAttribute('hidden')
        expect(image[1]).not.toHaveAttribute('hidden')
        expect(image[2]).toHaveAttribute('hidden')
        expect(image[3]).toHaveAttribute('hidden')
    })
    test('click the back button twice and check that things rendered correctly', () => {
        const back = screen.queryByTestId('image-viewer-back')
        const forward = screen.queryByTestId('image-viewer-forward')
        const image = screen.queryAllByTestId('image-viewer-image')
        if (back && forward) {
            fireEvent.click(back)
            fireEvent.click(back)
        }
        expect(image[0]).toHaveAttribute('hidden')
        expect(image[1]).toHaveAttribute('hidden')
        expect(image[2]).not.toHaveAttribute('hidden')
        expect(image[3]).toHaveAttribute('hidden')
    })
})
