import React from 'react'
import {
    act,
    fireEvent,
    renderWithMockProvider,
    screen,
} from 'app/testing-utils'
import store from 'app/store'
import { clearAnimal, setAnimal } from 'ducks/animalSlice'
import { MockedResponse } from '@apollo/client/testing'
import { GraphQLError } from 'graphql'
import { clearUser, setUser } from 'ducks/userSlice'
import {
    DELETE_ANIMAL_STATUS_UPDATE,
    UPDATE_ANIMAL_STATUS_UPDATE,
} from 'gql/mutations'
import { AnimalType } from 'gql/types'
import AnimalProfileStatusUpdateListItem from '../AnimalProfileStatusUpdateListItem'
import client from 'app/client'
import { clearAnimals, setAnimals } from 'ducks/animalSearchSlice'
import { clearNotification } from 'ducks/notificationSlice'
jest.mock('app/client')

const mockAnimal: AnimalType = {
    breed: 'Corgi',
    statusUpdates: [
        { id: 1, statusText: 'A status update', created: '2020-02-02' },
        { id: 2, statusText: 'another update', created: '2020-02-02' },
    ],
    dateOfBirth: '01-01-2020',
    dateAdded: '01-01-2020',
    favoriteAnimals: [],
    description: 'A cute thing',
    photographs: [{ id: 1, url: 'testUrl' }],
    dispositions: [
        {
            id: 1,
            description: 'Happy',
        },
    ],
    gender: 'Female',
    kind: 'Dog',
    name: 'Boys',
    status: 'Pending',
    id: 1,
}

const mockPetSeeker = {
    username: 'test',
    id: 1,
    isStaff: false,
    favorites: [],
    emailOptIn: false,
}

const mockAdmin = {
    username: 'test',
    id: 1,
    isStaff: true,
    favorites: [],
    emailOptIn: false,
}

const props = {
    created: '2020-02-02',
    statusText: 'status',
    id: 1,
}

const mockFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: UPDATE_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
                statusText: 'new status',
            },
        },
        result: {
            errors: [new GraphQLError('test')],
        },
    },
    {
        request: {
            query: DELETE_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
            },
        },
        result: {
            errors: [new GraphQLError('test')],
        },
    },
]

const mockSuccess: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: DELETE_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
            },
        },
        result: {
            data: {
                deleteAnimalStatusUpdate: {
                    id: 1,
                },
            },
        },
    },
    {
        request: {
            query: UPDATE_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
                statusText: 'new status',
            },
        },
        result: {
            data: {
                updateAnimalStatusUpdate: {
                    statusUpdate: {
                        id: 1,
                        statusText: 'new status',
                        created: '2020-02-02',
                        animal: {
                            ...mockAnimal,
                            statusUpdates: [
                                {
                                    id: 1,
                                    statusText: 'new status',
                                    created: '2020-02-02',
                                },
                            ],
                        },
                    },
                },
            },
        },
    },
]

describe('AnimalProfileStatusUpdateListItem Component Test.', () => {
    afterEach(async () => {
        store.dispatch(clearUser())
        store.dispatch(clearAnimal())
        store.dispatch(clearNotification())
        store.dispatch(clearAnimals())
    })
    test('render ListItem component as pet seeker', async () => {
        store.dispatch(setUser(mockPetSeeker))
        store.dispatch(setAnimal(mockAnimal))
        renderWithMockProvider(
            <AnimalProfileStatusUpdateListItem {...props} />,
            mockSuccess
        )
        expect(screen.getByTestId('list-item-text')).toBeInTheDocument()
        expect(
            screen.queryByTestId('animal-profile-edit-button')
        ).not.toBeInTheDocument()
        expect(
            screen.queryByTestId('animal-profile-delete-button')
        ).not.toBeInTheDocument()
    })

    test('render ListItem component as an admin user', async () => {
        store.dispatch(setUser(mockAdmin))
        store.dispatch(setAnimal(mockAnimal))
        renderWithMockProvider(
            <AnimalProfileStatusUpdateListItem {...props} />,
            mockSuccess
        )
        expect(screen.getByTestId('list-item-text')).toBeInTheDocument()
        expect(
            screen.getByTestId('animal-profile-edit-button')
        ).toBeInTheDocument()
        expect(
            screen.getByTestId('animal-profile-delete-button')
        ).toBeInTheDocument()
    })

    test('As admin user, click on the delete button and successfully delete ', async () => {
        await act(async () => {
            client.query = jest.fn().mockReturnValue({ data: { animals: [] } })
            store.dispatch(setUser(mockAdmin))
            store.dispatch(setAnimal(mockAnimal))

            renderWithMockProvider(
                <AnimalProfileStatusUpdateListItem {...props} />,
                mockSuccess
            )
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        const deleteButton = screen.getByTestId('animal-profile-delete-button')
        await act(async () => {
            fireEvent.click(deleteButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().animal.statusUpdates.length).toBe(1)
    })

    test('As admin user, click on the delete button and error on delete ', async () => {
        await act(async () => {
            store.dispatch(setUser(mockAdmin))
            store.dispatch(setAnimal(mockAnimal))

            renderWithMockProvider(
                <AnimalProfileStatusUpdateListItem {...props} />,
                mockFail
            )
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        const deleteButton = screen.getByTestId('animal-profile-delete-button')
        await act(async () => {
            fireEvent.click(deleteButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        expect(store.getState().notification.type).toBe('error')
    })

    test('As admin user, click on the edit button and cancel edit', async () => {
        await act(async () => {
            store.dispatch(setUser(mockAdmin))
            store.dispatch(setAnimal(mockAnimal))

            renderWithMockProvider(
                <AnimalProfileStatusUpdateListItem {...props} />,
                mockSuccess
            )
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        const editButton = screen.getByTestId('animal-profile-edit-button')
        await act(async () => {
            fireEvent.click(editButton)
        })
        const form = screen.queryByTestId('list-item-form')
        const textfield = screen.queryByTestId('statusText-field')
        const submitButton = screen.queryByTestId(
            'animal-profile-submit-button'
        )
        const cancelButton = screen.queryByTestId(
            'animal-profile-cancel-button'
        )
        const components = [form, textfield, submitButton, cancelButton]
        components.forEach(component => expect(component).toBeInTheDocument())
        await act(async () => {
            fireEvent.click(screen.getByTestId('animal-profile-cancel-button'))
        })
        components.forEach(component =>
            expect(component).not.toBeInTheDocument()
        )
    })

    test('As admin user, click on the edit button and successfully edit status update', async () => {
        await act(async () => {
            store.dispatch(setUser(mockAdmin))
            store.dispatch(setAnimal(mockAnimal))
            store.dispatch(setAnimals([mockAnimal, { ...mockAnimal, id: 2 }]))

            renderWithMockProvider(
                <AnimalProfileStatusUpdateListItem {...props} />,
                mockSuccess
            )
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        const editButton = screen.getByTestId('animal-profile-edit-button')
        await act(async () => {
            fireEvent.click(editButton)
        })
        const form = screen.getByTestId('list-item-form')
        const textfield = screen.getByTestId('statusText-field')
        const submitButton = screen.getByTestId('animal-profile-submit-button')
        const cancelButton = screen.getByTestId('animal-profile-cancel-button')
        const components = [form, textfield, submitButton, cancelButton]

        components.forEach(component => expect(component).toBeInTheDocument())
        await act(async () => {
            fireEvent.input(textfield, { target: { value: 'new status' } })
        })
        await act(async () => {
            fireEvent.submit(submitButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().animal.statusUpdates[0].statusText).toBe(
            'new status'
        )
    })

    test('As admin user, click on the edit button and error on edit status update', async () => {
        await act(async () => {
            store.dispatch(setUser(mockAdmin))
            store.dispatch(setAnimal(mockAnimal))

            renderWithMockProvider(
                <AnimalProfileStatusUpdateListItem {...props} />,
                mockFail
            )
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
        const editButton = screen.getByTestId('animal-profile-edit-button')
        await act(async () => {
            fireEvent.click(editButton)
        })
        const form = screen.getByTestId('list-item-form')
        const textfield = screen.getByTestId('statusText-field')
        const submitButton = screen.getByTestId('animal-profile-submit-button')
        const cancelButton = screen.getByTestId('animal-profile-cancel-button')
        const components = [form, textfield, submitButton, cancelButton]

        components.forEach(component => expect(component).toBeInTheDocument())
        await act(async () => {
            fireEvent.input(textfield, { target: { value: 'new status' } })
        })
        await act(async () => {
            fireEvent.submit(submitButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().notification.type).toBe('error')
    })
})
