import React from 'react'
import {
    act,
    fireEvent,
    renderWithMockProvider,
    screen,
} from 'app/testing-utils'
import store from 'app/store'
import { AnimalState, clearAnimal, setAnimal } from 'ducks/animalSlice'
import { MockedResponse } from '@apollo/client/testing'
import { GraphQLError } from 'graphql'
import { ADD_ANIMAL_STATUS_UPDATE } from 'gql/mutations'
import AnimalProfileAddStatusUpdate from '../AnimalProfileAddStatusUpdate'
import client from 'app/client'
jest.mock('app/client')

const mockFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ADD_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
                statusText: 'error',
            },
        },
        result: {
            errors: [new GraphQLError('error')],
        },
    },
]
const mockSuccess: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ADD_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
                statusText: '',
            },
        },
        result: {
            data: {
                addAnimalStatusUpdate: {
                    statusUpdate: {
                        id: 1,
                        created: '2020-02-02',
                        statusText: 'test',
                    },
                },
            },
        },
    },
    {
        request: {
            query: ADD_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
                statusText: 'test',
            },
        },
        result: {
            data: {
                addAnimalStatusUpdate: {
                    statusUpdate: {
                        id: 1,
                        created: '2020-02-02',
                        statusText: 'test',
                    },
                },
            },
        },
    },
]

const animalMockState: AnimalState = {
    breed: 'Corgi',
    statusUpdates: [
        { id: 1, statusText: 'A status update', created: '2020-02-02' },
    ],
    dateOfBirth: '01-01-2020',
    dateAdded: '01-01-2020',
    favoriteAnimals: [],
    description: 'A cute thing',
    photographs: [{ id: 1, url: 'testUrl' }],
    dispositions: [
        {
            id: 1,
            description: 'Happy',
        },
    ],
    gender: 'Female',
    kind: 'Dog',
    name: 'Boys',
    status: 'Pending',
    id: 1,
}
describe('AnimalProfileAddStatusUpdate Component Tests', () => {
    afterEach(() => {
        store.dispatch(clearAnimal())
    })
    test('render AnimalProfileAddStatusUpdate Component and confirm it has rendered', async () => {
        await act(async () => {
            store.dispatch(setAnimal(animalMockState))
            await renderWithMockProvider(
                <AnimalProfileAddStatusUpdate />,
                mockSuccess
            )
        })
        const statusUpdateForm = screen.getByTestId('new-status-update-form')
        const textfield = screen.getByTestId('statusUpdate-field')
        const submitButton = screen.getByTestId(
            'new-status-update-form-submit-button'
        )
        expect(statusUpdateForm).toBeInTheDocument()
        expect(textfield).toBeInTheDocument()
        expect(submitButton).toBeInTheDocument()
        expect(submitButton).toBeDisabled()
    })
    test('submit status update successfully.', async () => {
        client.query = jest.fn().mockReturnValue([])
        await act(async () => {
            store.dispatch(setAnimal(animalMockState))
            await renderWithMockProvider(
                <AnimalProfileAddStatusUpdate />,
                mockSuccess
            )
        })
        const textfield = screen.getByTestId('statusUpdate-field')
        const submitButton = screen.getByTestId(
            'new-status-update-form-submit-button'
        )
        await act(async () => {
            await fireEvent.change(textfield, { target: { value: 'test' } })
        })
        expect(store.getState().animal.statusUpdates.length).toBe(1)
        await act(async () => {
            await fireEvent.submit(submitButton)
        })
        expect(store.getState().animal.statusUpdates.length).toBe(2)
    })

    test('submit new status update with gql error', async () => {
        client.query = jest.fn().mockReturnValue([])
        await act(async () => {
            store.dispatch(setAnimal(animalMockState))
            await renderWithMockProvider(
                <AnimalProfileAddStatusUpdate />,
                mockFail
            )
        })
        const textfield = screen.getByTestId('statusUpdate-field')
        const submitButton = screen.getByTestId(
            'new-status-update-form-submit-button'
        )
        await act(async () => {
            await fireEvent.change(textfield, { target: { value: 'error' } })
        })
        expect(store.getState().animal.statusUpdates.length).toBe(1)
        await act(async () => {
            await fireEvent.submit(submitButton)
        })
        expect(store.getState().notification.type).toBe('error')
        expect(store.getState().animal.statusUpdates.length).toBe(1)
    })
    test('have validation trigger', async () => {
        client.query = jest.fn().mockReturnValue([])
        await act(async () => {
            store.dispatch(setAnimal(animalMockState))
            await renderWithMockProvider(
                <AnimalProfileAddStatusUpdate />,
                mockSuccess
            )
        })
        const textfield = screen.getByTestId('statusUpdate-field')
        const submitButton = screen.getByTestId(
            'new-status-update-form-submit-button'
        )
        expect(submitButton).toBeDisabled()
        await act(async () => {
            fireEvent.input(textfield, { target: { value: 'test' } })
        })
        expect(submitButton).toBeEnabled()
        await act(async () => {
            fireEvent.input(textfield, { target: { value: '' } })
        })
        expect(submitButton).toBeDisabled()
    })
})
