import React from 'react'
import { act, renderWithMockProvider, screen } from 'app/testing-utils'
import store from 'app/store'
import { AnimalState, clearAnimal, setAnimal } from 'ducks/animalSlice'
import AnimalProfile from 'components/routes/animalProfile/AnimalProfile'
import { Route } from 'react-router'
import { MockedResponse } from '@apollo/client/testing'
import { ANIMAL_BY_ID } from 'gql/queries'
import history from 'app/history'
import { GraphQLError } from 'graphql'
import { clearUser, setUser } from 'ducks/userSlice'
import { ADD_ANIMAL_STATUS_UPDATE } from 'gql/mutations'

const mockFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ANIMAL_BY_ID,
            variables: {
                id: 1,
            },
        },
        result: {
            errors: [new GraphQLError('test')],
        },
    },
]
const mocks: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ANIMAL_BY_ID,
            variables: {
                id: 1,
            },
        },
        result: {
            data: {
                animalById: {
                    breed: 'Corgi',
                    statusUpdates: [],
                    dateOfBirth: '01-01-2020',
                    dateAdded: '01-01-2020',
                    favoriteAnimals: [],
                    description: 'A cute thing',
                    photographs: [{ id: 1, url: 'testUrl' }],
                    dispositions: [
                        {
                            id: 1,
                            description: 'Happy',
                        },
                    ],
                    gender: 'Female',
                    kind: 'Dog',
                    name: 'Boys',
                    status: 'Pending',
                    id: 1,
                },
            },
        },
    },
    {
        request: {
            query: ADD_ANIMAL_STATUS_UPDATE,
            variables: {
                id: 1,
            },
        },
        result: {
            data: {
                animalById: {
                    breed: 'Corgi',
                    statusUpdates: [],
                    dateOfBirth: '01-01-2020',
                    dateAdded: '01-01-2020',
                    favoriteAnimals: [],
                    description: 'A cute thing',
                    photographs: [{ id: 1, url: 'testUrl' }],
                    dispositions: [
                        {
                            id: 1,
                            description: 'Happy',
                        },
                    ],
                    gender: 'Female',
                    kind: 'Dog',
                    name: 'Boys',
                    status: 'Pending',
                    id: 1,
                },
            },
        },
    },
]

const animalMockState: AnimalState = {
    breed: 'Corgi',
    statusUpdates: [
        { id: 1, statusText: 'A status update', created: '2020-02-02' },
    ],
    dateOfBirth: '01-01-2020',
    dateAdded: '01-01-2020',
    favoriteAnimals: [],
    description: 'A cute thing',
    photographs: [{ id: 1, url: 'testUrl' }],
    dispositions: [
        {
            id: 1,
            description: 'Happy',
        },
    ],
    gender: 'Female',
    kind: 'Dog',
    name: 'Boys',
    status: 'Pending',
    id: 1,
}
describe('AnimalProfile Component Test with data loaded.', () => {
    afterEach(async () => {
        await act(async () => {
            store.dispatch(clearAnimal())
            store.dispatch(clearUser())
        })
    })
    test('render AnimalProfile Component and confirm it has rendered', async () => {
        await act(async () => {
            history.push('/animal/1')
            store.dispatch(setAnimal(animalMockState))
            store.dispatch(
                setUser({
                    username: 'test',
                    id: 1,
                    isStaff: true,
                    favorites: [],
                    emailOptIn: false,
                })
            )
            await renderWithMockProvider(
                <Route exact path='/animal/:id'>
                    <AnimalProfile />
                </Route>,
                mocks
            )
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        const animalProfileContainer = screen.queryByTestId(
            'animal-profile-container'
        )
        const imageViewer = screen.queryByTestId('image-viewer')
        const contentContainer = screen.queryByTestId(
            'animal-profile-content-container'
        )
        expect(animalProfileContainer).toBeInTheDocument()
        expect(imageViewer).toBeInTheDocument()
        expect(contentContainer).toBeInTheDocument()
    })
    test('render AnimalProfile Component and confirm it has rendered with no animal in state', async () => {
        expect(store.getState().animal.id).toBe(undefined)
        await act(async () => {
            history.push('/animal/1')
            await renderWithMockProvider(
                <Route exact path='/animal/:id'>
                    <AnimalProfile />
                </Route>,
                mocks
            )
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        expect(store.getState().animal.id).toBe(1)
    })
    test('render AnimalProfile Component and have animals by id mutation fail', async () => {
        await act(async () => {
            history.push('/animal/1')
            await renderWithMockProvider(
                <Route exact path='/animal/:id'>
                    <AnimalProfile />
                </Route>,
                mockFail
            )
            await new Promise(resolve => setTimeout(resolve, 100))
        })
        expect(
            screen.getByText(/Hey, you're not supposed to be here!/)
        ).toBeInTheDocument()
    })
})
