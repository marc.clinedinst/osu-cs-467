import React from 'react'
import {
    fireEvent,
    render,
    screen,
    act,
    renderWithMockProvider,
} from 'app/testing-utils'
import AnimalProfileButtonBar from '../AnimalProfileButtonBar'
import { clearUser, setFavorites, setUser } from 'ducks/userSlice'
import { clearAnimal, setAnimal } from 'ducks/animalSlice'
import client from 'app/client'
import { MockedResponse } from '@apollo/client/testing'
import { DELETE_ANIMAL, TOGGLE_FAVORITE_ANIMAL } from 'gql/mutations'
import { GraphQLError } from 'graphql'
import store from 'app/store'
import { clearNotification } from 'ducks/notificationSlice'
import { AnimalType } from 'gql/types'

jest.mock('app/client')
const mockQuery = client.query as jest.MockedFunction<typeof client.query>

const mockDeleteSuccess: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: DELETE_ANIMAL,
            variables: {
                id: 1,
            },
        },
        result: { data: { deleteAnimal: { id: 1 } } },
    },
]

const mockDeleteFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: DELETE_ANIMAL,
            variables: {
                id: 1,
            },
        },
        result: {
            errors: [new GraphQLError('invalid test')],
            data: {},
        },
    },
]

const mockFavoriteSuccess: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_FAVORITE_ANIMAL,
            variables: {
                id: 1,
            },
        },
        result: {
            data: { toggleFavoriteAnimal: { id: 1 } },
        },
    },
]

const mockFavoriteFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_FAVORITE_ANIMAL,
            variables: {
                id: 1,
            },
        },
        result: {
            errors: [new GraphQLError('error')],
        },
    },
]

const mockAnimal: AnimalType = {
    id: 1,
    photographs: [],
    name: 'Toto',
    description: 'test',
    breed: 'test breed',
    dateAdded: '2020-02-02',
    dateOfBirth: '2020-02-02',
    gender: 'Female',
    dispositions: [],
    kind: 'Cat',
    favoriteAnimals: [],
    status: 'Adopted',
    statusUpdates: [],
}

const mockUser = {
    id: 1,
    username: 'test',
    isStaff: true,
    favorites: [],
}

describe('AnimalProfileButtonBar Component Tests', () => {
    afterEach(() => {
        store.dispatch(clearUser())
        store.dispatch(clearNotification())
        store.dispatch(clearAnimal())
    })

    test('click on delete button - success', async () => {
        mockQuery.mockReturnValue(
            new Promise(resolve => {
                resolve({
                    networkStatus: 7,
                    loading: false,
                    data: {
                        animals: [mockAnimal],
                    },
                })
            })
        )
        await act(async () => {
            renderWithMockProvider(
                <AnimalProfileButtonBar />,
                mockDeleteSuccess
            )
            await store.dispatch(setUser(mockUser))
            await store.dispatch(setAnimal(mockAnimal))
            const deleteButton = screen.queryByTestId(
                'animal-profile-delete-button'
            )
            expect(deleteButton).toBeInTheDocument()
            if (deleteButton) await fireEvent.click(deleteButton)
            await new Promise(resolve => setTimeout(resolve, 100))
            expect(store.getState().notification.type).toBe('success')
            expect(mockQuery).toBeCalled()
        })
    })
    test('click on delete button - error', async () => {
        await act(async () => {
            renderWithMockProvider(<AnimalProfileButtonBar />, mockDeleteFail)
            await store.dispatch(setUser(mockUser))
            await store.dispatch(setAnimal(mockAnimal))
            const deleteButton = screen.queryByTestId(
                'animal-profile-delete-button'
            )
            expect(deleteButton).toBeInTheDocument()
            if (deleteButton) await fireEvent.click(deleteButton)
            await new Promise(resolve => setTimeout(resolve, 100))
            expect(store.getState().notification.type).toBe('error')
        })
    })
    test('click on edit button', () => {
        store.dispatch(setUser(mockUser))
        render(<AnimalProfileButtonBar />)
        expect(store.getState().customDialog.type).toBe(null)
        fireEvent.click(screen.getByTestId('animal-profile-edit-button'))
        expect(store.getState().customDialog.type).toBe('animalEdit')
    })

    test('render the AnimalProfileButtonBar with a non-favorited animal.', () => {
        store.dispatch(setFavorites([]))
        store.dispatch(setAnimal(mockAnimal))
        render(<AnimalProfileButtonBar />)
        const buttonBar = screen.queryByTestId('animal-profile-button-bar')
        const favoriteButton = screen.queryByTestId('favorite-button')
        const favoriteIcon = screen.queryByTestId('favorite-icon')
        const favoriteBorderIcon = screen.queryByTestId('favorite-border-icon')
        const shareButton = screen.queryByTestId('share-button')
        const shareIcon = screen.queryByTestId('share-icon')
        expect(buttonBar).toBeInTheDocument()
        expect(favoriteButton).toBeInTheDocument()
        expect(favoriteIcon).not.toBeInTheDocument()
        expect(favoriteBorderIcon).toBeInTheDocument()
        expect(shareButton).toBeInTheDocument()
        expect(shareIcon).toBeInTheDocument()
    })

    test('render the AnimalProfileButtonBar with a favorited animal', async () => {
        store.dispatch(setFavorites([mockAnimal]))
        store.dispatch(setAnimal(mockAnimal))

        await render(<AnimalProfileButtonBar />)

        expect(screen.queryByTestId('favorite-icon')).toBeInTheDocument()

        jest.clearAllMocks()
    })

    test('click on like button and have a successful transaction.', async () => {
        client.query = jest
            .fn()
            .mockReturnValueOnce({ data: { favoriteAnimals: [mockAnimal] } })

        await act(async () => {
            renderWithMockProvider(
                <AnimalProfileButtonBar />,
                mockFavoriteSuccess
            )
            store.dispatch(setAnimal(mockAnimal))
            store.dispatch(setUser(mockUser))
        })
        expect(store.getState().user.favorites?.length).toBe(0)
        const favoriteButton = screen.getByTestId('favorite-button')
        await act(async () => {
            fireEvent.click(favoriteButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().user.favorites?.length).toBe(1)
    })

    test('click on like button and have getFavorites thunk fail', async () => {
        client.query = jest.fn().mockImplementationOnce(() => {
            throw new Error('oh no')
        })

        await act(async () => {
            renderWithMockProvider(
                <AnimalProfileButtonBar />,
                mockFavoriteSuccess
            )
            store.dispatch(setAnimal(mockAnimal))
            store.dispatch(setUser(mockUser))
        })
        expect(store.getState().user.favorites?.length).toBe(0)
        const favoriteButton = screen.getByTestId('favorite-button')
        await act(async () => {
            fireEvent.click(favoriteButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().notification.type).toBe('error')
    })

    test('click on like button and have toggleFavorites mutation fail', async () => {
        client.query = jest.fn().mockImplementationOnce(() => {
            throw new Error('oh no')
        })

        await act(async () => {
            renderWithMockProvider(<AnimalProfileButtonBar />, mockFavoriteFail)
            store.dispatch(setAnimal(mockAnimal))
            store.dispatch(setUser(mockUser))
        })
        expect(store.getState().user.favorites?.length).toBe(0)
        const favoriteButton = screen.getByTestId('favorite-button')
        await act(async () => {
            fireEvent.click(favoriteButton)
        })
        await new Promise(resolve => setTimeout(resolve, 100))
        expect(store.getState().notification.type).toBe('error')
    })
})
