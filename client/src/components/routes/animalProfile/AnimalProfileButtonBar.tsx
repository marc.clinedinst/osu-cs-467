import React, { FC } from 'react'
import { useDispatch, useSelector } from 'app/store'
import { useHistory } from 'react-router-dom'
import { useMutation } from '@apollo/client'
import { IconButton } from '@material-ui/core'
import {
    Delete,
    Favorite,
    FavoriteBorder,
    Share,
    Edit,
} from '@material-ui/icons'
import { DELETE_ANIMAL, TOGGLE_FAVORITE_ANIMAL } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'
import getAnimals from 'thunks/getAnimals'
import { openDialog } from 'ducks/customDialogSlice'
import getFavorites from 'thunks/getFavorites'
import { ToggleFavoriteAnimalReponse } from 'gql/types'

const AnimalProfileButtonBar: FC = () => {
    const dispatch = useDispatch()
    const id = useSelector(state => state.animal.id)
    const { isStaff, favorites } = useSelector(state => state.user)
    const history = useHistory()
    const favoritesIds = favorites?.map(({ id }) => id) ?? []

    const [deleteAnimal] = useMutation<{ id: number }, { id: number }>(
        DELETE_ANIMAL,
        {
            variables: { id },
            onCompleted: () => {
                dispatch(
                    openNotification({
                        type: 'success',
                        message: `Animal removed`,
                    })
                )
                dispatch(getAnimals())
                history.push('/search')
            },
            onError: error =>
                dispatch(
                    openNotification({
                        type: 'error',
                        message: error.message,
                    })
                ),
        }
    )

    const [submitToggleFavorite] = useMutation<ToggleFavoriteAnimalReponse>(
        TOGGLE_FAVORITE_ANIMAL,
        {
            variables: { id },
            onCompleted: () => dispatch(getFavorites()),
            onError: err =>
                dispatch(
                    openNotification({ type: 'error', message: err.message })
                ),
        }
    )

    const handleClickToggleFavorite = () => submitToggleFavorite()
    const handleDelete = () => deleteAnimal()

    return (
        <div data-testid='animal-profile-button-bar'>
            <IconButton
                data-testid='favorite-button'
                edge='start'
                onClick={handleClickToggleFavorite}
            >
                {favoritesIds.includes(id) ? (
                    <Favorite data-testid='favorite-icon' color='secondary' />
                ) : (
                    <FavoriteBorder data-testid='favorite-border-icon' />
                )}
            </IconButton>
            {isStaff && (
                <>
                    <IconButton
                        onClick={() => {
                            dispatch(
                                openDialog({
                                    type: 'animalEdit',
                                    isFullscreen: true,
                                })
                            )
                        }}
                        data-testid='animal-profile-edit-button'
                    >
                        <Edit data-testid='animal-profile-edit-icon' />
                    </IconButton>
                    <IconButton
                        onClick={handleDelete}
                        data-testid='animal-profile-delete-button'
                    >
                        <Delete data-testid='delete-icon' />
                    </IconButton>
                </>
            )}
            <IconButton data-testid='share-button' edge='end'>
                <Share data-testid='share-icon' />
            </IconButton>
        </div>
    )
}
export default AnimalProfileButtonBar
