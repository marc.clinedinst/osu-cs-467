import { PhotographType } from 'gql/types'

export interface ImageViewerProps {
    images: Omit<PhotographType, 'animal'>[]
    isEditable?: boolean
    onDelete?: (arg: number[]) => void
}

export type UseParamsValues = Record<string, string | undefined>

export interface AnimalProfileDetailsProps {
    loading: boolean
}

export interface AnimalProfileStatusUpdateDeleteButtonProps {
    id: number
}

export interface AnimalProfileStatusUpdateListItemProps {
    created: string
    statusText: string
    id: number
}

export interface UpdateAnimalStatusFormValues {
    statusText: string
}
