import React, { FC } from 'react'
import { useSelector } from 'app/store'
import styled from 'styled-components'

import { Divider, List, ListSubheader } from '@material-ui/core'
import Typography from 'components/common/StyledTypography'
import AnimalProfileStatusUpdateListItem from './AnimalProfileStatusUpdateListItem'

const StyledList = styled(List)`
    && {
        margin-top: 20px;
        padding-bottom: 0px;
        @media (min-width: 992px) {
            grid-column-start: 1;
            grid-column-end: 3;
        }
        @media (min-width: 1200px) {
            grid-column-start: 1;
            grid-column-end: 3;
        }
    }
` as typeof List

const StyledText = styled(Typography)`
    && {
        padding-bottom: 5px;
    }
`

const AnimalProfileStatusUpdates: FC = () => {
    const { statusUpdates } = useSelector(state => state.animal)
    return (
        <StyledList
            subheader={
                <>
                    <ListSubheader disableSticky>
                        <StyledText $textColor='black' variant='h5'>
                            Status Updates
                        </StyledText>
                    </ListSubheader>
                    <Divider />
                </>
            }
        >
            {statusUpdates?.map(props => (
                <AnimalProfileStatusUpdateListItem key={props.id} {...props} />
            ))}
        </StyledList>
    )
}
export default AnimalProfileStatusUpdates
