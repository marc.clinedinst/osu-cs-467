import React, { FunctionComponent } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { useDispatch } from 'app/store'
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
    IconButton,
} from '@material-ui/core'
import { Share } from '@material-ui/icons'
import { AnimalCardProps } from 'components/routes/animalSearch/types'
import { DEFAULT_IMAGE_PAW_PRINT } from 'constants/constants'
import { setAnimal } from 'ducks/animalSlice'
import Typography from 'components/common/StyledTypography'

const StyledCard = styled(Card)`
    && {
        width: 100%;
    }
`

const StyledCardMedia = styled(CardMedia)`
    && {
        height: 250px;
    }
`
const StyledCardActions = styled(CardActions)`
    && {
        display: flex;
        & button:first-child {
            margin-right: auto;
        }
    }
`

const StyledCardContent = styled(CardContent)`
    && {
        max-height: 250px;
        height: 250px;
    }
`

const AnimalCard: FunctionComponent<AnimalCardProps> = props => {
    const dispatch = useDispatch()
    const history = useHistory()

    const { breed, gender, description, name, photographs } = props
    const handleClick = () => {
        dispatch(setAnimal(props))
        history.push(`/animal/${props.id}`)
    }
    const image = photographs.length
        ? props.photographs[0].url
        : DEFAULT_IMAGE_PAW_PRINT

    return (
        <StyledCard data-testid='animal-card'>
            <StyledCardMedia image={image} data-testid='animal-card-image' />
            <StyledCardContent>
                <Typography
                    data-testid='animal-card-name'
                    gutterBottom
                    variant='h5'
                >
                    {name}
                </Typography>
                <Typography
                    variant='subtitle2'
                    color='textSecondary'
                    data-testid='animal-card-sex-breed'
                >
                    {`${gender}, ${breed}`}
                </Typography>
                <Typography
                    data-testid='animal-card-description'
                    variant='body2'
                >
                    {description}
                </Typography>
            </StyledCardContent>
            <StyledCardActions>
                <Button
                    size='small'
                    color='primary'
                    data-testid='animal-card-view'
                    onClick={handleClick}
                >
                    View
                </Button>
                <IconButton
                    size='small'
                    color='primary'
                    data-testid='animal-card-share'
                >
                    <Share />
                </IconButton>
            </StyledCardActions>
        </StyledCard>
    )
}

export default AnimalCard
