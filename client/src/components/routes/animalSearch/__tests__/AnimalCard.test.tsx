import React from 'react'
import { fireEvent, render, screen } from 'app/testing-utils'
import AnimalCard from 'components/routes/animalSearch/AnimalCard'
import { AnimalCardProps } from '../types'
import store from 'app/store'
import { BrowserRouter } from 'react-router-dom'
import { clearUser, setUser } from 'ducks/userSlice'

describe('AnimalCard Component Test', () => {
    beforeEach(() => {
        const animalCardProps: AnimalCardProps[] = [
            {
                breed: 'Corgi',
                favoriteAnimals: [],
                photographs: [{ id: 1, url: 'testUrl' }],
                statusUpdates: [],
                dateOfBirth: '01-01-2020',
                dateAdded: '01-01-2020',
                description: 'A cute thing',
                dispositions: [
                    {
                        id: 1,
                        description: 'Happy',
                    },
                ],
                gender: 'Female',
                kind: 'Dog',
                name: 'Boys',
                status: 'Pending',
                id: 1,
            },
        ]
        const components = animalCardProps.map(props => {
            return <AnimalCard key={1} {...props} />
        })
        render(<BrowserRouter>{components}</BrowserRouter>)
    })
    afterEach(() => store.dispatch(clearUser()))

    test('render component as not staff user', () => {
        store.dispatch(
            setUser({
                username: 'test',
                id: 1,
                isStaff: false,
                favorites: [],
                emailOptIn: false,
            })
        )
        const animalCard = screen.queryByTestId('animal-card')
        const viewButton = screen.queryByTestId('animal-card-view')
        expect(viewButton).toBeInTheDocument()
        expect(animalCard).toBeInTheDocument()
    })

    test('render component as staff user', () => {
        store.dispatch(
            setUser({
                username: 'test',
                id: 1,
                isStaff: true,
                favorites: [],
                emailOptIn: false,
            })
        )
        const animalCard = screen.queryByTestId('animal-card')
        const viewButton = screen.queryByTestId('animal-card-view')
        expect(viewButton).toBeInTheDocument()
        expect(animalCard).toBeInTheDocument()
    })

    test('click on the view button to view the animals profile', async () => {
        const viewButton = screen.queryByTestId('animal-card-view')
        if (viewButton) {
            await fireEvent.click(viewButton)
            expect(store.getState().animal.id).toBe(1)
        }
    })
})

describe('AnimalCard Component Test', () => {
    test('render AnimalCard without an image', () => {
        const animalCardProps: AnimalCardProps[] = [
            {
                breed: 'Corgi',
                favoriteAnimals: [],

                dateOfBirth: '01-01-2020',
                photographs: [],
                statusUpdates: [],
                dateAdded: '01-01-2020',
                description: 'A cute thing',
                dispositions: [
                    {
                        id: 1,
                        description: 'Happy',
                    },
                ],
                gender: 'Female',
                kind: 'Dog',
                name: 'Boys',
                status: 'Pending',
                id: 1,
            },
        ]
        const components = animalCardProps.map(props => {
            return <AnimalCard key={1} {...props} />
        })
        render(<BrowserRouter>{components}</BrowserRouter>)
        const animalCard = screen.queryByTestId('animal-card')

        const viewButton = screen.queryByTestId('animal-card-view')
        expect(viewButton).toBeInTheDocument()
        expect(animalCard).toBeInTheDocument()
    })
})
