import React from 'react'
import { screen, render } from 'app/testing-utils'
import AnimalSearch from 'components/routes/animalSearch/AnimalSearch'
import client from 'app/client'
import store from 'app/store'
import { clearAnimals } from 'ducks/animalSearchSlice'
import { clearNotification } from 'ducks/notificationSlice'
jest.mock('app/client')

describe('AnimalSearch Component Tests', () => {
    afterEach(() => {
        store.dispatch(clearAnimals())
        store.dispatch(clearNotification())
        jest.clearAllMocks()
    })
    test('renders the AnimalSearch component and confirm it has rendered with animals', async () => {
        client.query = jest
            .fn()
            .mockReturnValueOnce({
                data: {
                    animals: [
                        {
                            description: 'description',
                            name: 'name',
                            gender: 'Male',
                            breed: 'breed',
                            dateOfBirth: '01-01-2020',
                            dateAdded: '01-01-2020',
                            photographs: [],
                            dispositions: [],
                            kind: 'Dog',
                            status: 'Adopted',
                            id: 1,
                        },
                    ],
                },
            })
            .mockImplementationOnce(() => {
                throw new Error()
            })
        await render(<AnimalSearch />)

        const animalCards = screen.getAllByTestId('animal-card')
        const contentContainer = screen.getByTestId('animal-search-container')
        expect(contentContainer).toBeInTheDocument()
        expect(animalCards.length).toBe(1)
    })

    test('renders the AnimalSearch component and confirm it has rendered with no animals', async () => {
        client.query = jest
            .fn()
            .mockReturnValueOnce({
                data: {
                    animals: [],
                },
            })
            .mockImplementationOnce(() => {
                throw new Error()
            })
        await render(<AnimalSearch />)

        const animalCards = screen.queryAllByTestId('animal-card')
        const contentContainer = screen.getByTestId('animal-search-container')
        const alert = screen.getByText(
            'No results were found that met your criteria.'
        )
        expect(contentContainer).toBeInTheDocument()
        expect(animalCards.length).toBe(0)
        expect(alert).toBeInTheDocument()
    })

    test('renders the AnimalSearch component and confirm it has errored.', async () => {
        client.query = jest.fn().mockImplementationOnce(() => {
            throw new Error()
        })
        await render(<AnimalSearch />)
        const animalCards = screen.queryAllByTestId('animal-card')
        expect(animalCards.length).toBe(0)
        expect(store.getState().notification.type).toBe('error')
    })
})
