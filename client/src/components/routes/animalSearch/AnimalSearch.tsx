import React, { FunctionComponent, useEffect } from 'react'
import AnimalCard from 'components/routes/animalSearch/AnimalCard'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'app/store'
import { Alert } from '@material-ui/lab'
import { SCREEN_SIZE } from 'constants/constants'
import getAnimals from 'thunks/getAnimals'

const ContentGridContainer = styled.div`
    display: grid;
    justify-content: center;
    grid-template-columns: 1fr;
    row-gap: 30px;
    padding: 60px 15px;
    @media (min-width: ${SCREEN_SIZE.small}) {
        grid-template-columns: 500px;
    }
    @media (min-width: ${SCREEN_SIZE.medium}) {
        grid-template-columns: 325px 325px;
        column-gap: 30px;
        row-gap: 60px;
    }
    @media (min-width: ${SCREEN_SIZE.large}) {
        grid-template-columns: 450px 450px;
        column-gap: 60px;
        row-gap: 60px;
    }
    @media (min-width: ${SCREEN_SIZE.xLarge}) {
        grid-template-columns: 325px 325px 325px;
        column-gap: 60px;
        row-gap: 60px;
    }
`

const StyledAlert = styled(Alert)`
    && {
        height: 48px;
        width: 100%;
        align-items: center;
    }
`

const AnimalSearch: FunctionComponent = () => {
    const dispatch = useDispatch()

    const animals = useSelector(state => state.animals)

    useEffect(() => {
        if (animals === null) {
            dispatch(getAnimals())
        }
    }, [dispatch, animals])

    return (
        <ContentGridContainer data-testid='animal-search-container'>
            {animals?.length !== 0 ? (
                animals?.map(props => <AnimalCard {...props} key={props.id} />)
            ) : (
                <StyledAlert severity='info'>
                    No results were found that met your criteria.
                </StyledAlert>
            )}
        </ContentGridContainer>
    )
}

export default AnimalSearch
