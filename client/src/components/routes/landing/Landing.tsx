import React, { FunctionComponent } from 'react'
import styled from 'styled-components'
import { Button } from '@material-ui/core'
import { useFetchImages } from 'hooks/useFetchImages'
import { useDispatch } from 'react-redux'
import { openDialog } from 'ducks/customDialogSlice'
import Container from 'components/common/Container'
import StyledTypography from 'components/common/StyledTypography'

const TitleTypography = styled(StyledTypography)`
    && {
        text-shadow: 4px 4px 6px #333;
    }
`

const LandingContainer = styled(Container)`
    background: url(${({ url }: { url: string }) => url ?? ''}) no-repeat center
        center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    align-items: center;
    box-shadow: inset 0 0 0 1000px rgb(36 39 42 / 60%);
`

const StyledLandingButton = styled(Button)`
    && {
        width: 325px;
        margin-bottom: 20px;
        padding: 20px;
    }
`

const LandingButtonContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0;
    div {
        width: 100%;
        min-width: 250px;
        margin-bottom: 10px;
    }
`

const LandingTextContainer = styled.div`
    margin-bottom: 80px;
`

const Landing: FunctionComponent = () => {
    const [image] = useFetchImages(1)
    const dispatch = useDispatch()

    const handleLogin = () => dispatch(openDialog('login'))
    const handleRegister = () => dispatch(openDialog('register'))

    return (
        <LandingContainer data-testid='banner' url={image}>
            <LandingTextContainer>
                <div>
                    <TitleTypography
                        data-testid='banner-title'
                        variant='h3'
                        $textColor='white'
                    >
                        Paw Swipe
                    </TitleTypography>
                </div>
                <div>
                    <StyledTypography
                        variant='subtitle1'
                        $textColor='white'
                        data-testid='banner-subtitle'
                    >
                        A dating app for animal adpotion
                    </StyledTypography>
                </div>
            </LandingTextContainer>
            <LandingButtonContainer>
                <StyledLandingButton
                    variant='contained'
                    color='primary'
                    size='large'
                    onClick={handleLogin}
                    data-testid='banner-login-button'
                >
                    Login
                </StyledLandingButton>
                <StyledLandingButton
                    variant='contained'
                    color='primary'
                    size='large'
                    onClick={handleRegister}
                    data-testid='banner-register-button'
                >
                    I'm New Here
                </StyledLandingButton>
            </LandingButtonContainer>
        </LandingContainer>
    )
}

export default Landing
