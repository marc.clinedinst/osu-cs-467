import React from 'react'
import { fireEvent, renderWithRouter, screen } from 'app/testing-utils'
import Landing from '../Landing'
import store from 'app/store'
import { closeDialog } from 'ducks/customDialogSlice'
describe('Landing Tests', () => {
    beforeEach(() => {
        renderWithRouter(<Landing />)
    })

    test('render Landing Component and check that all main components are there', () => {
        const banner = screen.getByTestId(/banner-title/i)
        const loginButton = screen.getByTestId(/banner-login-button/i)
        const registerButton = screen.getByTestId(/banner-register-button/i)
        expect(banner).toBeInTheDocument()
        expect(loginButton).toBeInTheDocument()
        expect(registerButton).toBeInTheDocument()
    })

    test('click on the login button.', () => {
        const loginButton = screen.getByTestId(/banner-login-button/i)
        fireEvent.click(loginButton)
        expect(store.getState().customDialog.type).toEqual('login')
        store.dispatch(closeDialog())
    })

    test('click on the register button.', () => {
        const registerButton = screen.queryByTestId('banner-register-button')
        if (registerButton) fireEvent.click(registerButton)
        expect(store.getState().customDialog.type).toEqual('register')
        store.dispatch(closeDialog())
    })
})
