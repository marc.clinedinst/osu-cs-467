import React, { FunctionComponent } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { useMutation } from '@apollo/client'
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core'
import { closeDialog } from 'ducks/customDialogSlice'
import { TOKEN_AUTH } from 'gql/mutations'
import { TokenAuthOutput } from 'gql/types'
import history from 'app/history'
import { setUser } from 'ducks/userSlice'
import { FormValues } from './types'
import ControlledText from './ControlledText'
import { openNotification } from 'ducks/notificationSlice'

const LoginDialogContent: FunctionComponent = () => {
    const { errors, handleSubmit, register } = useForm()

    const dispatch = useDispatch()

    const [fetchTokenAuth] = useMutation<TokenAuthOutput, FormValues>(
        TOKEN_AUTH,
        {
            onCompleted: ({ tokenAuth }) => {
                localStorage.setItem('jwtToken', tokenAuth.token)
                const { favoriteAnimals, ...rest } = tokenAuth.user
                dispatch(setUser({ favorites: favoriteAnimals, ...rest }))
                dispatch(closeDialog())
                history.push('/search')
            },
            onError: err =>
                dispatch(
                    openNotification({ type: 'error', message: err.message })
                ),
        }
    )

    const onSubmit: SubmitHandler<FormValues> = ({ username, password }) => {
        fetchTokenAuth({
            variables: { username, password },
        })
    }

    return (
        <>
            <DialogTitle
                data-testid='login-dialog-header'
                id='form-dialog-title'
            >
                Login
            </DialogTitle>
            <form
                data-testid='login-dialog-login-form'
                onSubmit={handleSubmit(onSubmit)}
            >
                <DialogContent>
                    <ControlledText
                        name='username'
                        label='Username'
                        register={() => register({ required: true })}
                        errors={errors}
                        textFieldProps={{ autoFocus: true }}
                    />
                    <ControlledText
                        register={() => register({ required: true })}
                        name='password'
                        label='Password'
                        errors={errors}
                        textFieldProps={{ type: 'password' }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        data-testid='login-dialog-login-button'
                        type='submit'
                        color='primary'
                        variant='contained'
                    >
                        Login
                    </Button>
                </DialogActions>
            </form>
        </>
    )
}

export default LoginDialogContent
