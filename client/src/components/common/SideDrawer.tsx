import React, { FunctionComponent, useState } from 'react'
import {
    Divider,
    Drawer,
    FormControlLabel,
    IconButton,
    List,
    ListItem,
    ListItemText,
    ListSubheader,
    Switch,
} from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'app/store'
import { openDialog } from 'ducks/customDialogSlice'
import { clearUser, setUser } from 'ducks/userSlice'
import { clearFilters } from 'ducks/filtersSlice'
import { useHistory } from 'react-router-dom'
import { useMutation } from '@apollo/client'
import { TOGGLE_EMAIL_OPT_IN } from 'gql/mutations'
import { openNotification } from 'ducks/notificationSlice'

const StyledList = styled(List)`
    && {
        width: 250px;
    }
`
const StyledListItem = styled(ListItem)`
    && {
        text-decoration: none;
        text-transform: uppercase;
        color: black;
    }
`
const StyledLabel = styled(FormControlLabel)`
    && {
        margin: 0;
    }
`

const SideDrawer: FunctionComponent = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const [open, setOpen] = useState<boolean>(false)
    const user = useSelector(state => state.user)
    const { username, isStaff, emailOptIn } = user

    const [toggleEmailOptIn] = useMutation<{
        toggleEmailOptIn: { optedIn: boolean }
    }>(TOGGLE_EMAIL_OPT_IN, {
        onCompleted: ({ toggleEmailOptIn: { optedIn } }) => {
            dispatch(setUser({ ...user, emailOptIn: optedIn }))
            dispatch(
                openNotification({
                    type: 'success',
                    message: `Email notifications successfully opted ${
                        optedIn ? `in` : `out`
                    }!`,
                })
            )
        },
        onError: err =>
            dispatch(openNotification({ type: 'error', message: err.message })),
    })
    const toggleDrawer = (open: boolean) => () => setOpen(open)

    const sideDrawerList = () => (
        <div
            role='presentation'
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
            data-testid='nav-drawer-container'
        >
            <StyledList>
                {username && isStaff && (
                    <>
                        <ListSubheader
                            component='div'
                            id='nested-list-subheader'
                            data-testid='nav-drawer-admin-tools'
                        >
                            Admin Tools
                        </ListSubheader>
                        <StyledListItem
                            button
                            onClick={() => history.push('/add-animal')}
                            data-testid='nav-drawer-add-animal'
                        >
                            <ListItemText primary='add animal' />
                        </StyledListItem>
                        <Divider />
                    </>
                )}
                {username ? (
                    <>
                        <StyledListItem
                            button
                            onClick={() => history.push('/search')}
                            data-testid={'nav-drawer-search'}
                        >
                            <ListItemText primary={'search'} />
                        </StyledListItem>
                        <StyledListItem
                            button
                            onClick={() => history.push('/favorites')}
                            data-testid='nav-drawer-my-favorites'
                        >
                            <ListItemText primary='my favorites' />
                        </StyledListItem>
                        <StyledListItem
                            button
                            onClick={() => toggleEmailOptIn()}
                            data-testid='nav-drawer-toggle-email-opt-in'
                        >
                            <StyledLabel
                                control={
                                    <Switch
                                        checked={emailOptIn}
                                        name='emailOptIn'
                                        color='primary'
                                    />
                                }
                                label='Animal Update Emails'
                                labelPlacement='start'
                            />
                        </StyledListItem>
                        <Divider />
                        <StyledListItem
                            button
                            onClick={() => {
                                localStorage.removeItem('jwtToken')
                                dispatch(clearUser())
                                dispatch(clearFilters())
                                history.push('/')
                            }}
                            data-testid='nav-drawer-logout'
                        >
                            <ListItemText primary='logout' />
                        </StyledListItem>
                    </>
                ) : (
                    <>
                        <StyledListItem
                            button
                            onClick={() => dispatch(openDialog('login'))}
                            data-testid='nav-drawer-login'
                        >
                            <ListItemText primary='login' />
                        </StyledListItem>
                        <StyledListItem
                            button
                            onClick={() => dispatch(openDialog('register'))}
                            data-testid='nav-drawer-register'
                        >
                            <ListItemText primary='register' />
                        </StyledListItem>
                    </>
                )}
            </StyledList>
        </div>
    )

    return (
        <>
            <IconButton
                edge='end'
                color='inherit'
                aria-label='menu'
                data-testid='navbar-hamburger-menu'
                onClick={toggleDrawer(true)}
            >
                <Menu />
            </IconButton>

            <Drawer anchor='right' open={open} onClose={toggleDrawer(false)}>
                {sideDrawerList()}
            </Drawer>
        </>
    )
}
export default SideDrawer
