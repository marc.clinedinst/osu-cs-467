import React from 'react'
import {
    render,
    screen,
    fireEvent,
    act,
    renderWithMockProvider,
} from 'app/testing-utils'
import store from 'app/store'
import RegistrationDialogContent from '../RegistrationDialogContent'
import { clearUser } from 'ducks/userSlice'
import { clearNotification } from 'ducks/notificationSlice'
import { REGISTER_USER } from 'gql/mutations'
import { GraphQLError } from 'graphql'
import history from 'app/history'

const mockSuccess = [
    {
        request: {
            query: REGISTER_USER,
            variables: {
                confirmPassword: 'asdf',
                password: 'asdf',
                username: 'asdf',
                email: 'asdf@asdf.com',
            },
        },
        result: {
            data: {
                registerUser: {
                    token: 'a token',
                    user: {
                        id: 1,
                        username: 'asdf',
                        isStaff: false,
                    },
                },
            },
        },
    },
]
const mockError = [
    {
        request: {
            query: REGISTER_USER,
            variables: {
                confirmPassword: 'error',
                password: 'error',
                username: 'error',
                email: 'error@error.com',
            },
        },
        result: {
            errors: [new GraphQLError('test error')],
        },
    },
]

describe('RegistrationDialogContent', () => {
    beforeEach(() => {
        store.dispatch(clearUser())
        store.dispatch(clearNotification())
    })
    afterEach(() => {
        store.dispatch(clearUser())
        store.dispatch(clearNotification())
    })
    test('renders', () => {
        render(<RegistrationDialogContent />)
        const registerHeader = screen.queryByTestId('register-dialog-header')
        const emailField = screen.queryByTestId('email-field')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')
        const confirmPasswordField = screen.queryByTestId(
            'confirmPassword-field'
        )
        const registerButton = screen.queryByTestId(
            'register-dialog-register-button'
        )

        expect(registerHeader).toBeInTheDocument()
        expect(emailField).toBeInTheDocument()
        expect(usernameField).toBeInTheDocument()
        expect(passwordField).toBeInTheDocument()
        expect(confirmPasswordField).toBeInTheDocument()
        expect(registerButton).toBeInTheDocument()
    })

    test('fill out form and submit - returns successful response', async () => {
        renderWithMockProvider(<RegistrationDialogContent />, mockSuccess)
        const emailField = screen.queryByTestId('email-field')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')
        const confirmPasswordField = screen.queryByTestId(
            'confirmPassword-field'
        )
        const registerButton = screen.queryByTestId(
            'register-dialog-register-button'
        )
        if (
            emailField &&
            usernameField &&
            passwordField &&
            confirmPasswordField &&
            registerButton
        ) {
            await act(async () => {
                fireEvent.change(emailField, {
                    target: { value: 'asdf@asdf.com' },
                })
                fireEvent.change(usernameField, {
                    target: { value: 'asdf' },
                })
                fireEvent.change(passwordField, {
                    target: { value: 'asdf' },
                })
                fireEvent.change(confirmPasswordField, {
                    target: { value: 'asdf' },
                })
                fireEvent.submit(registerButton)
            })
        }
        await new Promise(resolve => setTimeout(resolve, 0))
        const errors = screen.queryAllByText(/required/i)
        const { username, id, isStaff } = store.getState().user
        const { type } = store.getState().customDialog

        expect(errors.length).toEqual(0)
        expect(username).toBe('asdf')
        expect(id).toBe(1)
        expect(isStaff).toBe(false)
        expect(type).toBe(null)
        expect(history.location.pathname).toEqual('/search')
    })
    test('fill out form and submit - returns GQL error', async () => {
        renderWithMockProvider(<RegistrationDialogContent />, mockError)
        const emailField = screen.queryByTestId('email-field')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')
        const confirmPasswordField = screen.queryByTestId(
            'confirmPassword-field'
        )
        const registerButton = screen.queryByTestId(
            'register-dialog-register-button'
        )
        if (
            emailField &&
            usernameField &&
            passwordField &&
            confirmPasswordField &&
            registerButton
        ) {
            await act(async () => {
                fireEvent.change(emailField, {
                    target: { value: 'error@error.com' },
                })
                fireEvent.change(usernameField, {
                    target: { value: 'error' },
                })
                fireEvent.change(passwordField, {
                    target: { value: 'error' },
                })
                fireEvent.change(confirmPasswordField, {
                    target: { value: 'error' },
                })
                fireEvent.submit(registerButton)
            })
            await new Promise(resolve => setTimeout(resolve, 0))
            const errors = screen.queryAllByText(/required/i)
            expect(errors.length).toEqual(0)
            expect(store.getState().user.username).not.toEqual('error')
            expect(store.getState().notification.type).toEqual('error')
            expect(store.getState().notification.message).toEqual('test error')
        }
    })
})
