import React from 'react'
import { fireEvent, render, screen } from 'app/testing-utils'
import store from 'app/store'
import Notification from '../Notification'
import { openNotification, clearNotification } from 'ducks/notificationSlice'

describe('Notifications Component Tests', () => {
    beforeEach(() =>
        store.dispatch(
            openNotification({ type: 'error', message: 'this is a test' })
        )
    )
    afterEach(() => store.dispatch(clearNotification()))
    test('render the Notification component and check that it rendered', () => {
        const { container } = render(<Notification />)
        const notificationPopup = screen.queryByTestId('notification')
        const closeButton = container.querySelector('button')
        expect(notificationPopup).toBeInTheDocument()
        expect(closeButton).toBeInTheDocument()
    })

    test('click the close button', () => {
        const { container } = render(<Notification />)
        const closeButton = container.querySelector('button')
        if (closeButton) {
            fireEvent.click(closeButton)
            expect(screen.queryByTestId('notification')).not.toBeInTheDocument()
        }
    })
})
