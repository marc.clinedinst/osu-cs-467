import React from 'react'
import {
    render,
    screen,
    fireEvent,
    act,
    renderWithMockProvider,
} from 'app/testing-utils'
import store from 'app/store'
import AnimalEditDialogContent from 'components/common/AnimalEditDialogContent'
import { clearUser, setUser } from 'ducks/userSlice'
import { clearNotification } from 'ducks/notificationSlice'
import { AnimalState, clearAnimal, setAnimal } from 'ducks/animalSlice'
import { closeDialog } from 'ducks/customDialogSlice'
import { MockedResponse } from '@apollo/client/testing'
import { ANIMAL_DISPOSITIONS } from 'gql/queries'
import client from 'app/client'
import { clearAnimals } from 'ducks/animalSearchSlice'
jest.mock('app/client')

const mockDispositions: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: ANIMAL_DISPOSITIONS,
        },
        result: {
            data: { animalDispositions: [{ id: 1, description: 'Happy' }] },
        },
    },
]

const animal: AnimalState = {
    description: 'description',
    name: 'name',
    gender: 'Male',
    breed: 'breed',
    photographs: [
        { id: 1, url: 'http://this.isa.test' },
        { id: 2, url: 'https://this.isa.test' },
    ],
    dateOfBirth: '2020-02-02',
    favoriteAnimals: [],
    dateAdded: '2020-02-02',
    statusUpdates: [],
    dispositions: [{ id: 1, description: 'Happy' }],
    kind: 'Dog',
    status: 'Adopted',
    id: 1,
}

describe('AnimalEditDialogContent Component Tests', () => {
    afterEach(() => {
        store.dispatch(clearUser())
        store.dispatch(closeDialog())
        store.dispatch(clearAnimal())
        store.dispatch(clearAnimals())

        store.dispatch(clearNotification())
    })
    test('delete two images and submit form - successfully', async () => {
        client.mutate = jest.fn()
        await act(async () => {
            store.dispatch(
                setUser({
                    username: 'asdf',
                    id: 1,
                    isStaff: true,
                    favorites: [],
                    emailOptIn: false,
                })
            )
            await store.dispatch(setAnimal(animal))
            await renderWithMockProvider(
                <AnimalEditDialogContent />,
                mockDispositions
            )
            await fireEvent.click(screen.getByTestId('image-viewer-forward'))
            const deleteButton = screen.getByTestId('image-viewer-delete')
            await fireEvent.click(deleteButton)
            await fireEvent.click(deleteButton)
            const updateButton = screen.getByTestId('animal-edit-update-button')
            await fireEvent.submit(updateButton)
        })
        expect(store.getState().animal.id).toBe(undefined)
        expect(store.getState().customDialog.type).toBe(null)
    })

    test('submit form - failure', async () => {
        await act(async () => {
            store.dispatch(setAnimal(animal))
            client.mutate = jest.fn().mockImplementation(() => {
                throw new Error('test')
            })
            renderWithMockProvider(
                <AnimalEditDialogContent />,
                mockDispositions
            )
            await new Promise(resolve => setTimeout(resolve, 200))
            const updateButton = screen.getByTestId('animal-edit-update-button')
            await fireEvent.submit(updateButton)
        })
        expect(store.getState().notification.type).toBe('error')
        expect(store.getState().animal.id).toBe(1)
    })
    test('render component and check that sub components render', () => {
        animal.dispositions = []
        store.dispatch(setAnimal(animal))
        renderWithMockProvider(<AnimalEditDialogContent />, mockDispositions)
        const appbar = screen.getByTestId('animal-edit-app-bar')
        const closeButton = screen.getByTestId('animal-edit-close-button')
        const container = screen.getByTestId('animal-edit-container')
        const form = screen.getByTestId('animal-edit-form')
        const nameField = screen.getByTestId('name-field')
        const descriptionField = screen.getByTestId('description-field')
        const breedField = screen.getByTestId('breed-field')
        const birthdayField = screen.getByTestId('dateOfBirth-field')
        const genderField = screen.getByTestId('dropdown-gender')
        const kindField = screen.getByTestId('dropdown-kind')
        const statusField = screen.getByTestId('dropdown-status')
        const dispositionsField = screen.getByText('Dispositions')
        const actionBar = screen.getByTestId('animal-edit-action-bar')
        const updateButton = screen.getByTestId('animal-edit-update-button')
        const cancelButton = screen.getByTestId('animal-edit-cancel-button')

        expect(appbar).toBeInTheDocument()
        expect(closeButton).toBeInTheDocument()
        expect(container).toBeInTheDocument()
        expect(form).toBeInTheDocument()
        expect(nameField).toBeInTheDocument()
        expect(descriptionField).toBeInTheDocument()
        expect(breedField).toBeInTheDocument()
        expect(birthdayField).toBeInTheDocument()
        expect(genderField).toBeInTheDocument()
        expect(kindField).toBeInTheDocument()
        expect(statusField).toBeInTheDocument()
        expect(dispositionsField).toBeInTheDocument()
        expect(actionBar).toBeInTheDocument()
        expect(updateButton).toBeInTheDocument()
        expect(cancelButton).toBeInTheDocument()
    })

    test('click on close button', () => {
        store.dispatch(setAnimal(animal))
        render(<AnimalEditDialogContent />)
        expect(store.getState().animal.id).toBe(1)
        const closeButton = screen.getByTestId('animal-edit-close-button')
        fireEvent.click(closeButton)
        expect(store.getState().customDialog.type).toBe(null)
    })

    test('click on cancel button', () => {
        store.dispatch(setAnimal(animal))
        render(<AnimalEditDialogContent />)
        const cancelButton = screen.getByTestId('animal-edit-cancel-button')
        fireEvent.click(cancelButton)
        expect(store.getState().customDialog.type).toBe(null)
    })
})
