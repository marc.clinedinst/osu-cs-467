import React from 'react'
import { render, screen, fireEvent } from 'app/testing-utils'
import CustomDialog from 'components/common/CustomDialog'
import store from 'app/store'
import { closeDialog, openDialog } from 'ducks/customDialogSlice'

describe('CustomDialog Component tests', () => {
    beforeEach(() => render(<CustomDialog />))
    afterAll(() => store.dispatch(closeDialog()))

    test('render the CustomDialog component with no type.', () => {
        const dialog = screen.queryByTestId('dialog')
        expect(dialog).not.toBeInTheDocument()
    })

    test('render the CustomDialog component as the "login" type.', () => {
        store.dispatch(openDialog('login'))

        const dialog = screen.queryByTestId('dialog')
        const loginDialogContent = screen.queryByTestId('login-dialog-header')
        const usernameText = screen.queryByTestId('username-field')
        const passwordText = screen.queryByTestId('password-field')
        const loginButton = screen.queryByTestId('login-dialog-login-button')

        expect(dialog).toBeInTheDocument()
        expect(loginDialogContent).toBeInTheDocument()
        expect(usernameText).toBeInTheDocument()
        expect(passwordText).toBeInTheDocument()
        expect(loginButton).toBeInTheDocument()
        if (dialog) {
            fireEvent.keyDown(dialog, {
                key: 'Escape',
                code: 'Escape',
                keyCode: 27,
                charCode: 27,
            })

            expect(dialog).not.toBeInTheDocument()
        }
    })

    test('open and close the CustomDialog', () => {
        store.dispatch(openDialog('login'))

        const dialog = screen.queryByTestId('dialog')
        const loginDialogContent = screen.queryByTestId('login-dialog-header')

        expect(dialog).toBeInTheDocument()
        expect(loginDialogContent).toBeInTheDocument()
        if (dialog) {
            fireEvent.keyDown(dialog, {
                key: 'Escape',
                code: 'Escape',
                keyCode: 27,
                charCode: 27,
            })

            expect(dialog).not.toBeInTheDocument()
            expect(loginDialogContent).not.toBeInTheDocument()
        }
    })
})
