import React from 'react'
import { fireEvent, render, screen } from 'app/testing-utils'
import NavBar from 'components/common/NavBar'
import store from 'app/store'
import { clearUser, setUser } from 'ducks/userSlice'
import { closeDialog } from 'ducks/customDialogSlice'
import history from 'app/history'

const mockUser = {
    username: 'admin',
    id: 1,
    isStaff: false,
    emailOptIn: false,
    favorites: [],
}
describe('NavBar Component Tests', () => {
    beforeEach(async () => {
        await render(<NavBar />)
        history.push('/search')
    })

    afterEach(() => {
        store.dispatch(closeDialog())
        store.dispatch(clearUser())
    })

    test('render the Navbar and check that it rendered', async () => {
        store.dispatch(setUser(mockUser))
        const navBarText = screen.getByText(/paw swipe/i)
        const hamburgerIcon = screen.queryByTestId('navbar-hamburger-menu')
        const filterButton = await screen.getByTestId('navbar-filter')
        expect(navBarText).toBeInTheDocument()
        expect(hamburgerIcon).toBeInTheDocument()
        expect(filterButton).toBeInTheDocument()
    })

    test('click on the title with user logged in.', async () => {
        store.dispatch(setUser(mockUser))
        history.push('/anotherRoute')
        const title = screen.queryByTestId('navbar-title')
        expect(title).toBeInTheDocument()
        if (title) await fireEvent.click(title)
        expect(history.location.pathname).toBe('/search')
    })
    test('click on the title with no user logged in.', async () => {
        history.push('/anotherRoute')
        const title = screen.queryByTestId('navbar-title')
        expect(title).toBeInTheDocument()
        if (title) await fireEvent.click(title)
        expect(history.location.pathname).toBe('/anotherRoute')
    })
    test('click on the filter', async () => {
        store.dispatch(setUser(mockUser))
        const filterButton = await screen.getByTestId('navbar-filter')
        expect(filterButton).toBeInTheDocument()
        if (filterButton) await fireEvent.click(filterButton)
        expect(history.location.pathname).toBe('/search')
    })
})
