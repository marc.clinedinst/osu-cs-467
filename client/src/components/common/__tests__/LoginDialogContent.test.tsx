import React from 'react'
import { render, screen, fireEvent, act } from 'app/testing-utils'
import store from 'app/store'
import LoginDialogContent from '../LoginDialogContent'
import { clearUser } from 'ducks/userSlice'
import { clearNotification } from 'ducks/notificationSlice'
import client from 'app/client'
jest.mock('app/client')

describe('LoginDialogContent Component Tests', () => {
    beforeEach(async () => {
        await act(async () => {
            await render(<LoginDialogContent />)
        })
    })
    afterEach(() => {
        store.dispatch(clearUser())
        store.dispatch(clearNotification())
    })
    test('render the LoginDialogContent component', () => {
        const loginHeader = screen.queryByTestId('login-dialog-header')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')
        const loginButton = screen.queryByTestId('login-dialog-login-button')

        expect(loginHeader).toBeInTheDocument()
        expect(usernameField).toBeInTheDocument()
        expect(passwordField).toBeInTheDocument()
        expect(loginButton).toBeInTheDocument()
    })

    test('render the LoginDialogContent component and press login button to trigger errors', async () => {
        const loginButton = screen.queryByTestId('login-dialog-login-button')
        if (loginButton) {
            await act(async () => {
                fireEvent.click(loginButton)
            })
            await new Promise(resolve => setTimeout(resolve, 0))
            const errors = screen.queryAllByText(/required/i)
            expect(errors.length).toEqual(2)
        }
    })

    test('render the LoginDialogContent component and log in successfullly', async () => {
        const loginButton = screen.queryByTestId('login-dialog-login-button')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')

        if (loginButton && usernameField && passwordField) {
            await act(async () => {
                client.query = jest
                    .fn()
                    .mockReturnValue({ data: { favoriteAnimals: [] } })
                fireEvent.change(usernameField, {
                    target: { value: 'test' },
                })
                fireEvent.change(passwordField, {
                    target: { value: 'asdf' },
                })
                fireEvent.submit(loginButton)
            })
            await new Promise(resolve => setTimeout(resolve, 0))
            const errors = screen.queryAllByText(/required/i)
            expect(errors.length).toEqual(0)
            expect(store.getState().user.username).toEqual('test')
            jest.clearAllMocks()
        }
    })

    test('render the LoginDialogContent component and log in unsuccessfullly', async () => {
        const loginButton = screen.queryByTestId('login-dialog-login-button')
        const usernameField = screen.queryByTestId('username-field')
        const passwordField = screen.queryByTestId('password-field')

        if (loginButton && usernameField && passwordField) {
            await act(async () => {
                fireEvent.change(usernameField, {
                    target: { value: 'a' },
                })
                fireEvent.change(passwordField, {
                    target: { value: 'a' },
                })
                fireEvent.submit(loginButton)
            })
            await new Promise(resolve => setTimeout(resolve, 0))
            const errors = screen.queryAllByText(/required/i)
            expect(errors.length).toEqual(0)
            expect(store.getState().notification.type).toEqual('error')
            expect(store.getState().notification.message).toEqual(
                'invalid test'
            )
        }
    })
})
