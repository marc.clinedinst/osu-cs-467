import React from 'react'
import { render, screen } from 'app/testing-utils'
import LazyLoad from '../LazyLoad'
import { Typography } from '@material-ui/core'

describe('LazyLoad Component Tests', () => {
    test('render the LazyLoad component as loading', () => {
        render(
            <LazyLoad
                numberOfSkeletons={3}
                skeletonProps={{ width: '200px' }}
                loading={true}
            >
                <Typography>This is a test</Typography>
            </LazyLoad>
        )
        const skeletons = screen.queryAllByTestId('lazy-load-skeleton')
        const text = screen.queryByText('This is a test')
        expect(skeletons.length).toBe(3)
        expect(text).not.toBeInTheDocument()
    })

    test('render the LazyLoad component with loading complete', () => {
        render(
            <LazyLoad
                numberOfSkeletons={3}
                skeletonProps={{ width: '200px' }}
                loading={false}
            >
                <Typography>This is a test</Typography>
            </LazyLoad>
        )
        const skeletons = screen.queryAllByTestId('lazy-load-skeleton')
        const text = screen.queryByText('This is a test')
        expect(skeletons.length).toBe(0)
        expect(text).toBeInTheDocument()
    })
})
