import React from 'react'
import {
    act,
    fireEvent,
    render,
    renderWithMockProvider,
    screen,
} from 'app/testing-utils'
import SideDrawer from 'components/common/SideDrawer'
import store from 'app/store'
import { clearUser, setUser } from 'ducks/userSlice'
import history from 'app/history'
import { MockedResponse } from '@apollo/client/testing'
import { TOGGLE_EMAIL_OPT_IN } from 'gql/mutations'
import { GraphQLError } from 'graphql'
import { clearNotification } from 'ducks/notificationSlice'

const mockUser = {
    username: 'testUser',
    id: 1,
    isStaff: true,
    favorites: [],
    emailOptIn: false,
}

const mockPetSeeker = { ...mockUser, isStaff: false }
const mockFail: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_EMAIL_OPT_IN,
        },
        result: {
            errors: [new GraphQLError('invalid test')],
        },
    },
]

const mockSuccessTrue: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_EMAIL_OPT_IN,
        },

        result: { data: { toggleEmailOptIn: { optedIn: true } } },
    },
]

const mockSuccessFalse: MockedResponse<Record<string, any>>[] = [
    {
        request: {
            query: TOGGLE_EMAIL_OPT_IN,
        },

        result: { data: { toggleEmailOptIn: { optedIn: false } } },
    },
]

describe('SideDrawer Component Tests', () => {
    beforeEach(() => {
        render(<SideDrawer />)
        const button = screen.queryByTestId('navbar-hamburger-menu')
        if (button) {
            fireEvent.click(button)
        }
    })

    afterEach(() => {
        const button = screen.queryByTestId('navbar-hamburger-menu')
        if (button) {
            fireEvent.click(button)
        }
        store.dispatch(clearUser())
    })

    test('render the side drawer and check that it rendered', () => {
        const hamburger = screen.queryByTestId('navbar-hamburger-menu')
        const navDrawerContainer = screen.queryByTestId('nav-drawer-container')
        expect(hamburger).toBeInTheDocument()
        expect(navDrawerContainer).toBeInTheDocument()
    })

    test('render Navbar with no user logged in', () => {
        const loginButton = screen.queryByTestId('nav-drawer-login')
        const registerButton = screen.queryByTestId('nav-drawer-register')
        const logoutButton = screen.queryByTestId('nav-drawer-logout')
        expect(loginButton).toBeInTheDocument()
        expect(registerButton).toBeInTheDocument()
        expect(logoutButton).not.toBeInTheDocument()
    })

    test('render component with user logged in', () => {
        store.dispatch(setUser(mockPetSeeker))
        const searchButton = screen.queryByTestId('nav-drawer-logout')
        const favoritesButtons = screen.queryByTestId('nav-drawer-logout')
        const logoutButton = screen.queryByTestId('nav-drawer-logout')
        const loginButton = screen.queryByTestId('nav-drawer-login')
        expect(searchButton).toBeInTheDocument()
        expect(favoritesButtons).toBeInTheDocument()
        expect(logoutButton).toBeInTheDocument()
        expect(loginButton).not.toBeInTheDocument()
    })

    test('render component as staff user', () => {
        store.dispatch(setUser(mockUser))
        const adminTools = screen.queryByTestId('nav-drawer-admin-tools')
        const addAnimalButton = screen.queryByTestId('nav-drawer-add-animal')
        expect(adminTools).toBeInTheDocument()
        expect(addAnimalButton).toBeInTheDocument()
    })
    test('click on search', () => {
        store.dispatch(setUser(mockUser))
        const navSearch = screen.queryByTestId('nav-drawer-search')
        if (navSearch) fireEvent.click(navSearch)
        expect(history.location.pathname).toBe('/search')
    })
    test('click on favorites', () => {
        store.dispatch(setUser(mockUser))
        const navFavorites = screen.queryByTestId('nav-drawer-my-favorites')
        if (navFavorites) fireEvent.click(navFavorites)
        expect(history.location.pathname).toBe('/favorites')
    })
    test('click on logout', () => {
        store.dispatch(setUser(mockUser))
        const navLogout = screen.queryByTestId('nav-drawer-logout')
        if (navLogout) fireEvent.click(navLogout)
        expect(store.getState().user.username).toBe(undefined)
        expect(history.location.pathname).toBe('/')
    })
    test('click on login', () => {
        const loginButton = screen.queryByTestId('nav-drawer-login')
        if (loginButton) fireEvent.click(loginButton)
        expect(store.getState().customDialog.type).toBe('login')
    })
    test('click on login', () => {
        const registerButton = screen.queryByTestId('nav-drawer-register')
        if (registerButton) fireEvent.click(registerButton)
        expect(store.getState().customDialog.type).toBe('register')
    })
    test('click on add animal', () => {
        store.dispatch(setUser(mockUser))
        const addAnimalButton = screen.queryByTestId('nav-drawer-add-animal')
        if (addAnimalButton) fireEvent.click(addAnimalButton)
        expect(history.location.pathname).toBe('/add-animal')
    })
})

describe('Side Drawer Component Async Tests', () => {
    afterEach(async () => {
        await act(async () => {
            store.dispatch(clearUser())
            store.dispatch(clearNotification())
        })
    })
    test('click on animal update email switch - toggle on mutation successful', async () => {
        await act(async () => {
            store.dispatch(setUser(mockUser))
            renderWithMockProvider(<SideDrawer />, mockSuccessTrue)
            fireEvent.click(screen.getByTestId('navbar-hamburger-menu'))
        })
        await act(async () => {
            fireEvent.click(
                screen.getByTestId('nav-drawer-toggle-email-opt-in')
            )
        })
        await new Promise(resolve => setTimeout(resolve, 0))
        expect(store.getState().user.emailOptIn).toBe(true)
        expect(store.getState().notification.type).toBe('success')
    })
    test('click on animal update email switch - toggle on mutation successful', async () => {
        await act(async () => {
            mockUser.emailOptIn = true
            store.dispatch(setUser(mockUser))
            renderWithMockProvider(<SideDrawer />, mockSuccessFalse)
            fireEvent.click(screen.getByTestId('navbar-hamburger-menu'))
        })
        await act(async () => {
            fireEvent.click(
                screen.getByTestId('nav-drawer-toggle-email-opt-in')
            )
        })
        await new Promise(resolve => setTimeout(resolve, 0))
        expect(store.getState().user.emailOptIn).toBe(false)
        expect(store.getState().notification.type).toBe('success')
    })
    test('click on animal update email switch - mutation fail', async () => {
        await act(async () => {
            mockUser.emailOptIn = false
            store.dispatch(setUser(mockUser))
            renderWithMockProvider(<SideDrawer />, mockFail)
            fireEvent.click(screen.getByTestId('navbar-hamburger-menu'))
        })
        await act(async () => {
            fireEvent.click(
                screen.getByTestId('nav-drawer-toggle-email-opt-in')
            )
        })
        await new Promise(resolve => setTimeout(resolve, 0))
        expect(store.getState().user.emailOptIn).toBe(false)
        expect(store.getState().notification.type).toBe('error')
    })
})
