import React from 'react'
import {
    render,
    screen,
    fireEvent,
    act,
    renderWithMockProvider,
} from 'app/testing-utils'
import store from 'app/store'
import FiltersDialogContent from '../FiltersDialogContent'
import { clearUser } from 'ducks/userSlice'
import { clearNotification } from 'ducks/notificationSlice'
import selectEvent from 'react-select-event'
import { MockedResponse } from '@apollo/client/testing'
import { ANIMAL_DISPOSITIONS } from 'gql/queries'
import { GraphQLError } from 'graphql'
import client from 'app/client'
jest.mock('app/client')

describe('FiltersDialogContent Component Tests', () => {
    afterEach(() => {
        store.dispatch(clearUser())
        store.dispatch(clearNotification())
    })

    test('render the FiltersDialogContent component', () => {
        render(<FiltersDialogContent />)
        const filtersHeader = screen.queryByTestId('search-dialog-header')
        const filtersForm = screen.queryByTestId('search-dialog-search-form')
        const kindField = screen.queryByTestId('dropdown-kind')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const dateAddedField = screen.queryByTestId('dropdown-daysBeforeToday')
        const filtersButton = screen.queryByTestId(
            'search-dialog-search-button'
        )
        const dispositionField = screen.queryByLabelText('Dispositions')
        expect(filtersHeader).toBeInTheDocument()
        expect(filtersForm).toBeInTheDocument()
        expect(kindField).toBeInTheDocument()
        expect(breedField).toBeInTheDocument()
        expect(genderField).toBeInTheDocument()
        expect(dateAddedField).toBeInTheDocument()
        expect(dispositionField).toBeInTheDocument()
        expect(filtersButton).toBeInTheDocument()
    })

    test('render the FiltersDialogContent component and press filters button to trigger errors', async () => {
        await act(async () => {
            const mockError: MockedResponse<Record<string, any>>[] = [
                {
                    request: {
                        query: ANIMAL_DISPOSITIONS,
                        variables: {},
                    },
                    result: {
                        errors: [new GraphQLError('error')],
                    },
                },
            ]
            await renderWithMockProvider(<FiltersDialogContent />, mockError)

            await new Promise(resolve => setTimeout(resolve, 100))
            expect(store.getState().notification.type).toEqual('error')
        })
    })

    test('Change no fields and submit', async () => {
        client.query = jest.fn().mockReturnValue({ data: { animals: [] } })
        const mock: MockedResponse<Record<string, any>>[] = [
            {
                request: {
                    query: ANIMAL_DISPOSITIONS,
                    variables: {},
                },
                result: {
                    data: {
                        animalDispositions: [
                            { id: 1, description: 'Aggressive' },
                            { id: 2, description: 'Bad with children' },
                        ],
                    },
                },
            },
        ]
        await act(async () => {
            await renderWithMockProvider(<FiltersDialogContent />, mock)
        })
        const filtersButton = screen.queryByTestId(
            'search-dialog-search-button'
        )

        await new Promise(resolve => setTimeout(resolve, 100))

        if (filtersButton) {
            await act(async () => {
                fireEvent.submit(filtersButton)

                await new Promise(resolve => setTimeout(resolve, 100))
                expect(store.getState().animals?.length).toBeGreaterThanOrEqual(
                    0
                )
                expect(store.getState().notification.type).toBe(undefined)
            })
        }
    })

    test('Change all fields and submit', async () => {
        client.query = jest.fn().mockReturnValue({ data: { animals: [] } })
        const mock: MockedResponse<Record<string, any>>[] = [
            {
                request: {
                    query: ANIMAL_DISPOSITIONS,
                    variables: {},
                },
                result: {
                    data: {
                        animalDispositions: [
                            { id: 1, description: 'Aggressive' },
                            { id: 2, description: 'Bad with children' },
                        ],
                    },
                },
            },
        ]
        await act(async () => {
            await renderWithMockProvider(<FiltersDialogContent />, mock)
        })
        const filtersButton = screen.queryByTestId(
            'search-dialog-search-button'
        )
        const kindField = screen.queryByTestId('dropdown-kind')
        const breedField = screen.queryByTestId('breed-field')
        const genderField = screen.queryByTestId('dropdown-gender')
        const dateAddedField = screen.queryByTestId('dropdown-daysBeforeToday')
        const dispositionField = screen.queryByLabelText('Dispositions')

        await new Promise(resolve => setTimeout(resolve, 100))

        if (
            filtersButton &&
            kindField &&
            breedField &&
            genderField &&
            dateAddedField &&
            dispositionField
        ) {
            await act(async () => {
                fireEvent.input(breedField, {
                    target: { value: 'Dog' },
                })
                fireEvent.change(kindField, {
                    target: { value: 'Dog' },
                })
                fireEvent.change(genderField, {
                    target: { value: 'Male' },
                })
                fireEvent.change(dateAddedField, {
                    target: { value: '1' },
                })
                await selectEvent.select(dispositionField, 'Aggressive')
                fireEvent.submit(filtersButton)

                await new Promise(resolve => setTimeout(resolve, 100))
                expect(store.getState().animals?.length).toBeGreaterThanOrEqual(
                    0
                )
                expect(store.getState().notification.type).toBe(undefined)
            })
        }
    })
})
