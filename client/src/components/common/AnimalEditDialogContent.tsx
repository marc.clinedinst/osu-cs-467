import React, { FC, useState } from 'react'
import styled from 'styled-components'

import { useDispatch, useSelector } from 'app/store'
import useDispositions from 'hooks/useDispositions'
import { useForm } from 'react-hook-form'

import { closeDialog } from 'ducks/customDialogSlice'
import {
    convertToMultiSelectOptions,
    sanitizeAnimalEditFormValues,
} from 'utils/sanitize'

import { AppBar, Button, IconButton, Paper, Toolbar } from '@material-ui/core'
import Typography from 'components/common/StyledTypography'
import Container from 'components/common/Container'
import CloseIcon from '@material-ui/icons/Close'
import ControlledText from 'components/common/ControlledText'
import MultiSelect from 'components/common/MultiSelect'
import Dropdown from 'components/common/Dropdown'

import {
    GENDER_DROPDOWN_OPTIONS,
    KIND_DROPDOWN_OPTIONS,
    STATUS_OPTIONS,
} from 'constants/constants'
import { AnimalEditFormValues } from 'components/common/types'
import ImageViewer from 'components/routes/animalProfile/ImageViewer'
import updateAnimal from 'thunks/updateAnimal'
import ImageDrop from 'components/routes/addAnimal/ImageDrop'
import { ImageFile } from 'components/routes/addAnimal/types'

const Grid = styled.div`
    display: grid;
    grid-template-columns: 325px;
    @media (min-width: 576px) {
        grid-template-columns: 500px;
    }
    @media (min-width: 768px) {
        grid-template-columns: 650px;
    }
    @media (min-width: 992px) {
        grid-template-columns: 520px 1fr;
        column-gap: 30px;
    }
    @media (min-width: 1200px) {
        grid-template-columns: 600px 1fr;
    }
`

const ProfileContainer = styled(Container)`
    padding: 40px 0;
    width: 100%;
    align-items: center;
    justify-content: initial;
`

const ActionBar = styled.div`
    display: flex;
    justify-content: end;
    margin-top: 10px;
    > button:not(:last-child) {
        margin-right: 10px;
    }
`

const ContentContainer = styled.form`
    padding: 18px 20px 30px 20px;
    && > * {
        margin-bottom: 10px;
    }
`

const AnimalEditDialogContent: FC = () => {
    const dispatch = useDispatch()
    const animal = useSelector(state => state.animal)
    const dispositionData = useDispositions()
    const [photosToDelete, setPhotosToDelete] = useState<number[]>([])
    const [files, setFiles] = useState<ImageFile[]>([])

    const {
        control,
        errors,
        register,
        handleSubmit,
    } = useForm<AnimalEditFormValues>({
        defaultValues: {
            name: animal.name,
            description: animal.description,
            breed: animal.breed,
            dateOfBirth: animal.dateOfBirth,
            dispositions: convertToMultiSelectOptions(
                animal.dispositions ?? []
            ),
            gender: animal.gender,
            kind: animal.kind,
            status: animal.status,
        },
    })

    const onSubmit = async (values: AnimalEditFormValues) => {
        dispatch(
            updateAnimal({
                updateAnimal: sanitizeAnimalEditFormValues(values, animal.id),
                photosToDelete,
                photosToUpload: files,
            })
        )
    }
    const onDelete = (ids: number[]) => setPhotosToDelete(ids)

    return (
        <div>
            <AppBar data-testid='animal-edit-app-bar' position='sticky'>
                <Toolbar>
                    <IconButton
                        data-testid='animal-edit-close-button'
                        edge='start'
                        color='inherit'
                        onClick={() => dispatch(closeDialog())}
                        aria-label='close'
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography>Animal Edit</Typography>
                </Toolbar>
            </AppBar>
            <ProfileContainer>
                <Paper>
                    <Grid data-testid='animal-edit-container'>
                        <ImageViewer
                            onDelete={onDelete}
                            images={animal.photographs}
                            isEditable
                        />
                        <ContentContainer
                            data-testid='animal-edit-form'
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            <Typography variant='h4'>
                                Animal Information
                            </Typography>
                            <ImageDrop
                                text='Add images here!'
                                files={files}
                                setFiles={setFiles}
                            />
                            <ControlledText
                                errors={errors}
                                register={() => register({ required: true })}
                                label={'Name'}
                                name={'name'}
                            />
                            <ControlledText
                                errors={errors}
                                register={() => register({ required: true })}
                                label={'Description'}
                                name={'description'}
                                textFieldProps={{ multiline: true, rows: 5 }}
                            />
                            <ControlledText
                                errors={errors}
                                register={() => register({ required: true })}
                                label={'Breed'}
                                name={'breed'}
                            />
                            <ControlledText
                                errors={errors}
                                register={() => register({ required: true })}
                                label={'Birthday'}
                                name={'dateOfBirth'}
                                textFieldProps={{
                                    type: 'date',
                                    InputLabelProps: { shrink: true },
                                }}
                            />
                            <Dropdown
                                outlined
                                errors={errors}
                                label='Gender'
                                options={GENDER_DROPDOWN_OPTIONS}
                                labelId='gender'
                                control={control}
                            />
                            <Dropdown
                                outlined
                                errors={errors}
                                label='Kind'
                                options={KIND_DROPDOWN_OPTIONS}
                                labelId='kind'
                                control={control}
                            />
                            <Dropdown
                                outlined
                                errors={errors}
                                label='Status'
                                options={STATUS_OPTIONS}
                                labelId='status'
                                control={control}
                            />
                            <MultiSelect
                                errors={errors}
                                required
                                native
                                label='Dispositions'
                                placeholder='Dispositions'
                                control={control}
                                options={convertToMultiSelectOptions(
                                    dispositionData?.animalDispositions ?? []
                                )}
                                noLabel
                                labelId='dispositions'
                            />

                            <ActionBar data-testid='animal-edit-action-bar'>
                                <Button
                                    data-testid='animal-edit-update-button'
                                    variant='contained'
                                    color='primary'
                                    type='submit'
                                >
                                    Update
                                </Button>
                                <Button
                                    data-testid='animal-edit-cancel-button'
                                    variant='contained'
                                    color='secondary'
                                    onClick={() => dispatch(closeDialog())}
                                >
                                    Cancel
                                </Button>
                            </ActionBar>
                        </ContentContainer>
                    </Grid>
                </Paper>
            </ProfileContainer>
        </div>
    )
}

export default AnimalEditDialogContent
