import React, { FunctionComponent } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { batch } from 'react-redux'
import { useMutation } from '@apollo/client'
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core'
import { closeDialog } from 'ducks/customDialogSlice'
import { REGISTER_USER } from 'gql/mutations'
import { RegisterUserType } from 'gql/types'
import { RegistrationFormValues } from './types'
import ControlledText from './ControlledText'
import history from 'app/history'
import { openNotification } from 'ducks/notificationSlice'
import { setUser } from 'ducks/userSlice'
import { useDispatch } from 'app/store'

const RegistrationDialogContent: FunctionComponent = () => {
    const { register, errors, handleSubmit } = useForm()

    const dispatch = useDispatch()

    const [submitRegistration] = useMutation<RegisterUserType>(REGISTER_USER, {
        onCompleted: ({ registerUser }) => {
            localStorage.setItem('jwtToken', registerUser.token)
            const { favoriteAnimals, ...user } = registerUser.user
            batch(() => {
                dispatch(setUser({ favorites: favoriteAnimals, ...user }))
                dispatch(closeDialog())
                history.push('/search')
            })
        },
        onError: err =>
            dispatch(openNotification({ type: 'error', message: err.message })),
    })

    const onSubmit: SubmitHandler<RegistrationFormValues> = formValues => {
        submitRegistration({
            variables: {
                ...formValues,
            },
        })
    }

    return (
        <>
            <DialogTitle
                id='form-dialog-title'
                data-testid='register-dialog-header'
            >
                Register
            </DialogTitle>
            <form onSubmit={handleSubmit(onSubmit)}>
                <DialogContent>
                    <ControlledText
                        register={() => register({ required: true })}
                        label='Email'
                        name='email'
                        errors={errors}
                        textFieldProps={{ autoFocus: true, type: 'email' }}
                    />
                    <ControlledText
                        register={() => register({ required: true })}
                        label='Username'
                        name='username'
                        errors={errors}
                    />
                    <ControlledText
                        register={() => register({ required: true })}
                        label='Password'
                        name='password'
                        errors={errors}
                        textFieldProps={{ type: 'password' }}
                    />
                    <ControlledText
                        register={() => register({ required: true })}
                        label='Confirm Password'
                        name='confirmPassword'
                        errors={errors}
                        textFieldProps={{ type: 'password' }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button
                        type='submit'
                        color='primary'
                        variant='contained'
                        data-testid='register-dialog-register-button'
                    >
                        Register
                    </Button>
                </DialogActions>
            </form>
        </>
    )
}

export default RegistrationDialogContent
