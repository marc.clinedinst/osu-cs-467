import React, { FunctionComponent } from 'react'
import { InputLabel, TextField } from '@material-ui/core'
import { ControlledTextProps } from './types'
import styled from 'styled-components'

const StyledTextField = styled(TextField)`
    && > label {
        font-size: 1rem;
    }
    && > div {
        padding: 10px;
        margin-bottom: 10px;
    }
    && {
        margin-bottom: 20px;
    }
`

const ControlledText: FunctionComponent<ControlledTextProps> = ({
    errors,
    label,
    className,
    register,
    name,
    styleAsSelect = false,
    textFieldProps,
}) => {
    return styleAsSelect ? (
        <div>
            <InputLabel id={name}>{label}</InputLabel>
            <StyledTextField
                inputProps={{
                    'data-testid': `${name}-field`,
                }}
                inputRef={register()}
                type='text'
                name={name}
                defaultValue=''
                fullWidth
                {...textFieldProps}
            />
        </div>
    ) : (
        <TextField
            className={className}
            inputRef={register()}
            label={label}
            variant='outlined'
            inputProps={{
                'data-testid': `${name}-field`,
            }}
            type='text'
            margin='dense'
            name={name}
            defaultValue=''
            error={errors && errors[name] ? true : false}
            fullWidth
            helperText={errors && errors[name] && `${label} is required.`}
            {...textFieldProps}
        />
    )
}
export default ControlledText
