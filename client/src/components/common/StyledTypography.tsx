import { Typography, TypographyProps } from '@material-ui/core'
import React from 'react'
import styled from 'styled-components'

interface TextProps extends TypographyProps {
    $textColor?: string
    component?: React.ElementType
}
const Text: React.FunctionComponent<TextProps> = styled(
    ({ $textColor, ...rest }: TextProps) => <Typography {...rest} />
)`
    color: ${({ $textColor }: TextProps) => $textColor};
`

export default Text
