import React, { FC } from 'react'
import Select from 'react-select'
import { Controller } from 'react-hook-form'
import { MultiselectDropdownOptions, MultiSelectProps } from './types'
import styled from 'styled-components'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import { InputLabel, FormHelperText } from '@material-ui/core'
const ErrorText = styled(FormHelperText)`
    && {
        margin: 4px 14px;
    }
`

const ReactSelect = styled(Select)<{ $error: boolean }>`
    && {
        .react-select__control--is-focused {
            box-shadow: none;
            border-color: none;
        }
        .react-select__control {
            border-color: ${({ $error }) => ($error ? `#f44336` : `#c4c4c4`)};
        }
        .react-select__placeholder {
            color: ${({ $error }) => ($error ? `#f44336` : `#c4c4c4`)};
        }
    }
`

const StyledSelect = styled(Select)`
    && {
        width: 250px;
        padding: 10px 0 0 0;
        margin-bottom: 30px;
        cursor: pointer;
    }
    && > * {
        cursor: pointer;
    }
    && > div {
        border-radius: 0px !important;
        transition: border-bottom-color 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.42);
    }
    && > div:hover {
        border-bottom: 2px solid rgba(0, 0, 0, 0.87);
    }
    && > div.react-select__control--is-focused {
        box-shadow: none !important;
        border-bottom: 2px solid #3f51b5 !important;
    }
    && > div > div.react-select__value-container {
        padding-left: 7px;
        padding-top: 0px;
        padding-bottom: 0px;
        margin-bottom: 12px;
    }
    &&
        > div
        > div.react-select__value-container
        > div.react-select__placeholder {
        margin-top: 1px;
        font-size: 1rem;
        font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
        font-weight: 400;
        line-height: 1.1876em;
    }
`

const customStyles = {
    option: (base: any) => ({
        ...base,
        backgroundColor: 'white',
        ':hover': {
            textDecoration: 'none',
            backgroundColor: 'rgba(0, 0, 0, 0.04)',
            cursor: 'pointer',
        },
    }),
}

const StyledArrowDropDownIcon = styled(ArrowDropDownIcon)<{ $native: boolean }>`
    ${({ $native }) => $native && `margin-right: 5px;`}
    color: rgba(0, 0, 0, 0.54);
`

const MultiSelect: FC<MultiSelectProps> = ({
    control,
    className,
    required = false,
    native = false,
    errors,
    label,
    noLabel = false,
    placeholder,
    labelId,
    options,
}) => {
    // Helper that creates the react-select props.
    const selectDefaultProps: (
        value: MultiselectDropdownOptions[],
        onChange: (event: any) => void
    ) => React.ComponentProps<typeof Select> = (value, onChange) => ({
        isMulti: true,
        inputId: `multiselect-${labelId}`,
        name: `multiselect-${labelId}`,
        menuPosition: 'fixed',
        styles: native ? {} : customStyles,
        components: {
            DropdownIndicator: () => (
                <StyledArrowDropDownIcon $native={native} />
            ),
            IndicatorSeparator: () => null,
        },
        classNamePrefix: 'react-select',
        placeholder: placeholder,
        onChange: onChange,
        ds: true,
        value: value,
        options: options,
    })

    const rules = required
        ? {
              required,
              validate: (value: MultiselectDropdownOptions[]) => {
                  if (!value || value.length === 0)
                      return `${label} is required.`
              },
          }
        : {}

    const selectMainComponent = (
        value: MultiselectDropdownOptions[],
        onChange: (event: any) => void
    ) => {
        return native ? (
            <ReactSelect
                $error={Boolean(errors && errors[labelId])}
                {...selectDefaultProps(value, onChange)}
            />
        ) : (
            <StyledSelect {...selectDefaultProps(value, onChange)} />
        )
    }

    const selectWithLabelHelper: (
        value: MultiselectDropdownOptions[],
        onChange: (event: any) => void
    ) => JSX.Element = (value, onChange) => (
        <>
            {!noLabel && (
                <InputLabel htmlFor={`multiselect-${labelId}`} id={labelId}>
                    {label}
                </InputLabel>
            )}
            {selectMainComponent(value, onChange)}
            {errors && errors[labelId] && (
                <ErrorText error>{`${errors[labelId].message}`}</ErrorText>
            )}
        </>
    )
    return (
        <div className={className}>
            <Controller
                name={labelId}
                defaultValue={[]}
                control={control}
                rules={rules}
                render={({ value, onChange }) =>
                    selectWithLabelHelper(value, onChange)
                }
            />
        </div>
    )
}

export default MultiSelect
