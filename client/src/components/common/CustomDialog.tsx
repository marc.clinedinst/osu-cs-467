import React, { FunctionComponent } from 'react'
import { Dialog, Slide } from '@material-ui/core'
import { closeDialog } from 'ducks/customDialogSlice'
import { useDispatch, useSelector } from 'app/store'
import { DIALOG_COMPONENTS } from 'constants/constants'
import { TransitionProps } from '@material-ui/core/transitions'

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>
) {
    return <Slide direction='up' ref={ref} {...props} />
})

const CustomDialog: FunctionComponent = () => {
    const dispatch = useDispatch()
    const { type, isFullscreen } = useSelector(state => state.customDialog)

    const handleClose = () => {
        dispatch(closeDialog())
    }

    if (!type) {
        return null
    }

    const ComponentDialog = DIALOG_COMPONENTS[type]

    return (
        <Dialog
            data-testid='dialog'
            TransitionComponent={Transition}
            fullScreen={isFullscreen}
            open
            onClose={handleClose}
            aria-labelledby='form-dialog-title'
        >
            <ComponentDialog />
        </Dialog>
    )
}

export default CustomDialog
