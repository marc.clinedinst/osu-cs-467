import { FunctionComponent } from 'react'
import { DeepMap, FieldError, UseFormMethods } from 'react-hook-form'
import { TextField, SelectProps, InputLabelProps } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { AnimalsInput, GenderType, KindType, StatusType } from 'gql/types'

export type DropdownOptions = {
    option: string
    optionId: string
}[]

export type MultiselectDropdownOptions = {
    value: number
    label: string
}[]

export type FormValues = {
    username: string
    password: string
}

export type RegistrationFormValues = {
    confirmPassword: string
    email: string
    password: string
    username: string
}

export interface FilterFormValues
    extends Omit<AnimalsInput, 'daysBeforeToday' | 'dispositions'> {
    daysBeforeToday: string
    dispositions?: { value: number; label: string }[]
}

interface DropdownDefault {
    value: string
    label: string
}

export interface DropdownProps {
    className?: string
    label: string
    labelId: string
    options?: DropdownOptions
    outlined?: boolean
    control: UseFormMethods['control']
    errors?: UseFormMethods['errors']
    variant?: InputLabelProps['variant']
    defaultValue?: DropdownDefault
    selectProps?: SelectProps
}

export interface MultiSelectProps {
    label: string
    className?: string
    required?: boolean
    native?: boolean
    placeholder: string
    noLabel?: boolean
    labelId: string
    options?: { value: number; label: string }[]
    control: UseFormMethods['control']
    errors?: UseFormMethods['errors']
}

export interface FilterButtonProps {
    handleClick: Function
    isOpened: boolean
}

export interface CustomDialogProps {
    open: boolean
    handleClose: () => void
    handleAction: () => void
}

export type DialogComponents = {
    login: FunctionComponent
    register: FunctionComponent
    filter: FunctionComponent
    animalEdit: FunctionComponent
}

export type DialogComponentTypes = keyof DialogComponents

export interface ControlledTextProps {
    className?: string
    errors?: DeepMap<Record<string, any>, FieldError>
    register: UseFormMethods['register']
    label: string
    name: string
    styleAsSelect?: boolean
    textFieldProps?: React.ComponentProps<typeof TextField>
}

export interface LazyLoadProps {
    loading: boolean
    numberOfSkeletons?: number
    skeletonProps: React.ComponentProps<typeof Skeleton>
}

export type FilterFormValueKeys = keyof FilterFormValues

export interface AnimalEditFormValues {
    name: string
    description: string
    dateOfBirth: string
    breed: string
    dispositions: MultiselectDropdownOptions
    gender: GenderType
    kind: KindType
    status: StatusType
}
