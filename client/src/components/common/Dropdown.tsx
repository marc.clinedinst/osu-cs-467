import React from 'react'
import { Controller } from 'react-hook-form'
import {
    InputLabel,
    Select,
    MenuItem,
    FormControl,
    FormHelperText,
} from '@material-ui/core'
import { DropdownProps } from './types'
import styled from 'styled-components'

const StyledSelect = styled(Select)<{ $outlined: boolean }>`
    && {
        ${({ $outlined }) => {
            if (!$outlined)
                return `width: 250px;
            padding: 10px;
            margin-bottom: 30px;`
        }}
    }
`
const Dropdown = ({
    className,
    control,
    errors,
    label,
    outlined = false,
    labelId,
    options,
    defaultValue,
    selectProps,
}: DropdownProps) => {
    const menuOptions = options?.map(({ optionId, option }) => (
        <MenuItem key={optionId} value={optionId}>
            {option}
        </MenuItem>
    ))
    const hasError = errors && errors[labelId] ? true : false

    const selectComponent = (
        onChange: Function,
        value: string,
        ref: React.MutableRefObject<object>
    ) => (
        <>
            <InputLabel error={hasError} id={labelId}>
                {label}
            </InputLabel>
            <StyledSelect
                inputRef={ref}
                $outlined={outlined}
                fullWidth
                onChange={e => onChange(e.target.value)}
                value={value}
                error={hasError}
                labelId={labelId}
                inputProps={{
                    'data-testid': `dropdown-${labelId}`,
                }}
                label={label}
                {...selectProps}
            >
                {defaultValue && (
                    <MenuItem value={defaultValue.value}>
                        {defaultValue.label}
                    </MenuItem>
                )}
                {menuOptions}
            </StyledSelect>
            {hasError && (
                <FormHelperText error={hasError}>
                    {`${label} is required`}
                </FormHelperText>
            )}
        </>
    )

    return (
        <div className={className}>
            <Controller
                control={control}
                rules={{ required: true }}
                defaultValue={defaultValue ? defaultValue.value : ''}
                name={labelId}
                render={({ onChange, value, ref }) => {
                    return outlined ? (
                        <FormControl
                            margin='dense'
                            fullWidth
                            variant='outlined'
                        >
                            {selectComponent(onChange, value, ref)}
                        </FormControl>
                    ) : (
                        selectComponent(onChange, value, ref)
                    )
                }}
            />
        </div>
    )
}

export default Dropdown
