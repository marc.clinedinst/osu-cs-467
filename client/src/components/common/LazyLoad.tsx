import React, { FC } from 'react'

import { Skeleton } from '@material-ui/lab'

import { LazyLoadProps } from 'components/common/types'

const generateSkeletons = (
    number: number,
    skeletonProps: React.ComponentProps<typeof Skeleton>
) => {
    const skeletonComponents = []
    for (let i = 0; i < number; i++) {
        skeletonComponents.push(
            <Skeleton
                data-testid='lazy-load-skeleton'
                key={i}
                {...skeletonProps}
            />
        )
    }
    return skeletonComponents
}

const LazyLoad: FC<LazyLoadProps> = ({
    loading,
    numberOfSkeletons = 1,
    skeletonProps,
    children: Component,
}) => {
    return (
        <>
            {loading
                ? generateSkeletons(numberOfSkeletons, skeletonProps)
                : Component}
        </>
    )
}

export default LazyLoad
