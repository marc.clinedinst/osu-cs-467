import React, { FunctionComponent } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'app/store'

import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core'
import Dropdown from 'components/common/Dropdown'
import MultiSelect from 'components/common/MultiSelect'
import ControlledText from 'components/common/ControlledText'

import { closeDialog } from 'ducks/customDialogSlice'
import getAnimals from 'thunks/getAnimals'
import { convertToMultiSelectOptions } from 'utils/sanitize'
import { saveFilters } from 'ducks/filtersSlice'

import { FilterFormValues } from 'components/common/types'
import {
    DATERANGE_OPTIONS,
    GENDER_DROPDOWN_OPTIONS,
    KIND_DROPDOWN_OPTIONS,
    STATUS_OPTIONS,
} from 'constants/constants'
import useDispositions from 'hooks/useDispositions'

const FilterContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 350px;
    max-width: 100%;
    align-items: center;
    margin: 0;
    @media (min-width: 768px) {
        width: 600px;
    }
`

const StyledDialogContent = styled(DialogContent)`
    && {
        display: grid;
    }
    grid-template-columns: 1fr;
    @media (min-width: 768px) {
        grid-template-columns: 275px 275px;
    }
`

const FiltersDialogContent: FunctionComponent = () => {
    const defaultValues = useSelector(state => state.filters)
    const { handleSubmit, register, control } = useForm({
        defaultValues,
    })

    const dispatch = useDispatch()

    const data = useDispositions()

    const onSubmit: SubmitHandler<FilterFormValues> = formValues => {
        dispatch(saveFilters(formValues))
        dispatch(getAnimals())
        dispatch(closeDialog())
    }

    return (
        <>
            <DialogTitle
                data-testid='search-dialog-header'
                id='form-dialog-title'
            >
                Filters
            </DialogTitle>

            <form
                data-testid='search-dialog-search-form'
                onSubmit={handleSubmit(onSubmit)}
            >
                <FilterContainer>
                    <StyledDialogContent>
                        <Dropdown
                            defaultValue={{ value: 'all', label: 'All' }}
                            control={control}
                            label='Kind'
                            labelId='kind'
                            options={KIND_DROPDOWN_OPTIONS}
                        />
                        <ControlledText
                            register={() => register()}
                            styleAsSelect
                            label='Breed'
                            name='breed'
                            textFieldProps={{ placeholder: 'All' }}
                        />
                        <Dropdown
                            defaultValue={{ value: 'all', label: 'All' }}
                            control={control}
                            options={GENDER_DROPDOWN_OPTIONS}
                            label='Gender'
                            labelId='gender'
                        />
                        <Dropdown
                            control={control}
                            options={DATERANGE_OPTIONS}
                            label='Date Added'
                            labelId='daysBeforeToday'
                            defaultValue={{ value: '-1', label: 'All' }}
                        />
                        <MultiSelect
                            control={control}
                            placeholder='All'
                            label='Dispositions'
                            labelId='dispositions'
                            options={convertToMultiSelectOptions(
                                data?.animalDispositions ?? []
                            )}
                        />
                        <Dropdown
                            defaultValue={{ value: 'all', label: 'All' }}
                            control={control}
                            label='Status'
                            labelId='status'
                            data-testid='search-dialog-dropdown-status'
                            options={STATUS_OPTIONS}
                        />
                    </StyledDialogContent>
                </FilterContainer>
                <DialogActions>
                    <Button
                        data-testid='search-dialog-search-button'
                        type='submit'
                        color='primary'
                        variant='contained'
                    >
                        Submit
                    </Button>
                </DialogActions>
            </form>
        </>
    )
}

export default FiltersDialogContent
