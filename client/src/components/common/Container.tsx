import styled from 'styled-components'
const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 40px 0;
    min-height: calc(100vh - 144px);
`
export default Container
