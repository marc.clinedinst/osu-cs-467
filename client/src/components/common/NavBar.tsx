import React, { FunctionComponent } from 'react'
import styled from 'styled-components'
import { AppBar, IconButton, Toolbar } from '@material-ui/core'
import { useDispatch, useSelector } from 'app/store'
import history from 'app/history'

import Typography from 'components/common/StyledTypography'
import FilterListIcon from '@material-ui/icons/FilterList'
import SideDrawer from './SideDrawer'
import { ReactComponent as Logo } from '../../paw.svg'
import SvgIcon from '@material-ui/core/SvgIcon'
import { openDialog } from 'ducks/customDialogSlice'
import { useLocation } from 'react-router-dom'

const StyledAppBar = styled(AppBar)`
    && {
        min-height: 64px;
    }
`

const Title = styled(Typography)`
    && {
        margin-left: 10px;
        display: flex;
        flex: 1;
    }
`

const StyledTitle = styled(Title)`
    cursor: pointer;
`

const NavBar: FunctionComponent = () => {
    const dispatch = useDispatch()
    const { username } = useSelector(state => state.user)
    const location = useLocation()

    return (
        <StyledAppBar data-testid='navbar' position='static'>
            <Toolbar>
                <SvgIcon>
                    <Logo />
                </SvgIcon>
                <StyledTitle
                    variant='h6'
                    data-testid='navbar-title'
                    onClick={() => {
                        if (username) history.push('/search')
                    }}
                >
                    Paw Swipe
                </StyledTitle>
                {username && location.pathname === '/search' && (
                    <IconButton
                        color='inherit'
                        data-testid='navbar-filter'
                        onClick={() => {
                            dispatch(openDialog('filter'))
                        }}
                    >
                        <FilterListIcon />
                    </IconButton>
                )}
                <SideDrawer />
            </Toolbar>
        </StyledAppBar>
    )
}

export default NavBar
