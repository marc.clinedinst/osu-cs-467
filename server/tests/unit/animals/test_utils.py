import pytest

from graphql import GraphQLError

from animals.enums import Gender, Kind, Status
from animals.utils import (
    get_accepted_values_string,
    sanitize_file_name,
    value_is_accepted_or_throw_graphql_error,
    value_is_not_blank_or_throw_graphql_error,
)


class TestAnimalsUtils:
    @pytest.mark.parametrize(
        "enum, accepted_values_string",
        [
            [Gender, "'Female', 'Male'"],
            [Kind, "'Cat', 'Dog', 'Other'"],
            [Status, "'Adopted', 'Available', 'In Foster', 'Pending'"],
        ],
    )
    def test_get_accepted_values_string(self, enum, accepted_values_string):
        assert get_accepted_values_string(enum.values) == accepted_values_string

    @pytest.mark.parametrize(
        "original_file_name, sanitized_file_name",
        [
            ["file_name.jpeg", "file_name.jpeg"],
            ["file.name.jpg", "filename.jpg"],
            ["file name.png", "file-name.png"],
            [
                "a.file.name.with.a.bunch.of.periods.jpg",
                "afilenamewithabunchofperiods.jpg",
            ],
            [
                "Screen Shot 2020-12-26 at 6.16.15 PM.png",
                "screen-shot-2020-12-26-at-61615-pm.png",
            ],
            ["a photograph!@#$%^&*().png", "a-photograph.png"],
        ],
    )
    def test_sanitize_filename_returns_expected_filename(
        self, original_file_name, sanitized_file_name
    ):
        assert sanitize_file_name(original_file_name) == sanitized_file_name

    @pytest.mark.parametrize(
        "value, accepted_values, value_name, value_name_plural, expected_error",
        [
            [
                "Invalid",
                Gender.values,
                "gender",
                "genders",
                "The provided gender 'Invalid' is invalid. Accepted genders: 'Female', 'Male'",
            ],
            [
                "Invalid",
                Kind.values,
                "kind",
                "kinds",
                "The provided kind 'Invalid' is invalid. Accepted kinds: 'Cat', 'Dog', 'Other'",
            ],
            [
                "Invalid",
                Status.values,
                "status",
                "statuses",
                "The provided status 'Invalid' is invalid. Accepted statuses: "
                "'Adopted', 'Available', 'In Foster', 'Pending'",
            ],
        ],
    )
    def test_value_is_accepted_or_throw_graphql_error_throws_error_for_invalid_values(
        self, value, accepted_values, value_name, value_name_plural, expected_error
    ):
        with pytest.raises(GraphQLError) as e:
            value_is_accepted_or_throw_graphql_error(
                value, accepted_values, value_name, value_name_plural
            )

        assert str(e.value) == expected_error

    @pytest.mark.parametrize(
        "value, accepted_values, value_name, value_name_plural",
        [
            [
                "Male",
                Gender.values,
                "gender",
                "genders",
            ],
            [
                "Dog",
                Kind.values,
                "kind",
                "kinds",
            ],
            [
                "Adopted",
                Status.values,
                "status",
                "statuses",
            ],
        ],
    )
    def test_value_is_accepted_or_throw_graphql_error_returns_none_for_valid_values(
        self,
        value,
        accepted_values,
        value_name,
        value_name_plural,
    ):
        assert (
            value_is_accepted_or_throw_graphql_error(
                value, accepted_values, value_name, value_name_plural
            )
            is None
        )

    @pytest.mark.parametrize(
        "value_name, expected_error",
        [
            ["breed", "breed cannot be blank."],
            ["dateOfBirth", "dateOfBirth cannot be blank."],
            ["description", "description cannot be blank."],
            ["gender", "gender cannot be blank."],
            ["kind", "kind cannot be blank."],
            ["name", "name cannot be blank."],
            ["status", "status cannot be blank."],
        ],
    )
    def test_value_is_not_blank_or_throw_graphql_error_throws_error_for_blank_values(
        self, value_name, expected_error
    ):
        with pytest.raises(GraphQLError) as e:
            value_is_not_blank_or_throw_graphql_error("", value_name)

        assert str(e.value) == expected_error
