import os

from tests.test_data import TEST_DATA_DIR

TEST_PHOTOGRAPHS_DIR = os.path.join(TEST_DATA_DIR, "photographs")
