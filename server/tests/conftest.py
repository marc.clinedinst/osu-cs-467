import json
import pytest

from django.contrib.auth import get_user_model
from graphene_django.utils.testing import graphql_query

from animals.models import Animal, AnimalDisposition

TOKEN_AUTH_QUERY = """
mutation TokenAuth($username: String!, $password: String!) {
  tokenAuth(username: $username, password: $password) {
    token
    payload
    refreshExpiresIn,
  }
}
"""


@pytest.fixture
def client_query(client):
    def _query(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client)

    return _query


@pytest.fixture
def create_animal():
    def _create_animal(
        breed, date_of_birth, description, dispositions, gender, kind, name, status
    ):
        animal = Animal.objects.create(
            breed=breed,
            date_of_birth=date_of_birth,
            description=description,
            gender=gender,
            kind=kind,
            name=name,
            status=status,
        )

        for disposition in dispositions:
            new_disposition = AnimalDisposition.objects.create(disposition)
            animal.dispositions.add(new_disposition)

        return animal

    return _create_animal


@pytest.fixture
def create_animal_disposition():
    def _create_animal_disposition(description):
        return AnimalDisposition.objects.create(description=description)

    return _create_animal_disposition


@pytest.fixture
def create_user():
    def _create_user(username, password, email="", first_name="", last_name=""):
        return get_user_model().objects.create_user(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password,
            username=username,
        )

    return _create_user


@pytest.fixture
def pet_seeker_access_token(client_query, create_user):
    email = "pet_seeker@email.com"
    username = "pet_seeker_username"
    password = "pet_seeker_password"
    create_user(username, password, email=email)
    response = client_query(
        TOKEN_AUTH_QUERY, variables={"password": password, "username": username}
    )

    return json.loads(response.content)["data"]["tokenAuth"]["token"]


@pytest.fixture
def site_admin_access_token(client_query, create_user):
    email = "site_admin@email.com"
    username = "site_admin_username"
    password = "site_admin_password"
    user = create_user(username, password, email=email)
    user.is_staff = True
    user.save()
    response = client_query(
        TOKEN_AUTH_QUERY, variables={"password": password, "username": username}
    )

    return json.loads(response.content)["data"]["tokenAuth"]["token"]
