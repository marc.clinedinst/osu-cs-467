import json
import pytest

from django.contrib.auth import get_user_model
from datetime import date
from graphql_jwt.utils import jwt_encode

ANIMAL_ONE_DATA = dict(
    breed="Terrier",
    date_of_birth=date(2015, 3, 20),
    description="Meatball is a lazy dog who likes to cuddle and eat treats.",
    dispositions=[],
    gender="Male",
    kind="Dog",
    name="Meatball",
    status="Available",
)

ANIMAL_TWO_DATA = dict(
    breed="British Shorthair",
    date_of_birth=date(2019, 2, 14),
    description="Tina is an active cat who loves chasing mice cuddling people.",
    dispositions=[],
    gender="Female",
    kind="Cat",
    name="Tina",
    status="In Foster",
)

ANIMAL_THREE_DATA = dict(
    breed="Egyptian Gerbil",
    date_of_birth=date(2018, 10, 28),
    description="Reggie is a just a gerbil.",
    dispositions=[],
    gender="Male",
    kind="Other",
    name="Reggie",
    status="Adopted",
)

ANIMALS_DATA = [ANIMAL_ONE_DATA, ANIMAL_TWO_DATA, ANIMAL_THREE_DATA]

GET_USER_BY_USERNAME_QUERY = """
query GetUserByToken($token: String!) {
   userByToken(token: $token)  {
      id
      username
      isStaff
   }
}
 """

GET_USER_FAVORITE_ANIMALS_QUERY = """
query GetFavoriteAnimals {
    favoriteAnimals {
        id,
        name
    }
}
"""

User = get_user_model()


@pytest.mark.django_db
class TestGetUserByToken:
    def test_unauthenticated_user_receives_error_messages_when_querying_for_user(
        self, client_query
    ):
        response = client_query(
            GET_USER_BY_USERNAME_QUERY, variables={"token": "fake-token"}
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["userByToken"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_authenticated_user_receives_error_message_when_querying_by_token_that_does_not_match_to_user(
        self, client_query, pet_seeker_access_token
    ):
        payload = {
            "username": "person-not-in-database",
            "exp": 1613261403,
            "origIat": 1613261103,
        }
        jwt = jwt_encode(payload)
        response = client_query(
            GET_USER_BY_USERNAME_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"token": f"{jwt}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["userByToken"] is None
        assert response_errors[0]["message"] == "User does not exist."

    def test_authenticated_user_receives_correct_animal_when_querying_for_id_that_exists(
        self, client_query, site_admin_access_token
    ):
        response = client_query(
            GET_USER_BY_USERNAME_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"token": f"{site_admin_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["userByToken"]["isStaff"]
        assert response_data["userByToken"]["username"] == "site_admin_username"


@pytest.mark.django_db
class TestGetUserFavoriteAnimalsQuery:
    def test_unauthenticated_user_receives_error(self, client_query):
        response = client_query(GET_USER_FAVORITE_ANIMALS_QUERY)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["favoriteAnimals"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_user_receives_empty_list_when_no_favorites_have_been_added(
        self, client_query, pet_seeker_access_token
    ):
        response = client_query(
            GET_USER_FAVORITE_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["favoriteAnimals"] == []

    def test_user_receives_correct_animal_when_one_favorite_has_been_added(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        User.objects.first().favorite_animals.add(animal)
        response = client_query(
            GET_USER_FAVORITE_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["favoriteAnimals"] == [
            {"id": str(animal.id), "name": animal.name}
        ]

    def test_user_receives_correct_animals_when_multiple_favorites_have_been_added(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal_data) for animal_data in ANIMALS_DATA]

        for animal in animals:
            User.objects.first().favorite_animals.add(animal)

        response = client_query(
            GET_USER_FAVORITE_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["favoriteAnimals"] == [
            {"id": str(animal.id), "name": animal.name} for animal in animals
        ]
