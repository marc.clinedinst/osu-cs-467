import jwt
import pytest

from django.conf import settings

PASSWORD = "test_password"
USERNAME = "test_user"


@pytest.mark.django_db
class TestUserAuthenticationWithValidCredentials:
    def test_valid_login_response_returns_status_code_200(self, client, create_user):
        create_user(USERNAME, PASSWORD)
        response = client.post(
            "/api/token/", data={"username": USERNAME, "password": PASSWORD}
        )

        assert response.status_code == 200

    def test_valid_login_response_returns_correct_json_keys(self, client, create_user):
        create_user(USERNAME, PASSWORD)
        response = client.post(
            "/api/token/", data={"username": USERNAME, "password": PASSWORD}
        )

        assert set(response.data.keys()) == {"access", "refresh"}

    def test_valid_login_response_returns_access_token_for_correct_user(
        self, client, create_user
    ):
        user = create_user(USERNAME, PASSWORD)
        response = client.post(
            "/api/token/", data={"username": USERNAME, "password": PASSWORD}
        )
        decoded_access_token = jwt.decode(
            response.data["access"], settings.SECRET_KEY, algorithms=["HS256"]
        )

        assert decoded_access_token["id"] == user.id
        assert decoded_access_token["token_type"] == "access"

    def test_valid_login_response_returns_refresh_token_for_correct_user(
        self, client, create_user
    ):
        user = create_user(USERNAME, PASSWORD)
        response = client.post(
            "/api/token/", data={"username": USERNAME, "password": PASSWORD}
        )
        decoded_refresh_token = jwt.decode(
            response.data["refresh"], settings.SECRET_KEY, algorithms=["HS256"]
        )

        assert decoded_refresh_token["id"] == user.id
        assert decoded_refresh_token["token_type"] == "refresh"

    def test_user_with_valid_credentials_can_access_protected_view(
        self, client, create_user
    ):
        create_user(USERNAME, PASSWORD)
        jwt_response = client.post(
            "/api/token/", data={"username": USERNAME, "password": PASSWORD}
        )
        access_token = jwt_response.data["access"]
        protected_response = client.get(
            "/api/protected/", {}, HTTP_AUTHORIZATION=f"Bearer {access_token}"
        )

        assert protected_response.status_code == 200
        assert protected_response.data == {"message": "This is a protected view."}


@pytest.mark.django_db
class TestAuthenticationWithInvalidCredentials:
    def test_user_can_not_log_in_with_invalid_credentials(self, client):
        response = client.post(
            "/api/token/",
            {"username": "invalid_username", "password": "invalid_password"},
        )
        response_body = response.json()

        assert response.status_code == 401
        assert response_body == {
            "detail": "No active account found with the given credentials"
        }

    @pytest.mark.parametrize(
        "test_data",
        [
            pytest.param(
                {
                    "credentials": {},
                    "expected_error": {
                        "password": ["This field is required."],
                        "username": ["This field is required."],
                    },
                },
                id="Missing both password and username.",
            ),
            pytest.param(
                {
                    "credentials": {"username": "the_username"},
                    "expected_error": {"password": ["This field is required."]},
                },
                id="Missing only password.",
            ),
            pytest.param(
                {
                    "credentials": {"password": "the_password"},
                    "expected_error": {"username": ["This field is required."]},
                },
                id="Missing only username.",
            ),
            pytest.param(
                {
                    "credentials": {"password": "", "username": ""},
                    "expected_error": {
                        "password": ["This field may not be blank."],
                        "username": ["This field may not be blank."],
                    },
                },
                id="Blank username and password.",
            ),
            pytest.param(
                {
                    "credentials": {"password": "", "username": "the_username"},
                    "expected_error": {"password": ["This field may not be blank."]},
                },
                id="Blank password.",
            ),
            pytest.param(
                {
                    "credentials": {"password": "the_password", "username": ""},
                    "expected_error": {"username": ["This field may not be blank."]},
                },
                id="Blank username.",
            ),
        ],
    )
    def test_user_receives_correct_error_message_when_leaving_values_blank(
        self, client, test_data
    ):
        response = client.post("/api/token/", test_data["credentials"])
        response_body = response.data

        assert response.status_code == 400
        assert response_body == test_data["expected_error"]

    def test_user_with_invalid_jwt_cannot_access_protected_view(
        self, client, create_user
    ):
        protected_response = client.get(
            "/api/protected/", {}, HTTP_AUTHORIZATION="Bearer fake_token"
        )
        protected_response_data = protected_response.data

        assert protected_response.status_code == 401
        assert protected_response_data["code"] == "token_not_valid"
        assert (
            protected_response_data["detail"]
            == "Given token not valid for any token type"
        )
        assert protected_response_data["messages"] == [
            {
                "token_class": "AccessToken",
                "token_type": "access",
                "message": "Token is invalid or expired",
            }
        ]
