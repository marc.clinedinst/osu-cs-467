import pytest

from datetime import date

ANIMAL_ONE_DATA = dict(
    breed="Terrier",
    date_of_birth=date(2015, 3, 20),
    description="Meatball is a lazy dog who likes to cuddle and eat treats.",
    dispositions=[],
    gender="Male",
    kind="Dog",
    name="Meatball",
    status="Available",
)

ANIMAL_TWO_DATA = dict(
    breed="British Shorthair",
    date_of_birth=date(2019, 2, 14),
    description="Tina is an active cat who loves chasing mice cuddling people.",
    dispositions=[],
    gender="Female",
    kind="Cat",
    name="Tina",
    status="In Foster",
)

ANIMAL_THREE_DATA = dict(
    breed="Egyptian Gerbil",
    date_of_birth=date(2018, 10, 28),
    description="Reggie is a just a gerbil.",
    dispositions=[],
    gender="Male",
    kind="Other",
    name="Reggie",
    status="Adopted",
)

ANIMALS_DATA = [ANIMAL_ONE_DATA, ANIMAL_TWO_DATA, ANIMAL_THREE_DATA]

PASSWORD = "the_password"

USERNAME = "the_username"


@pytest.mark.django_db
class TestCustomUserModel:
    def test_user_can_have_favorite_animal(self, create_animal, create_user):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        user = create_user(USERNAME, PASSWORD)

        assert list(user.favorite_animals.all()) == []

        for animal in animals:
            user.favorite_animals.add(animal)

        assert list(user.favorite_animals.all()) == animals

    def test_user_is_opted_into_emails_by_default(self, create_user):
        user = create_user(USERNAME, PASSWORD)

        assert user.email_opt_in is True

    def test_user_can_opt_out_of_emails(self, create_user):
        user = create_user(USERNAME, PASSWORD)
        user.email_opt_in = False
        user.save()

        assert user.email_opt_in is False
