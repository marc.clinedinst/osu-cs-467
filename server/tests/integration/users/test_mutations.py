import json
import jwt
import pytest

from datetime import date
from django.conf import settings
from django.contrib.auth import get_user_model

ANIMAL_ONE_DATA = dict(
    breed="Terrier",
    date_of_birth=date(2015, 3, 20),
    description="Meatball is a lazy dog who likes to cuddle and eat treats.",
    dispositions=[],
    gender="Male",
    kind="Dog",
    name="Meatball",
    status="Available",
)

ANIMAL_TWO_DATA = dict(
    breed="British Shorthair",
    date_of_birth=date(2019, 2, 14),
    description="Tina is an active cat who loves chasing mice cuddling people.",
    dispositions=[],
    gender="Female",
    kind="Cat",
    name="Tina",
    status="In Foster",
)

ANIMAL_THREE_DATA = dict(
    breed="Egyptian Gerbil",
    date_of_birth=date(2018, 10, 28),
    description="Reggie is a just a gerbil.",
    dispositions=[],
    gender="Male",
    kind="Other",
    name="Reggie",
    status="Adopted",
)

ANIMALS_DATA = [ANIMAL_ONE_DATA, ANIMAL_TWO_DATA, ANIMAL_THREE_DATA]

EMAIL = "test@email.com"

PASSWORD = "the_password"

PROTECTED_QUERY = """
query {
  protected
}
"""

REGISTER_USER_MUTATION = """
mutation registerUser($confirmPassword: String!, $email: String!, $password: String!, $username: String!) {
    registerUser(confirmPassword: $confirmPassword, email: $email, password: $password, username: $username) {
        user {
            id,
            email,
            username
        }
    }
}
"""

TOKEN_AUTH_MUTATION = """
mutation TokenAuth($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
        payload,
        refreshExpiresIn,
        token,
        user {
            isStaff,
            username
        }
    }
}
"""

TOGGLE_FAVORITE_ANIMAL_MUTATION = """
mutation ToggleFavoriteAnimal($id: ID!) {
    toggleFavoriteAnimal(id: $id) {
        id
    }
}
"""

TOGGLE_EMAIL_OPT_IN_MUTATION = """
mutation ToggleEmailOptIn {
    toggleEmailOptIn {
        optedIn
    }
}
"""

USERNAME = "the_username"

User = get_user_model()


@pytest.mark.django_db
class TestUserAuthenticationWithValidCredentials:
    def test_token_auth_mutation_with_valid_credentials_returns_jwt_for_correct_user(
        self, client_query, create_user
    ):
        create_user(USERNAME, PASSWORD)
        response = client_query(
            TOKEN_AUTH_MUTATION, variables={"password": PASSWORD, "username": USERNAME}
        )
        response_data = json.loads(response.content)["data"]["tokenAuth"]
        decoded_access_token = jwt.decode(
            response_data["token"], settings.SECRET_KEY, algorithms=["HS256"]
        )

        assert decoded_access_token["username"] == USERNAME

    def test_token_auth_mutation_for_pet_seeker_returns_correct_is_staff_value(
        self, client_query, create_user
    ):
        create_user(USERNAME, PASSWORD)
        response = client_query(
            TOKEN_AUTH_MUTATION, variables={"password": PASSWORD, "username": USERNAME}
        )
        response_data = json.loads(response.content)["data"]
        response_user = response_data["tokenAuth"]["user"]

        assert response_user["username"] == USERNAME
        assert not response_user["isStaff"]

    def test_token_auth_mutation_for_site_admin_returns_correct_is_staff_value(
        self, client_query, create_user
    ):
        user = create_user(USERNAME, PASSWORD)
        user.is_staff = True
        user.save()
        response = client_query(
            TOKEN_AUTH_MUTATION, variables={"password": PASSWORD, "username": USERNAME}
        )
        response_data = json.loads(response.content)["data"]
        response_user = response_data["tokenAuth"]["user"]

        assert response_user["username"] == USERNAME
        assert response_user["isStaff"]

    def test_user_with_valid_credentials_can_access_protected_query(
        self, client_query, create_user
    ):
        create_user(USERNAME, PASSWORD)
        response = client_query(
            TOKEN_AUTH_MUTATION, variables={"password": PASSWORD, "username": USERNAME}
        )
        access_token = json.loads(response.content)["data"]["tokenAuth"]["token"]

        response = client_query(
            PROTECTED_QUERY, headers={"HTTP_AUTHORIZATION": f"JWT {access_token}"}
        )
        response_data = json.loads(response.content)["data"]
        assert response_data == {"protected": "This is a protected query."}


@pytest.mark.django_db
class TestUserRegistrationMutation:
    def test_mutation_returns_error_message_when_password_confirmation_is_not_present(
        self, client_query
    ):
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "email": EMAIL,
                "password": PASSWORD,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$confirmPassword" of required type "String!" was not provided.'
        )
        assert len(User.objects.all()) == 0

    def test_mutation_returns_error_message_when_email_is_not_present(
        self, client_query
    ):
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "password": PASSWORD,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$email" of required type "String!" was not provided.'
        )
        assert len(User.objects.all()) == 0

    def test_mutation_returns_error_message_when_email_is_invalid(self, client_query):
        invalid_email = "invalid_email"
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "email": invalid_email,
                "password": PASSWORD,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f"The provided email '{invalid_email}' is invalid."
        )
        assert len(User.objects.all()) == 0

    def test_mutation_returns_error_message_when_password_is_not_present(
        self, client_query
    ):
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "email": EMAIL,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$password" of required type "String!" was not provided.'
        )
        assert len(User.objects.all()) == 0

    def test_mutation_returns_error_message_when_username_is_not_present(
        self, client_query
    ):
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "email": EMAIL,
                "password": PASSWORD,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$username" of required type "String!" was not provided.'
        )
        assert len(User.objects.all()) == 0

    def test_mutation_returns_error_message_when_passwords_do_not_match(
        self, client_query
    ):
        confirm_password = "password_confirmation_does_not_match"
        password = "the_actual_password"

        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": confirm_password,
                "email": EMAIL,
                "password": password,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["registerUser"] is None
        assert len(response_errors) == 1
        assert response_errors[0]["message"] == "Passwords must match."
        assert len(User.objects.all()) == 0

    def test_mutation_does_not_create_user_when_username_already_exists(
        self, client_query, create_user
    ):
        existing_user = create_user(USERNAME, PASSWORD)

        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "email": EMAIL,
                "password": PASSWORD,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]
        users = User.objects.all()

        assert response_data["registerUser"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f"A user with the username '{USERNAME}' already exists."
        )
        assert len(users) == 1
        assert users[0] == existing_user

    def test_mutation_creates_user_when_valid_data_is_provided(self, client_query):
        response = client_query(
            REGISTER_USER_MUTATION,
            variables={
                "confirmPassword": PASSWORD,
                "email": EMAIL,
                "password": PASSWORD,
                "username": USERNAME,
            },
        )
        response_content = json.loads(response.content)
        user_data = response_content["data"]["registerUser"]["user"]
        user = User.objects.first()

        assert len(User.objects.all()) == 1
        assert str(user.id) == user_data["id"]
        assert user.email == user_data["email"]
        assert user.username == user_data["username"]
        assert user.check_password(PASSWORD)


@pytest.mark.django_db
class TestToggleEmailOptInMutation:
    def test_unauthenticated_user_receives_error_message(self, client_query):
        response = client_query(TOGGLE_EMAIL_OPT_IN_MUTATION)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["toggleEmailOptIn"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_user_can_toggle_email_opt_in_off(
        self, client_query, pet_seeker_access_token
    ):
        user = User.objects.first()

        assert user.email_opt_in is True

        response = client_query(
            TOGGLE_EMAIL_OPT_IN_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        user = User.objects.first()

        assert response_data["toggleEmailOptIn"]["optedIn"] is False
        assert user.email_opt_in is False

    def test_user_can_toggle_email_opt_in_on(
        self, client_query, pet_seeker_access_token
    ):
        user = User.objects.first()
        user.email_opt_in = False
        user.save()

        assert user.email_opt_in is False

        response = client_query(
            TOGGLE_EMAIL_OPT_IN_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        user = User.objects.first()

        assert response_data["toggleEmailOptIn"]["optedIn"] is True
        assert user.email_opt_in is True


@pytest.mark.django_db
class TestToggleFavoriteAnimalMutation:
    def test_unauthenticated_user_receives_error_message_when_querying_for_user(
        self, client_query, create_animal
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        response = client_query(
            TOGGLE_FAVORITE_ANIMAL_MUTATION, variables={"id": animal.id}
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["toggleFavoriteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_user_receives_error_message_when_attempting_to_add_animal_that_does_not_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        response = client_query(
            TOGGLE_FAVORITE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal.id + 1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["toggleFavoriteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"Animal with ID {animal.id + 1} does not exist."
        )

    def test_user_can_add_animal_to_favorites(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        user = User.objects.first()

        assert list(user.favorite_animals.all()) == []

        response = client_query(
            TOGGLE_FAVORITE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["toggleFavoriteAnimal"] == {"id": str(animal.id)}
        assert list(user.favorite_animals.all()) == [animal]

    def test_user_can_remove_animal_from_favorites(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        user = User.objects.first()
        user.favorite_animals.add(animal)

        assert list(user.favorite_animals.all()) == [animal]

        response = client_query(
            TOGGLE_FAVORITE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["toggleFavoriteAnimal"] == {"id": str(animal.id)}
        assert list(user.favorite_animals.all()) == []

    def test_user_can_remove_animal_from_favorites_when_multiple_favorites_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal_data) for animal_data in ANIMALS_DATA]
        user = User.objects.first()

        for animal in animals:
            User.objects.first().favorite_animals.add(animal)

        assert list(user.favorite_animals.all()) == animals

        animal_to_remove = animals.pop()

        response = client_query(
            TOGGLE_FAVORITE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal_to_remove.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["toggleFavoriteAnimal"] == {"id": str(animal_to_remove.id)}
        assert list(user.favorite_animals.all()) == animals
