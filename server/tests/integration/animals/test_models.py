import pytest

from datetime import date

ANIMAL_DATA = dict(
    breed="Terrier",
    date_of_birth=date(2015, 3, 20),
    description="Meatball is a lazy dog who likes to cuddle and eat treats.",
    dispositions=[],
    gender="Male",
    kind="Dog",
    name="Meatball",
    status="Available",
)


@pytest.mark.django_db
class TestAnimalsModel:
    def test_animal_creation(self, create_animal):
        animal = create_animal(**ANIMAL_DATA)

        assert str(animal) == ANIMAL_DATA["name"]
        assert animal.breed == ANIMAL_DATA["breed"]
        assert animal.date_added == date.today()
        assert animal.date_of_birth == ANIMAL_DATA["date_of_birth"]
        assert animal.description == ANIMAL_DATA["description"]
        assert list(animal.dispositions.all()) == []
        assert animal.gender == ANIMAL_DATA["gender"]
        assert type(animal.id) == int
        assert animal.kind == ANIMAL_DATA["kind"]
        assert animal.name == ANIMAL_DATA["name"]
        assert animal.status == ANIMAL_DATA["status"]

    @pytest.mark.parametrize(
        "dispositions_to_add",
        [
            [],
            ["Moody"],
            ["Aggressive", "Bad with children", "Bad with other pets"],
            [f"Disposition #{number}" for number in range(1, 21)],
        ],
    )
    def test_animal_can_have_dispositions(
        self, create_animal, create_animal_disposition, dispositions_to_add
    ):
        animal = create_animal(**ANIMAL_DATA)

        for disposition in dispositions_to_add:
            animal.dispositions.add(create_animal_disposition(disposition))

        dispositions = [str(disposition) for disposition in animal.dispositions.all()]

        assert dispositions == dispositions_to_add

    @pytest.mark.parametrize(
        "photographs_to_add",
        [
            [],
            ["https://pawswipe.s3.amazonaws.com/media/photographs/photograph_1.jpg"],
            [
                f"https://pawswipe.s3.amazonaws.com/media/photographs/photograph_{iteration}.jpg"
                for iteration in range(10)
            ],
        ],
    )
    def test_animals_can_have_photographs(self, create_animal, photographs_to_add):
        animal = create_animal(**ANIMAL_DATA)

        for photograph in photographs_to_add:
            animal.photographs.create(url=photograph)

        photographs = [str(photograph) for photograph in animal.photographs.all()]

        assert photographs == photographs_to_add

    @pytest.mark.parametrize(
        "status_updates_to_add",
        [
            [],
            ["status_update"],
            [f"status_update_{iteration}" for iteration in range(10)],
        ],
    )
    def test_animals_can_have_status_updates(
        self, create_animal, status_updates_to_add
    ):
        animal = create_animal(**ANIMAL_DATA)

        for status_update in status_updates_to_add:
            animal.status_updates.create(status_text=status_update)

        status_updates = list(
            reversed(
                [str(status_update) for status_update in animal.status_updates.all()]
            )
        )

        assert status_updates == status_updates_to_add
