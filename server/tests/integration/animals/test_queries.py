import json
import pytest

from datetime import date, timedelta

ANIMAL_ONE_DATA = dict(
    breed="Terrier",
    date_of_birth=date(2015, 3, 20),
    description="Meatball is a lazy dog who likes to cuddle and eat treats.",
    dispositions=[],
    gender="Male",
    kind="Dog",
    name="Meatball",
    status="Available",
)

ANIMAL_TWO_DATA = dict(
    breed="British Shorthair",
    date_of_birth=date(2019, 2, 14),
    description="Tina is an active cat who loves chasing mice cuddling people.",
    dispositions=[],
    gender="Female",
    kind="Cat",
    name="Tina",
    status="In Foster",
)

ANIMAL_THREE_DATA = dict(
    breed="Egyptian Gerbil",
    date_of_birth=date(2018, 10, 28),
    description="Reggie is a just a gerbil.",
    dispositions=[],
    gender="Male",
    kind="Other",
    name="Reggie",
    status="Adopted",
)

ANIMALS_DATA = [ANIMAL_ONE_DATA, ANIMAL_TWO_DATA, ANIMAL_THREE_DATA]

GET_ANIMAL_BY_ID_QUERY = """
query GetAnimalById($id: ID!) {
    animalById(id: $id) {
        breed,
        dateOfBirth,
        description,
        dispositions {
              id,
              description
        },
        gender,
        id,
        kind,
        name,
        photographs {
            id,
            url
        },
        status,
        statusUpdates {
            id,
            statusText
        }
    }
}
"""

GET_ANIMAL_DISPOSITIONS_QUERY = """
query {
    animalDispositions {
        id,
        description
    }
}
"""

GET_ANIMAL_STATUS_UPDATES = """
query GetAnimalStatusUpdates {
    animalStatusUpdates {
        id,
        statusText,
    }
}
"""

GET_ANIMALS_QUERY = """
query GetAnimals(
    $breed: String,
    $daysBeforeToday: Int,
    $dispositions: [ID],
    $gender:String,
    $kind: String,
    $status: String
) {
    animals(
        breed: $breed,
        daysBeforeToday: $daysBeforeToday,
        dispositions: $dispositions,
        gender: $gender,
        kind: $kind,
        status: $status
    ) {
        breed,
        id,
        name,
        dateAdded,
        dateOfBirth,
        description,
        dispositions {
            id,
            description
        },
        gender,
        kind,
        photographs {
            id,
            url
        },
        status,
        statusUpdates {
            id,
            statusText
        }
    }
}
"""

TODAY_FORMATTED = date.strftime(date.today(), "%Y-%m-%d")


@pytest.mark.django_db
class TestGetAnimalById:
    def test_unauthenticated_user_receives_error_message_when_querying_for_animals(
        self, client_query
    ):
        response = client_query(GET_ANIMAL_BY_ID_QUERY, variables={"id": 1})
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animalById"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_authenticated_user_receives_error_message_when_querying_by_id_that_does_not_exist(
        self, client_query, pet_seeker_access_token
    ):
        response = client_query(
            GET_ANIMAL_BY_ID_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": 1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animalById"] is None
        assert response_errors[0]["message"] == "Animal with ID 1 does not exist."

    def test_authenticated_user_receives_error_message_when_querying_for_invalid_id(
        self, client_query, pet_seeker_access_token
    ):
        response = client_query(
            GET_ANIMAL_BY_ID_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": "abc"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animalById"] is None
        assert (
            response_errors[0]["message"]
            == "Field 'id' expected a number but got 'abc'."
        )

    def test_authenticated_user_receives_correct_animal_when_querying_for_id_that_exists(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        create_animal(**ANIMAL_ONE_DATA)
        animal_two = create_animal(**ANIMAL_TWO_DATA)
        photograph = animal_two.photographs.create(
            url="https://www.imagehost.com/image.gif"
        )
        status_update = animal_two.status_updates.create(status_text="status_text")
        response = client_query(
            GET_ANIMAL_BY_ID_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal_two.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["animalById"] == {
            "breed": ANIMAL_TWO_DATA["breed"],
            "dateOfBirth": date.strftime(ANIMAL_TWO_DATA["date_of_birth"], "%Y-%m-%d"),
            "description": ANIMAL_TWO_DATA["description"],
            "dispositions": [],
            "gender": ANIMAL_TWO_DATA["gender"],
            "id": str(animal_two.id),
            "kind": ANIMAL_TWO_DATA["kind"],
            "name": ANIMAL_TWO_DATA["name"],
            "photographs": [{"id": str(photograph.id), "url": photograph.url}],
            "status": ANIMAL_TWO_DATA["status"],
            "statusUpdates": [
                {"id": str(status_update.id), "statusText": status_update.status_text}
            ],
        }


@pytest.mark.django_db
class TestGetAnimalDispositions:
    def test_unauthenticated_user_receives_error_message_when_querying_for_animals(
        self, client_query
    ):
        response = client_query(GET_ANIMAL_DISPOSITIONS_QUERY)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animalDispositions"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_authenticated_user_receives_empty_list_when_no_animals_exist(
        self, pet_seeker_access_token, client_query
    ):
        response = client_query(
            GET_ANIMAL_DISPOSITIONS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]

        assert response_data == {"animalDispositions": []}

    def test_authenticated_user_receives_list_with_correct_data_when_one_animal_disposition_exists(
        self, client_query, create_animal_disposition, pet_seeker_access_token
    ):
        animal_disposition = create_animal_disposition("Moody")
        response = client_query(
            GET_ANIMAL_DISPOSITIONS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]

        assert response_data["animalDispositions"] == [
            {
                "description": animal_disposition.description,
                "id": str(animal_disposition.id),
            }
        ]

    def test_authenticated_user_receives_list_with_correct_data_when_multiple_animal_dispositions_exist(
        self, client_query, create_animal_disposition, pet_seeker_access_token
    ):
        animal_disposition_one = create_animal_disposition("Aggressive")
        animal_disposition_two = create_animal_disposition("Bad with children")
        animal_disposition_three = create_animal_disposition("Bad with other animals")
        response = client_query(
            GET_ANIMAL_DISPOSITIONS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]

        assert response_data["animalDispositions"] == [
            {
                "description": animal_disposition_one.description,
                "id": str(animal_disposition_one.id),
            },
            {
                "description": animal_disposition_two.description,
                "id": str(animal_disposition_two.id),
            },
            {
                "description": animal_disposition_three.description,
                "id": str(animal_disposition_three.id),
            },
        ]


@pytest.mark.django_db
class TestGetAnimalsQueryNoFilters:
    def test_unauthenticated_user_receives_error_message_when_querying_for_animals(
        self, client_query
    ):
        response = client_query(GET_ANIMALS_QUERY)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animals"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_authenticated_user_receives_empty_list_when_no_animals_exist(
        self, pet_seeker_access_token, client_query
    ):
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]

        assert response_data == {"animals": []}

    @pytest.mark.parametrize(
        "dispositions_to_add",
        [
            [],
            ["Moody"],
            ["Aggressive", "Bad with children", "Bad with other pets"],
            [f"Disposition #{number}" for number in range(1, 21)],
        ],
    )
    def test_authenticated_user_receives_list_with_correct_animal_data_when_one_animal_exists(
        self,
        pet_seeker_access_token,
        client_query,
        create_animal,
        create_animal_disposition,
        dispositions_to_add,
    ):
        animal = create_animal(**ANIMAL_ONE_DATA)
        dispositions = []

        for disposition in dispositions_to_add:
            new_disposition = create_animal_disposition(disposition)
            dispositions.append(new_disposition)
            animal.dispositions.add(new_disposition)

        photograph = animal.photographs.create(
            url="https://www.imageserver.com/image.jpg"
        )
        status_update = animal.status_updates.create(status_text="status text")

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]

        assert response_data["animals"] == [
            {
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [
                    {"id": str(disposition.id), "description": disposition.description}
                    for disposition in dispositions
                ],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animal.id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_ONE_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_authenticated_user_receives_list_with_correct_animal_data_when_multiple_animals_exist(
        self,
        pet_seeker_access_token,
        client_query,
        create_animal,
        create_animal_disposition,
    ):
        animal_one = create_animal(**ANIMAL_ONE_DATA)
        animal_one_dispositions = []

        for disposition in ["Aggressive", "Moody"]:
            new_disposition = create_animal_disposition(disposition)
            animal_one_dispositions.append(new_disposition)
            animal_one.dispositions.add(new_disposition)

        animal_two = create_animal(**ANIMAL_TWO_DATA)
        animal_two_dispositions = []

        for disposition in [
            "Affectionate",
            "Good with children",
            "God with other animals",
        ]:
            new_disposition = create_animal_disposition(disposition)
            animal_two_dispositions.append(new_disposition)
            animal_two.dispositions.add(new_disposition)

        animal_two_photograph = animal_two.photographs.create(
            url="http://www.imageserver.com/image.png"
        )
        animal_two_status_update = animal_two.status_updates.create(
            status_text="status_text"
        )

        animal_three = create_animal(**ANIMAL_THREE_DATA)
        animal_three_dispositions = []

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_data = json.loads(response.content)["data"]
        animals = response_data["animals"]

        assert animals == [
            {
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [
                    {"id": str(disposition.id), "description": disposition.description}
                    for disposition in animal_one_dispositions
                ],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animal_one.id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "photographs": [],
                "status": ANIMAL_ONE_DATA["status"],
                "statusUpdates": [],
            },
            {
                "breed": ANIMAL_TWO_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_TWO_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_TWO_DATA["description"],
                "dispositions": [
                    {"id": str(disposition.id), "description": disposition.description}
                    for disposition in animal_two_dispositions
                ],
                "gender": ANIMAL_TWO_DATA["gender"],
                "id": str(animal_two.id),
                "kind": ANIMAL_TWO_DATA["kind"],
                "name": ANIMAL_TWO_DATA["name"],
                "photographs": [
                    {
                        "id": str(animal_two_photograph.id),
                        "url": animal_two_photograph.url,
                    }
                ],
                "status": ANIMAL_TWO_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(animal_two_status_update.id),
                        "statusText": animal_two_status_update.status_text,
                    }
                ],
            },
            {
                "breed": ANIMAL_THREE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_THREE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_THREE_DATA["description"],
                "dispositions": [
                    {"id": str(disposition.id), "description": disposition.description}
                    for disposition in animal_three_dispositions
                ],
                "gender": ANIMAL_THREE_DATA["gender"],
                "id": str(animal_three.id),
                "kind": ANIMAL_THREE_DATA["kind"],
                "name": ANIMAL_THREE_DATA["name"],
                "photographs": [],
                "status": ANIMAL_THREE_DATA["status"],
                "statusUpdates": [],
            },
        ]


@pytest.mark.django_db
class TestGetAnimalStatusUpdates:
    def test_unauthenticated_user_receives_error(self, client_query):
        response = client_query(GET_ANIMAL_STATUS_UPDATES)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["animalStatusUpdates"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_authenticated_user_receives_empty_list_when_no_status_updates_exist(
        self, client_query, pet_seeker_access_token
    ):
        response = client_query(
            GET_ANIMAL_STATUS_UPDATES,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["animalStatusUpdates"] == []

    def test_authenticated_user_receives_correct_data_when_status_updates_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        status_updates = [
            animal.status_updates.create(status_text=f"status update #{index}")
            for index, animal in enumerate(animals)
        ]

        response = client_query(
            GET_ANIMAL_STATUS_UPDATES,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert response_data["animalStatusUpdates"] == list(
            reversed(
                [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                    for status_update in status_updates
                ]
            )
        )


@pytest.mark.django_db
class TestGetAnimalsWithAllFilters:
    def test_correct_animal_is_returned_when_all_filters_are_applied(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        pet_seeker_access_token,
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        animal_dispositions = [
            create_animal_disposition(disposition)
            for disposition in ["Aggressive", "Moody"]
        ]

        for disposition in animal_dispositions:
            animals[0].dispositions.add(disposition)
            animals[1].dispositions.add(disposition)

        photograph = animals[0].photographs.create(
            url="https://www.imageserver.com/some_image.jpeg"
        )
        status_update = animals[0].status_updates.create(status_text="status text")

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [disposition.id for disposition in animal_dispositions],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animals[0].id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "status": ANIMAL_ONE_DATA["status"],
            },
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [
                    {"description": disposition.description, "id": str(disposition.id)}
                    for disposition in animal_dispositions
                ],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animals[0].id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_ONE_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]


@pytest.mark.django_db
class TestGetAnimalsWithBreedFilter:
    def test_correct_animal_is_returned_when_filtering_by_breed_that_exists(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        photograph = animals[0].photographs.create(
            url="https://www.imageserver.com/some_image.jpeg"
        )
        status_update = animals[0].status_updates.create(status_text="status text")
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"breed": ANIMAL_ONE_DATA["breed"]},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animals[0].id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_ONE_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_breed_does_not_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"breed": "This is not a real breed."},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == []


@pytest.mark.django_db
class TestGetAnimalsWithDaysBeforeTodayFilter:
    @pytest.mark.parametrize("days_before_today", [0, 7, 30, 90, 365])
    def test_correct_animal_is_returned_when_filtering_for_animals_added_a_specific_number_of_days_before_today(
        self, days_before_today, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        desired_animal = animals[0]
        photograph = desired_animal.photographs.create(
            url="https://www.images.com/image.jpeg"
        )
        status_update = desired_animal.status_updates.create(
            status_text="this is the status"
        )
        animals[1].date_added = date.today() - timedelta(days=days_before_today + 1)
        animals[1].save()
        animals[2].date_added = date.today() - timedelta(
            days=(days_before_today + 1) * 2
        )
        animals[2].save()
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"daysBeforeToday": days_before_today},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": desired_animal.breed,
                "dateAdded": date.strftime(desired_animal.date_added, "%Y-%m-%d"),
                "dateOfBirth": date.strftime(desired_animal.date_of_birth, "%Y-%m-%d"),
                "description": desired_animal.description,
                "dispositions": [],
                "gender": desired_animal.gender,
                "id": str(desired_animal.id),
                "kind": desired_animal.kind,
                "name": desired_animal.name,
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": desired_animal.status,
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_date_added_filters_do_not_match_any_animal(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]

        for animal in animals:
            animal.date_added = date.today() - timedelta(days=14)
            animal.save()

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"daysBeforeToday": 7},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == []

    def test_user_receives_error_when_user_passes_negative_value_for_days_before_today(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"daysBeforeToday": -1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]
        returned_animals = response_data["animals"]

        assert (
            response_errors[0]["message"]
            == "The value for 'daysBeforeToday' must be greater than or equal to 1."
        )
        assert returned_animals is None

    def test_user_receives_error_when_user_passes_invalid_value_for_days_before_today(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"daysBeforeToday": "invalid value"},
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert (
            response_errors[0]["message"]
            == "could not convert string to float: 'invalid value'"
        )


@pytest.mark.django_db
class TestGetAnimalsWithDispositionsFilter:
    def test_correct_animals_are_returned_when_filtering_for_dispositions(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        pet_seeker_access_token,
    ):
        animal_one = create_animal(**ANIMAL_ONE_DATA)
        animal_one_dispositions = []

        for disposition in ["Aggressive", "Moody"]:
            new_disposition = create_animal_disposition(disposition)
            animal_one_dispositions.append(new_disposition)
            animal_one.dispositions.add(new_disposition)

        animal_one_disposition_ids = [
            disposition.id for disposition in animal_one.dispositions.all()
        ]

        photograph = animal_one.photographs.create(
            url="http://www.images.com/image.jpeg"
        )
        status_update = animal_one.status_updates.create(status_text="status text")

        animal_two = create_animal(**ANIMAL_TWO_DATA)
        animal_two_dispositions = []

        for disposition in [
            "Affectionate",
            "Good with children",
            "Good with other animals",
        ]:
            new_disposition = create_animal_disposition(disposition)
            animal_two_dispositions.append(disposition)
            animal_two.dispositions.add(new_disposition)

        create_animal(**ANIMAL_THREE_DATA)

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"dispositions": animal_one_disposition_ids},
        )
        response_data = json.loads(response.content)["data"]
        animals = response_data["animals"]

        assert len(animals) == 1
        assert animals == [
            {
                "breed": ANIMAL_ONE_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_ONE_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_ONE_DATA["description"],
                "dispositions": [
                    {"id": str(disposition.id), "description": disposition.description}
                    for disposition in animal_one_dispositions
                ],
                "gender": ANIMAL_ONE_DATA["gender"],
                "id": str(animal_one.id),
                "kind": ANIMAL_ONE_DATA["kind"],
                "name": ANIMAL_ONE_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_ONE_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_dispositions_do_not_match(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        pet_seeker_access_token,
    ):
        animal_one = create_animal(**ANIMAL_ONE_DATA)
        animal_one_dispositions = []

        for disposition in ["Aggressive", "Moody"]:
            new_disposition = create_animal_disposition(disposition)
            animal_one_dispositions.append(new_disposition)
            animal_one.dispositions.add(new_disposition)

        animal_two = create_animal(**ANIMAL_TWO_DATA)
        animal_two_dispositions = []

        for disposition in [
            "Affectionate",
            "Good with children",
            "Good with other animals",
        ]:
            new_disposition = create_animal_disposition(disposition)
            animal_two_dispositions.append(disposition)
            animal_two.dispositions.add(new_disposition)

        create_animal(**ANIMAL_THREE_DATA)

        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"dispositions": [101928271]},
        )
        response_data = json.loads(response.content)["data"]
        animals = response_data["animals"]

        assert animals == []


@pytest.mark.django_db
class TestGetAnimalsWithGenderFilter:
    def test_correct_animal_is_returned_when_filtering_by_gender_that_exists(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        photograph = animals[1].photographs.create(
            url="http://www.imageserver.com/images/image.jpeg"
        )
        status_update = animals[1].status_updates.create(status_text="this is a status")
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"gender": "Female"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": ANIMAL_TWO_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_TWO_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_TWO_DATA["description"],
                "dispositions": [],
                "gender": ANIMAL_TWO_DATA["gender"],
                "id": str(animals[1].id),
                "kind": ANIMAL_TWO_DATA["kind"],
                "name": ANIMAL_TWO_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_TWO_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_filtering_by_gender_that_does_not_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_two_data = ANIMAL_TWO_DATA.copy()
        animal_two_data["gender"] = "Male"
        animals_data = [ANIMAL_ONE_DATA, animal_two_data, ANIMAL_THREE_DATA]
        _ = [create_animal(**animal) for animal in animals_data]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"gender": "Female"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == []

    def test_user_receives_error_when_passing_invalid_gender(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"gender": "Invalid"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]
        returned_animals = response_data["animals"]

        assert returned_animals is None
        assert (
            response_errors[0]["message"]
            == "The provided gender 'Invalid' is invalid. Accepted genders: 'Female', 'Male'"
        )


@pytest.mark.django_db
class TestGetAnimalsWithKindFilter:
    def test_correct_animal_is_returned_when_filtering_by_kind_that_exists(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        photograph = animals[1].photographs.create(
            url="http://www.imageserver.com/images/image.jpeg"
        )
        status_update = animals[1].status_updates.create(status_text="this is a status")
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"kind": "Cat"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": ANIMAL_TWO_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_TWO_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_TWO_DATA["description"],
                "dispositions": [],
                "gender": ANIMAL_TWO_DATA["gender"],
                "id": str(animals[1].id),
                "kind": ANIMAL_TWO_DATA["kind"],
                "name": ANIMAL_TWO_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_TWO_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_filtering_by_kind_that_does_not_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_two_data = ANIMAL_TWO_DATA.copy()
        animal_two_data["kind"] = "Dog"
        animals_data = [ANIMAL_ONE_DATA, animal_two_data, ANIMAL_THREE_DATA]
        _ = [create_animal(**animal) for animal in animals_data]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"kind": "Cat"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == []

    def test_user_receives_error_when_passing_invalid_kind(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"kind": "Invalid"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]
        returned_animals = response_data["animals"]

        assert returned_animals is None
        assert (
            response_errors[0]["message"]
            == "The provided kind 'Invalid' is invalid. Accepted kinds: 'Cat', 'Dog', 'Other'"
        )


@pytest.mark.django_db
class TestGetAnimalsWithStatusFilter:
    def test_correct_animal_is_returned_when_filtering_by_status_that_exists(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animals = [create_animal(**animal) for animal in ANIMALS_DATA]
        photograph = animals[1].photographs.create(
            url="http://www.imageserver.com/images/image.jpeg"
        )
        status_update = animals[1].status_updates.create(status_text="this is a status")
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"status": "In Foster"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == [
            {
                "breed": ANIMAL_TWO_DATA["breed"],
                "dateAdded": TODAY_FORMATTED,
                "dateOfBirth": date.strftime(
                    ANIMAL_TWO_DATA["date_of_birth"], "%Y-%m-%d"
                ),
                "description": ANIMAL_TWO_DATA["description"],
                "dispositions": [],
                "gender": ANIMAL_TWO_DATA["gender"],
                "id": str(animals[1].id),
                "kind": ANIMAL_TWO_DATA["kind"],
                "name": ANIMAL_TWO_DATA["name"],
                "photographs": [{"id": str(photograph.id), "url": photograph.url}],
                "status": ANIMAL_TWO_DATA["status"],
                "statusUpdates": [
                    {
                        "id": str(status_update.id),
                        "statusText": status_update.status_text,
                    }
                ],
            }
        ]

    def test_no_animals_are_returned_when_filtering_by_kind_that_does_not_exist(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"status": "Pending"},
        )
        response_data = json.loads(response.content)["data"]
        returned_animals = response_data["animals"]

        assert returned_animals == []

    def test_user_receives_error_when_passing_invalid_status(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        _ = [create_animal(**animal) for animal in ANIMALS_DATA]
        response = client_query(
            GET_ANIMALS_QUERY,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"status": "Invalid"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]
        returned_animals = response_data["animals"]

        assert returned_animals is None
        assert response_errors[0]["message"] == (
            "The provided status 'Invalid' is invalid. Accepted statuses: "
            "'Adopted', 'Available', 'In Foster', 'Pending'"
        )
