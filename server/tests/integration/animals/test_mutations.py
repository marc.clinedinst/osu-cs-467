import json
import os
import pytest

from datetime import date
from django.core import mail
from django.contrib.auth import get_user_model

from animals.models import Animal, AnimalPhotograph
from api_project.storage_backends import MediaStorage
from tempfile import NamedTemporaryFile
from tests.test_data.photographs import TEST_PHOTOGRAPHS_DIR

ADD_ANIMAL_STATUS_UPDATE_MUTATION = """
mutation AddAnimalStatusUpdate(
    $id: ID!,
    $statusText: String!
) {
    addAnimalStatusUpdate(
        id: $id,
        statusText: $statusText
    ) {
        statusUpdate {
            id,
            statusText
        }
    }
}
"""

ANIMAL_DATA = {
    "breed": "Terrier",
    "dateOfBirth": "2019-05-20",
    "description": "This is a description.",
    "dispositions": [],
    "gender": "Male",
    "kind": "Dog",
    "name": "Meatball",
    "status": "Adopted",
}

CREATE_ANIMAL_MUTATION = """
mutation CreateAnimal(
    $breed: String!,
    $dateOfBirth: Date!,
    $description: String!,
    $dispositions: [ID]!,
    $gender: String!,
    $kind: String!,
    $name: String!,
    $status: String!
) {
    createAnimal(
        breed: $breed,
        dateOfBirth: $dateOfBirth,
        description: $description,
        dispositions: $dispositions,
        gender: $gender,
        kind: $kind,
        name: $name,
        status: $status
    ) {
        animal {
            breed,
            dateOfBirth,
            description,
            dispositions {
                id,
                description
            },
            gender,
            id,
            kind,
            name,
            status
        }
    }
}
"""

DELETE_ANIMAL_MUTATION = """
mutation DeleteAnimal($id: ID!) {
    deleteAnimal(id: $id) {
        id
    }
}
"""

DELETE_ANIMAL_PHOTOGRAPH_MUTATION = """
mutation DeleteAnimalPhotograph(
    $id: ID!
) {
    deleteAnimalPhotograph(id: $id) {
        id
    }
}
"""

DELETE_ANIMAL_STATUS_UPDATE_MUTATION = """
mutation DeleteAnimalStatusUpdate($id: ID!) {
    deleteAnimalStatusUpdate(id: $id) {
        id
    }
}
"""

UPDATE_ANIMAL_MUTATION = """
mutation UpdateAnimal(
    $breed: String!,
    $dateOfBirth: Date!,
    $description: String!,
    $dispositions: [ID]!,
    $gender: String!,
    $id: ID!,
    $kind: String!,
    $name: String!,
    $status: String!
) {
    updateAnimal(
        breed: $breed,
        dateOfBirth: $dateOfBirth,
        description: $description,
        dispositions: $dispositions,
        gender: $gender,
        id: $id,
        kind: $kind,
        name: $name,
        status: $status
    ) {
        animal {
            breed,
            dateOfBirth,
            description,
            dispositions {
                id,
                description
            },
            gender,
            id,
            kind,
            name,
            status
        }
    }
}
"""

UPDATE_ANIMAL_STATUS_UPDATE_MUTATION = """
mutation UpdateAnimalStatusUpdate(
    $id: ID!,
    $statusText: String!
) {
    updateAnimalStatusUpdate(
        id: $id,
        statusText: $statusText
    ) {
        statusUpdate {
            id,
            statusText
        }
    }
}
"""

User = get_user_model()


@pytest.mark.django_db
class TestAddAnimalStatusUpdateMutation:
    def test_unauthenticated_user_receives_error_when_attempting_add_status_update_to_animal(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert list(animal.status_updates.all()) == []

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            variables={
                "id": animal.id,
                "statusText": "this is the current status of the animal",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["addAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert list(animal.status_updates.all()) == []

    def test_pet_seeker_user_receives_error_when_attempting_add_status_update_to_animal(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert list(animal.status_updates.all()) == []

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={
                "id": animal.id,
                "statusText": "this is the current status of the animal",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["addAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert list(animal.status_updates.all()) == []

    def test_site_admin_receives_error_when_animal_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert list(animal.status_updates.all()) == []

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={
                "id": animal.id + 1,
                "statusText": "this is the current status of the animal",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["addAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f"Animal with ID {animal.id + 1} does not exist."
        )
        assert list(animal.status_updates.all()) == []

    def test_site_admin_receives_error_when_neglecting_to_provide_status_text(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert list(animal.status_updates.all()) == []

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={
                "id": animal.id,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$statusText" of required type "String!" was not provided.'
        )
        assert list(animal.status_updates.all()) == []

    def test_site_admin_receives_error_when_leaving_status_text_blank(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert list(animal.status_updates.all()) == []

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": animal.id, "statusText": ""},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["addAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "The value for 'statusText' cannot be blank."
        )
        assert list(animal.status_updates.all()) == []

    def test_site_admin_can_create_status_update_when_providing_valid_data(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert animal.status_updates.count() == 0

        response = client_query(
            ADD_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": animal.id, "statusText": "this is a valid status"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        animal_status_update = animal.status_updates.first()

        assert animal.status_updates.count() == 1
        assert response_data["addAnimalStatusUpdate"]["statusUpdate"] == {
            "id": str(animal_status_update.id),
            "statusText": "this is a valid status",
        }


@pytest.mark.django_db
class TestCreateAnimalMutation:
    def test_unauthenticated_user_receives_error_when_attempting_to_create_animal(
        self, client_query
    ):
        response = client_query(CREATE_ANIMAL_MUTATION, variables=ANIMAL_DATA)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert len(response_errors) == 1
        assert response_data["createAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_pet_seeker_receives_error_when_attempting_to_create_animal(
        self, client_query, pet_seeker_access_token
    ):
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables=ANIMAL_DATA,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert len(response_errors) == 1
        assert response_data["createAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_site_admin_can_create_animal_with_no_dispositions_when_providing_valid_data(
        self, client_query, create_animal_disposition, site_admin_access_token
    ):
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        response_data = json.loads(response.content)["data"]
        animal = Animal.objects.first()

        assert len(mail.outbox) == 1
        assert response_data["createAnimal"]["animal"] == {
            "breed": ANIMAL_DATA["breed"],
            "dateOfBirth": ANIMAL_DATA["dateOfBirth"],
            "description": ANIMAL_DATA["description"],
            "dispositions": [],
            "gender": ANIMAL_DATA["gender"],
            "id": str(animal.id),
            "kind": ANIMAL_DATA["kind"],
            "name": ANIMAL_DATA["name"],
            "status": ANIMAL_DATA["status"],
        }

    def test_site_admin_can_create_animal_with_one_disposition_when_providing_valid_data(
        self, client_query, create_animal_disposition, site_admin_access_token
    ):
        disposition = create_animal_disposition("Moody")
        animal_data = ANIMAL_DATA.copy()
        animal_data["dispositions"] = [disposition.id]
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_data = json.loads(response.content)["data"]
        animal = Animal.objects.first()

        assert len(mail.outbox) == 1
        assert response_data["createAnimal"]["animal"] == {
            "breed": ANIMAL_DATA["breed"],
            "dateOfBirth": ANIMAL_DATA["dateOfBirth"],
            "description": ANIMAL_DATA["description"],
            "dispositions": [
                {"description": disposition.description, "id": str(disposition.id)}
            ],
            "gender": ANIMAL_DATA["gender"],
            "id": str(animal.id),
            "kind": ANIMAL_DATA["kind"],
            "name": ANIMAL_DATA["name"],
            "status": ANIMAL_DATA["status"],
        }

    def test_site_admin_can_create_animal_with_multiple_disposition_when_providing_valid_data(
        self, client_query, create_animal_disposition, site_admin_access_token
    ):
        disposition_one = create_animal_disposition("Aggressive")
        disposition_two = create_animal_disposition("Bad with children")
        disposition_three = create_animal_disposition("Bad with other pets")
        animal_data = ANIMAL_DATA.copy()
        animal_data["dispositions"] = [
            disposition_one.id,
            disposition_two.id,
            disposition_three.id,
        ]
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_data = json.loads(response.content)["data"]
        animal = Animal.objects.first()

        assert len(mail.outbox) == 1
        assert response_data["createAnimal"]["animal"] == {
            "breed": ANIMAL_DATA["breed"],
            "dateOfBirth": ANIMAL_DATA["dateOfBirth"],
            "description": ANIMAL_DATA["description"],
            "dispositions": [
                {
                    "description": disposition_one.description,
                    "id": str(disposition_one.id),
                },
                {
                    "description": disposition_two.description,
                    "id": str(disposition_two.id),
                },
                {
                    "description": disposition_three.description,
                    "id": str(disposition_three.id),
                },
            ],
            "gender": ANIMAL_DATA["gender"],
            "id": str(animal.id),
            "kind": ANIMAL_DATA["kind"],
            "name": ANIMAL_DATA["name"],
            "status": ANIMAL_DATA["status"],
        }

    def test_site_admin_receives_error_message_when_trying_to_create_animal_with_nonexistent_disposition(
        self, client_query, create_animal_disposition, site_admin_access_token
    ):
        fake_disposition_id = 191928281822
        animal_data = ANIMAL_DATA.copy()
        animal_data["dispositions"] = [fake_disposition_id]
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert response_data["createAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"A disposition with id {fake_disposition_id} does not exist."
        )

    def test_site_admin_receives_error_message_when_trying_to_create_animal_with_invalid_disposition_id(
        self, client_query, create_animal_disposition, site_admin_access_token
    ):
        invalid_disposition_id = "abc"
        animal_data = ANIMAL_DATA.copy()
        animal_data["dispositions"] = [invalid_disposition_id]
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert response_data["createAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"Received a non-integer value for disposition: '{invalid_disposition_id}'"
        )

    @pytest.mark.parametrize(
        "field",
        ["breed", "description", "gender", "kind", "name", "status"],
    )
    def test_site_admin_receives_error_message_when_leaving_field_blank(
        self, client_query, field, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data[field] = ""
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert response_data["createAnimal"] is None
        assert response_errors[0]["message"] == f"{field} cannot be blank."

    def test_site_admin_receives_error_message_when_leaving_date_of_birth_blank(
        self, client_query, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["dateOfBirth"] = ""
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert response_errors[0]["message"] == "string index out of range"

    @pytest.mark.parametrize(
        "field, field_type",
        [
            ("breed", "String!"),
            ("dateOfBirth", "Date!"),
            ("description", "String!"),
            ("gender", "String!"),
            ("kind", "String!"),
            ("name", "String!"),
            ("status", "String!"),
        ],
    )
    def test_site_admin_receives_error_message_when_omitting_required_field(
        self, client_query, field, field_type, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        del animal_data[field]
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f'Variable "${field}" of required type "{field_type}" was not provided.'
        )

    @pytest.mark.parametrize(
        "field, expected_error",
        [
            (
                "gender",
                "The provided gender 'Invalid' is invalid. Accepted genders: 'Female', 'Male'",
            ),
            (
                "kind",
                "The provided kind 'Invalid' is invalid. Accepted kinds: 'Cat', 'Dog', 'Other'",
            ),
            (
                "status",
                "The provided status 'Invalid' is invalid. Accepted statuses: "
                "'Adopted', 'Available', 'In Foster', 'Pending'",
            ),
        ],
    )
    def test_site_admin_receives_error_message_when_passing_invalid_value_for_field(
        self, client_query, expected_error, field, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data[field] = "Invalid"
        response = client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(mail.outbox) == 0
        assert Animal.objects.count() == 0
        assert len(response_errors) == 1
        assert response_errors[0]["message"] == expected_error

    def test_email_metadata_is_correct_when_new_animal_is_created(
        self, client_query, site_admin_access_token
    ):
        client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        email = mail.outbox[0]

        assert len(mail.outbox) == 1
        assert "site_admin_username" in email.body
        assert ANIMAL_DATA["breed"] in email.body
        assert (
            date.fromisoformat(ANIMAL_DATA["dateOfBirth"]).strftime("%b %d, %Y")
            in email.body
        )
        assert ANIMAL_DATA["description"] in email.body
        assert ", ".join(ANIMAL_DATA["dispositions"]) in email.body
        assert ANIMAL_DATA["gender"] in email.body
        assert ANIMAL_DATA["kind"] in email.body
        assert ANIMAL_DATA["name"] in email.body
        assert ANIMAL_DATA["status"] in email.body
        assert email.from_email == "clinedim@oregonstate.edu"
        assert email.subject == f"New {ANIMAL_DATA['kind']} - {ANIMAL_DATA['name']}"
        assert email.to == ["site_admin@email.com"]

    def test_email_includes_correct_url_when_in_debug_mode(
        self, client_query, settings, site_admin_access_token
    ):
        settings.DEBUG = True
        client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        animal = Animal.objects.first()
        email = mail.outbox[0]

        assert len(mail.outbox) == 1
        assert f"http://localhost:3001/animal/{animal.id}" in email.body

    def test_email_includes_correct_url_when_not_in_debug_mode(
        self, client_query, settings, site_admin_access_token
    ):
        settings.DEBUG = False
        client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        animal = Animal.objects.first()
        email = mail.outbox[0]

        assert len(mail.outbox) == 1
        assert f"https://www.team-pawswipe.com/animal/{animal.id}" in email.body

    def test_correct_number_of_emails_sent_when_multiple_users_opted_in(
        self, client_query, create_user, site_admin_access_token
    ):
        create_user("user_two", "user_two_password", email="an_email@email.com")
        create_user(
            "user_three", "user_three_password", email="another_email@email.com"
        )
        client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        user_emails = [[user.email] for user in User.objects.all()]
        recipients = [email.to for email in mail.outbox]

        assert len(mail.outbox) == 3
        assert recipients == user_emails

    def test_only_opted_in_users_receive_emails(
        self, client_query, create_user, site_admin_access_token
    ):
        create_user("user_two", "user_two_password", email="an_email@email.com")
        opt_out_user = create_user(
            "user_three", "user_three_password", email="another_email@email.com"
        )
        opt_out_user.email_opt_in = False
        opt_out_user.save()
        client_query(
            CREATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=ANIMAL_DATA,
        )
        user_emails = [
            [user.email] for user in User.objects.all() if user.email_opt_in is True
        ]
        recipients = [email.to for email in mail.outbox]

        assert len(mail.outbox) == 2
        assert recipients == user_emails


@pytest.mark.django_db
class TestDeleteAnimalMutation:
    def test_unauthenticated_user_receives_error_when_attempting_to_delete_animal(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert Animal.objects.count() == 1

        response = client_query(DELETE_ANIMAL_MUTATION, variables={"id": animal.id})
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_pet_seeker_receives_error_when_attempting_to_delete_animal(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert Animal.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_site_admin_can_delete_pet(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert Animal.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": animal.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert Animal.objects.count() == 0
        assert response_data["deleteAnimal"] == {"id": str(animal.id)}

    def test_site_admin_receives_error_when_attempting_to_delete_animal_that_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert Animal.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": animal.id + 1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"Animal with ID {animal.id + 1} does not exist."
        )

    def test_site_admin_receives_error_when_attempting_to_delete_animal_with_invalid_id(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        create_animal(**animal_data)

        assert Animal.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": "invalid_id"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "Field 'id' expected a number but got 'invalid_id'."
        )


@pytest.mark.django_db
class TestDeleteAnimalStatusUpdateMutation:
    def test_unauthenticated_user_receives_error_when_attempting_to_delete_animal_status_update(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            DELETE_ANIMAL_STATUS_UPDATE_MUTATION, variables={"id": status_update.id}
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert list(animal.status_updates.all()) == [status_update]
        assert len(response_errors) == 1
        assert response_data["deleteAnimalStatusUpdate"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_pet_seeker_receives_error_when_attempting_to_delete_animal_status_update(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            DELETE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": animal.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalStatusUpdate"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_site_admin_receives_error_when_attempting_to_delete_animal_status_update_that_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            DELETE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": animal.id + 1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert Animal.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalStatusUpdate"] is None
        assert (
            response_errors[0]["message"]
            == f"Animal status update with ID {animal.id + 1} does not exist."
        )

    def test_site_admin_receives_error_when_attempting_to_delete_animal_status_update_with_invalid_id(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            DELETE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": "invalid_id"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert list(animal.status_updates.all()) == [status_update]
        assert len(response_errors) == 1
        assert response_data["deleteAnimalStatusUpdate"] is None
        assert (
            response_errors[0]["message"]
            == "Field 'id' expected a number but got 'invalid_id'."
        )

    def test_site_admin_can_delete_animal_status_update_with_valid_id(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            DELETE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": status_update.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert list(animal.status_updates.all()) == []
        assert response_data["deleteAnimalStatusUpdate"] == {
            "id": str(status_update.id)
        }


@pytest.mark.django_db
class TestDeleteAnimalPhotographMutation:
    def test_unauthenticated_user_receives_error_when_attempting_to_delete_animal_photograph(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        photograph = animal.photographs.create(url="https://www.images.com/image.jpg")

        assert AnimalPhotograph.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_PHOTOGRAPH_MUTATION, variables={"id": photograph.id}
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert AnimalPhotograph.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalPhotograph"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_pet_seeker_receives_error_when_attempting_to_delete_animal_photograph(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        photograph = animal.photographs.create(url="https://www.images.com/image.jpg")

        assert AnimalPhotograph.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_PHOTOGRAPH_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={"id": photograph.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert AnimalPhotograph.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalPhotograph"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )

    def test_site_admin_receives_error_when_attempting_to_delete_animal_photograph_that_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        photograph = animal.photographs.create(url="https://www.images.com/image.jpg")

        assert AnimalPhotograph.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_PHOTOGRAPH_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": photograph.id + 1},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert AnimalPhotograph.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalPhotograph"] is None
        assert (
            response_errors[0]["message"]
            == f"Animal photograph with ID {photograph.id + 1} does not exist."
        )

    def test_site_admin_receives_error_when_attempting_to_delete_animal_photograph_with_invalid_id(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal.photographs.create(url="https://www.images.com/image.jpg")

        assert AnimalPhotograph.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_PHOTOGRAPH_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": "invalid_id"},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert AnimalPhotograph.objects.count() == 1
        assert len(response_errors) == 1
        assert response_data["deleteAnimalPhotograph"] is None
        assert (
            response_errors[0]["message"]
            == "Field 'id' expected a number but got 'invalid_id'."
        )

    def test_site_admin_can_delete_animal_photograph(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        photograph = animal.photographs.create(url="https://www.images.com/image.jpg")

        assert AnimalPhotograph.objects.count() == 1

        response = client_query(
            DELETE_ANIMAL_PHOTOGRAPH_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": photograph.id},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]

        assert AnimalPhotograph.objects.count() == 0
        assert response_data["deleteAnimalPhotograph"] == {"id": str(photograph.id)}


@pytest.mark.django_db
class TestUpdateAnimalMutation:
    def test_unauthenticated_user_receives_error_when_attempting_to_update_animal(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert animal.name == ANIMAL_DATA["name"]

        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data.update({"id": animal.id, "name": "Updated Name"})
        response = client_query(UPDATE_ANIMAL_MUTATION, variables=animal_data)
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert animal.name == ANIMAL_DATA["name"]

    def test_pet_seeker_receives_error_when_attempting_to_update_animal(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        assert animal.name == ANIMAL_DATA["name"]

        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data.update({"id": animal.id, "name": "Updated Name"})
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert animal.name == ANIMAL_DATA["name"]

    def test_site_admin_can_update_animal_data(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        disposition_one = create_animal_disposition("Moody")
        disposition_two = create_animal_disposition("Sleepy")
        animal_updates = {
            "breed": "Terrier",
            "dateOfBirth": "2019-05-20",
            "description": "This is a description.",
            "dispositions": [disposition_one.id, disposition_two.id],
            "gender": "Male",
            "id": animal.id,
            "kind": "Dog",
            "name": "Meatball",
            "status": "Adopted",
        }
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_updates,
        )
        response_data = json.loads(response.content)["data"]

        assert response_data["updateAnimal"]["animal"] == {
            "breed": animal_updates["breed"],
            "dateOfBirth": animal_updates["dateOfBirth"],
            "description": animal_updates["description"],
            "dispositions": [
                {
                    "description": disposition_one.description,
                    "id": str(disposition_one.id),
                },
                {
                    "description": disposition_two.description,
                    "id": str(disposition_two.id),
                },
            ],
            "gender": animal_updates["gender"],
            "id": str(animal.id),
            "kind": animal_updates["kind"],
            "name": animal_updates["name"],
            "status": animal_updates["status"],
        }

    def test_site_admin_can_remove_dispositions_during_update(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        disposition_one = create_animal_disposition("Moody")
        disposition_two = create_animal_disposition("Sleepy")
        animal.dispositions.add(disposition_one)
        animal.dispositions.add(disposition_two)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data["id"] = animal.id
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_data = json.loads(response.content)["data"]

        assert response_data["updateAnimal"]["animal"] == {
            "breed": animal_data["breed"],
            "dateOfBirth": animal_data["dateOfBirth"],
            "description": animal_data["description"],
            "dispositions": [],
            "gender": animal_data["gender"],
            "id": str(animal.id),
            "kind": animal_data["kind"],
            "name": animal_data["name"],
            "status": animal_data["status"],
        }

    def test_site_admin_receives_error_when_animal_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data["date_of_birth"]
        animal_data["id"] = animal.id + 1
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"Animal with ID {animal.id + 1} does not exist."
        )

    def test_site_admin_receives_error_when_attempting_to_update_animal_with_nonexistent_disposition(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        fake_disposition_id = 1911881
        animal_data.update({"dispositions": [fake_disposition_id], "id": animal.id})
        animal_data["dispositions"] = [fake_disposition_id]
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"A disposition with id {fake_disposition_id} does not exist."
        )
        assert list(animal.dispositions.all()) == []

    def test_site_admin_receives_error_when_attempting_to_update_animal_with_invalid_disposition(
        self,
        client_query,
        create_animal,
        create_animal_disposition,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        invalid_disposition_id = "abc"
        animal_data.update({"dispositions": [invalid_disposition_id], "id": animal.id})
        animal_data["dispositions"] = [invalid_disposition_id]
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert (
            response_errors[0]["message"]
            == f"Received a non-integer value for disposition: '{invalid_disposition_id}'"
        )
        assert list(animal.dispositions.all()) == []

    @pytest.mark.parametrize(
        "field",
        ["breed", "description", "gender", "kind", "name", "status"],
    )
    def test_site_admin_receives_error_message_when_leaving_field_blank(
        self, client_query, create_animal, field, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data.update({"id": animal.id, field: ""})
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_data["updateAnimal"] is None
        assert response_errors[0]["message"] == f"{field} cannot be blank."

    def test_site_admin_receives_error_message_when_leaving_date_of_birth_blank(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data.pop("date_of_birth")
        animal_data.update({"dateOfBirth": "", "id": animal.id})
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_errors[0]["message"] == "string index out of range"

    @pytest.mark.parametrize(
        "field, field_type",
        [
            ("breed", "String!"),
            ("dateOfBirth", "Date!"),
            ("description", "String!"),
            ("gender", "String!"),
            ("kind", "String!"),
            ("name", "String!"),
            ("status", "String!"),
        ],
    )
    def test_site_admin_receives_error_message_when_omitting_required_field(
        self, client_query, create_animal, field, field_type, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data["id"] = animal.id
        del animal_data[field]
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f'Variable "${field}" of required type "{field_type}" was not provided.'
        )

    @pytest.mark.parametrize(
        "field, expected_error",
        [
            (
                "gender",
                "The provided gender 'Invalid' is invalid. Accepted genders: 'Female', 'Male'",
            ),
            (
                "kind",
                "The provided kind 'Invalid' is invalid. Accepted kinds: 'Cat', 'Dog', 'Other'",
            ),
            (
                "status",
                "The provided status 'Invalid' is invalid. Accepted statuses: "
                "'Adopted', 'Available', 'In Foster', 'Pending'",
            ),
        ],
    )
    def test_site_admin_receives_error_message_when_passing_invalid_value_for_field(
        self,
        client_query,
        create_animal,
        expected_error,
        field,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        animal_data["dateOfBirth"] = animal_data.pop("date_of_birth")
        animal_data.update({"id": animal.id, field: "Invalid"})
        response = client_query(
            UPDATE_ANIMAL_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables=animal_data,
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert response_errors[0]["message"] == expected_error


@pytest.mark.django_db
class TestUpdateAnimalStatusUpdateMutation:
    def test_unauthenticated_user_receives_error_when_attempting_update_status_update_for_animal(
        self, client_query, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            variables={
                "id": status_update.id,
                "statusText": "this is an updated statusl",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["updateAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert list(animal.status_updates.all()) == [status_update]

    def test_pet_seeker_user_receives_error_when_attempting_update_status_update_for_animal(
        self, client_query, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {pet_seeker_access_token}"},
            variables={
                "id": status_update.id,
                "statusText": "this is an updated status",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["updateAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "You do not have permission to perform this action"
        )
        assert list(animal.status_updates.all()) == [status_update]

    def test_site_admin_receives_error_when_animal_status_update_does_not_exist(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={
                "id": status_update.id + 1,
                "statusText": "this is the updated status of the animal",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["updateAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == f"Animal status update with ID {status_update.id + 1} does not exist."
        )
        assert list(animal.status_updates.all()) == [status_update]

    def test_site_admin_receives_error_when_neglecting_to_provide_status_text(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={
                "id": status_update.id,
            },
        )
        response_content = json.loads(response.content)
        response_errors = response_content["errors"]

        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == 'Variable "$statusText" of required type "String!" was not provided.'
        )
        assert list(animal.status_updates.all()) == [status_update]

    def test_site_admin_receives_error_when_leaving_status_text_blank(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={"id": status_update.id, "statusText": ""},
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        response_errors = response_content["errors"]

        assert response_data["updateAnimalStatusUpdate"] is None
        assert len(response_errors) == 1
        assert (
            response_errors[0]["message"]
            == "The value for 'statusText' cannot be blank."
        )
        assert list(animal.status_updates.all()) == [status_update]

    def test_site_admin_can_update_status_update_when_providing_valid_data(
        self, client_query, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)
        status_update = animal.status_updates.create(status_text="this is the status")

        assert list(animal.status_updates.all()) == [status_update]

        response = client_query(
            UPDATE_ANIMAL_STATUS_UPDATE_MUTATION,
            headers={"HTTP_AUTHORIZATION": f"JWT {site_admin_access_token}"},
            variables={
                "id": status_update.id,
                "statusText": "this is the updated status",
            },
        )
        response_content = json.loads(response.content)
        response_data = response_content["data"]
        animal_status_update = animal.status_updates.first()

        assert animal.status_updates.count() == 1
        assert response_data["updateAnimalStatusUpdate"]["statusUpdate"] == {
            "id": str(animal_status_update.id),
            "statusText": "this is the updated status",
        }


@pytest.mark.django_db
class TestUploadAnimalPhotograph:
    def test_unauthenticated_user_receives_error_when_attempting_to_upload_photograph(
        self, client, create_animal
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        with NamedTemporaryFile(prefix="image", suffix=".jpeg") as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
            )
            response_content = json.loads(response.content)
            response_errors = response_content["errors"]

            assert AnimalPhotograph.objects.count() == 0
            assert (
                response_errors[0]["message"]
                == "You do not have permission to perform this action"
            )

    def test_pet_seeker_receives_error_when_attempting_to_upload_photograph(
        self, client, create_animal, pet_seeker_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        with NamedTemporaryFile(prefix="image", suffix=".jpeg") as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
                HTTP_AUTHORIZATION=f"JWT {pet_seeker_access_token}",
            )
            response_content = json.loads(response.content)
            response_errors = response_content["errors"]

            assert AnimalPhotograph.objects.count() == 0
            assert (
                response_errors[0]["message"]
                == "You do not have permission to perform this action"
            )

    def test_site_admin_receives_error_when_animal_does_not_exist(
        self, client, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        with NamedTemporaryFile(prefix="image", suffix=".jpeg") as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id + 1}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
                HTTP_AUTHORIZATION=f"JWT {site_admin_access_token}",
            )
            response_content = json.loads(response.content)
            response_errors = response_content["errors"]

            assert AnimalPhotograph.objects.count() == 0
            assert (
                response_errors[0]["message"]
                == f"Animal with ID {animal.id + 1} does not exist."
            )

    @pytest.mark.parametrize(
        "file_extension, content_type",
        [
            [".ai", "application/postscript"],
            [".doc", "application/msword"],
            [".eps", "application/postscript"],
            [".json", "application/json"],
            [".mp3", "audio/mpeg"],
            [".mp4", "video/mp4"],
            [".pdf", "application/pdf"],
            [".tiff", "image/tiff"],
            [".txt", "text/plain"],
            [".xls", "application/vnd.ms-excel"],
        ],
    )
    def test_site_admin_receives_error_when_attempting_to_upload_invalid_file_type(
        self,
        client,
        content_type,
        create_animal,
        file_extension,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        with NamedTemporaryFile(prefix="image", suffix=file_extension) as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
                HTTP_AUTHORIZATION=f"JWT {site_admin_access_token}",
            )
            response_content = json.loads(response.content)
            response_errors = response_content["errors"]

            assert AnimalPhotograph.objects.count() == 0
            assert (
                response_errors[0]["message"]
                == f"Invalid content type: {content_type}. Accepted content types: image/gif image/jpeg image/png"
            )

    def test_site_admin_receives_error_when_attempting_to_upload_file_larger_than_5_mb(
        self, client, create_animal, site_admin_access_token
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        with open(
            os.path.join(TEST_PHOTOGRAPHS_DIR, "large_image.jpg"), "rb"
        ) as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
                HTTP_AUTHORIZATION=f"JWT {site_admin_access_token}",
            )
            response_content = json.loads(response.content)
            response_errors = response_content["errors"]

            assert AnimalPhotograph.objects.count() == 0
            assert (
                response_errors[0]["message"]
                == "Your image exceeds the file size limit of 5 MB."
            )

    @pytest.mark.parametrize("file_extension", [".gif", "jpeg", "jpg", ".png"])
    def test_site_admin_can_upload_image_with_accepted_file_extension(
        self,
        client,
        create_animal,
        file_extension,
        monkeypatch,
        site_admin_access_token,
    ):
        animal_data = ANIMAL_DATA.copy()
        animal_data["date_of_birth"] = animal_data.pop("dateOfBirth")
        animal = create_animal(**animal_data)

        def mock_save(*_):
            return f"photographs/image.{file_extension}"

        def mock_url(*_):
            return f"https://pawswipe.s3.amazonaws.com/media/photographs/image.{file_extension}"

        monkeypatch.setattr(MediaStorage, "save", mock_save)
        monkeypatch.setattr(MediaStorage, "url", mock_url)

        with NamedTemporaryFile(prefix="image", suffix=".jpeg") as photograph:
            response = client.post(
                "/graphql/",
                {
                    "operations": (
                        '{"query": "mutation UploadAnimalPhotograph($file: Upload!, $id: ID!) '
                        '{ uploadAnimalPhotograph(file: $file, id: $id) { url }}", '
                        f'"variables": {{"file": null, "id": {animal.id}}}}}'
                    ),
                    "map": '{"0": ["variables.file"]}',
                    "0": photograph,
                },
                HTTP_AUTHORIZATION=f"JWT {site_admin_access_token}",
            )
            response_content = json.loads(response.content)
            response_data = response_content["data"]

            assert AnimalPhotograph.objects.count() == 1
            assert response_data["uploadAnimalPhotograph"] == {
                "url": f"https://pawswipe.s3.amazonaws.com/media/photographs/image.{file_extension}"
            }
