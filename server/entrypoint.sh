#!/bin/sh

# This section waits for the PostgreSQL database to be up and running before attempting to execute
# Django management commands against the database.
echo "Waiting for PostgreSQL database to be up and running."

while ! nc -z "$DATABASE_HOST" "$DATABASE_PORT"; do
  echo "Pausing for 0.1 seconds before pinging PostgreSQL again."
  sleep 0.1
done

echo "PostgreSQL is up and running."

# These commands use Django's administrative utility to remove all data from the database and then
# apply all database migrations. This ensures a clean database each time a developer rebuilds the
# server. Documentation for these and other Django administrative utility commands can be found
# here: https://docs.djangoproject.com/en/3.1/ref/django-admin/
python manage.py flush --no-input
python manage.py migrate
python manage.py loaddata users
python manage.py loaddata animal_dispositions
python manage.py loaddata animals
python manage.py loaddata animal_photographs
python manage.py loaddata animal_status_updates

exec "$@"
