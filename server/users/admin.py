from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    filter_horizontal = ["favorite_animals"]


admin.site.register(CustomUser, CustomUserAdmin)
