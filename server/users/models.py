from django.contrib.auth.models import AbstractUser
from django.db import models

from animals.models import Animal


# The official Django documentation recommends creating a custom user model when starting a new
# Django project:
# https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
# This allows for easier changes to the user model if the default user model is not sufficient for
# the requirements of a particular project.
class CustomUser(AbstractUser):
    email_opt_in = models.BooleanField(default=True)
    favorite_animals = models.ManyToManyField(Animal, related_name="favorite_animals")
