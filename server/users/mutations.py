import graphene
import graphql_jwt

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from graphql import GraphQLError
from graphql_jwt.decorators import login_required
from graphql_jwt.shortcuts import get_token

from animals.models import Animal
from .types import UserType

User = get_user_model()


class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)


class RegisterUser(graphene.Mutation):
    class Arguments:
        confirm_password = graphene.String(required=True)
        email = graphene.String(required=True)
        password = graphene.String(required=True)
        username = graphene.String(required=True)

    user = graphene.Field(UserType)
    token = graphene.String()

    @classmethod
    def mutate(cls, root, info, confirm_password, email, password, username):
        if confirm_password != password:
            raise GraphQLError("Passwords must match.")

        if User.objects.filter(username=username):
            raise GraphQLError(f"A user with the username '{username}' already exists.")

        try:
            # https://docs.djangoproject.com/en/3.1/ref/validators/#validate-email
            validate_email(email)
        except ValidationError:
            raise GraphQLError(f"The provided email '{email}' is invalid.")

        user = User.objects.create_user(
            email=email, username=username, password=password
        )
        token = get_token(user)

        return RegisterUser(user=user, token=token)


class ToggleEmailOptIn(graphene.Mutation):
    opted_in = graphene.Boolean()

    @classmethod
    @login_required
    def mutate(cls, root, info):
        current_user = info.context.user
        current_user.email_opt_in = not current_user.email_opt_in
        current_user.save()

        return ToggleEmailOptIn(opted_in=current_user.email_opt_in)


class ToggleFavoriteAnimal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    id = graphene.ID()

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        current_user = info.context.user

        if animal in current_user.favorite_animals.all():
            current_user.favorite_animals.remove(animal)
        else:
            current_user.favorite_animals.add(animal)

        return ToggleFavoriteAnimal(id=id)


class ToggleEmailOptInMutation(graphene.ObjectType):
    toggle_email_opt_in = ToggleEmailOptIn.Field()


class ToggleFavoriteAnimalMutation(graphene.ObjectType):
    toggle_favorite_animal = ToggleFavoriteAnimal.Field()


class UserRegistrationMutation(graphene.ObjectType):
    register_user = RegisterUser.Field()
