import graphene

from django.contrib.auth import get_user_model
from graphql import GraphQLError
from graphql_jwt.decorators import login_required
from graphql_jwt.utils import jwt_decode

from animals.types import AnimalType
from .types import UserType

User = get_user_model()


class CurrentUserFavoriteAnimals(graphene.ObjectType):
    favorite_animals = graphene.List(AnimalType)

    @login_required
    def resolve_favorite_animals(self, info, **kwargs):
        return info.context.user.favorite_animals.all()


class UserQuery(graphene.ObjectType):
    user_by_token = graphene.Field(UserType, token=graphene.String(required=True))

    @login_required
    def resolve_user_by_token(parent, info, token, **kwargs):
        decoded_token = jwt_decode(token)
        username = decoded_token["username"]
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise GraphQLError("User does not exist.")

        return user
