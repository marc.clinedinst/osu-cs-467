"""
Django settings file documentation: https://docs.djangoproject.com/en/3.1/topics/settings/

Available settings: https://docs.djangoproject.com/en/3.1/ref/settings/

Deployment documentation: https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/
"""
import datetime
import dj_database_url
import os

from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# SECURITY WARNING: keep the secret key used in production secret!
# Load the secret key from an environment variable named SECRET_KEY so that this can be kept out of
# version control. In development, this is set within the docker-compose.yml file. During deployment,
# set this using the CI/CD platform.
SECRET_KEY = os.environ.get("SECRET_KEY")


# SECURITY WARNING: don't run with debug turned on in production!
# Load the secret key from an environment variable, so that this can be set dynamically at run time.
# In development, this is set within the docker-compose.yml file. During deployment, set this using
# the CI/CD platform.
DEBUG = int(os.environ.get("DEBUG", default=0))


# SECURITY WARNING: don't set this to *
# Load the allowed hosts for the Django application from an environment variable; multiple hosts
# can be passed by separating each by spaces. In development, this is set within the
# docker-compose.yml file. During deployment, set this using the CI/CD platform.
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS").split(" ")

# Application definition

INSTALLED_APPS = [
    # Internal Django apps
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.postgres",
    "django.contrib.staticfiles",
    # Third-party apps
    "corsheaders",
    "graphene_django",
    "rest_framework",
    "storages",
    # Custom apps
    "animals",
    "users",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "api_project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "api_project.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
# Load configuration for database from environment variables. In development, these are set in the
# docker-compose.yml file. During deployment, set these with the CI/CD platform.
# Note: If the DATABASE_* environment variables are not set, the application will default to using
# sqlite as its database.

DATABASES = {
    "default": {
        "ENGINE": os.environ.get("DATABASE_ENGINE", "django.db.backends.sqlite3"),
        "HOST": os.environ.get("DATABASE_HOST", "localhost"),
        "NAME": os.environ.get("DATABASE_NAME", os.path.join(BASE_DIR, "db.sqlite3")),
        "PASSWORD": os.environ.get("DATABASE_PASSWORD", "password"),
        "PORT": os.environ.get("DATABASE_PORT", "5432"),
        "USER": os.environ.get("DATABASE_USER", "user"),
    }
}

DATABASE_URL = os.environ.get("DATABASE_URL")
database_from_environment = dj_database_url.config(
    default=DATABASE_URL, conn_max_age=500
)
DATABASES["default"].update(database_from_environment)


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Authentication Backends

AUTHENTICATION_BACKENDS = [
    "graphql_jwt.backends.JSONWebTokenBackend",
    "django.contrib.auth.backends.ModelBackend",
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID")

AWS_LOCATION = "static"

AWS_QUERYSTRING_AUTH = False

AWS_S3_CUSTOM_DOMAIN = os.environ.get("AWS_S3_CUSTOM_DOMAIN")

AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age=86400"}

AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY")

AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME")

DEFAULT_FILE_STORAGE = "api_project.storage_backends.MediaStorage"

STATIC_ROOT = os.path.join(BASE_DIR, "static_files")

STATIC_URL = "/static/"

STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"


# The official Django documentation recommends creating a custom user model when starting a new
# Django project:
# https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
# This allows for easier changes to the user model if the default user model is not sufficient for
# the requirements of a particular project.

AUTH_USER_MODEL = "users.CustomUser"


# Django REST Framework

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "rest_framework.authentication.SessionAuthentication",
    )
}

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": datetime.timedelta(minutes=60),
    "REFRESH_TOKEN_LIFETIME": datetime.timedelta(minutes=(60 * 12)),
    "USER_ID_CLAIM": "id",
}


# GraphQL

GRAPHENE = {
    "MIDDLEWARE": [
        "graphql_jwt.middleware.JSONWebTokenMiddleware",
    ],
    "SCHEMA": "api_project.schema.schema",
}

# Add CORS_ORIGIN_WHITELIST to allow these domains be authorized to make cross-site HTTP requests
CORS_ALLOWED_ORIGINS = [
    "http://localhost:3001",
    "http://localhost:3000",
    "http://127.0.0.1:3001",
    "http://127.0.0.1:3000",
    "http://frozen-beach-52872.herokuapp.com",
    "https://frozen-beach-52872.herokuapp.com",
    "http://team-pawswipe.com",
    "http://www.team-pawswipe.com",
    "https://www.team-pawswipe.com",
    "https://team-pawswipe.com",
]

# Email Settings
SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY")

EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = "apikey"  # this is exactly the value 'apikey'
EMAIL_HOST_PASSWORD = SENDGRID_API_KEY
EMAIL_PORT = 587
EMAIL_USE_TLS = True


if not DEBUG:
    # Security Settings for Deployment
    CSRF_COOKIE_SECURE = True
    SECURE_BROWSER_XSS_FILTER = True
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True
    SECURE_HSTS_SECONDS = 3600
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    SECURE_REFERRER_POLICY = "same-origin"
    SECURE_SSL_REDIRECT = True
    SESSION_COOKIE_SECURE = True
    X_FRAME_OPTIONS = "DENY"

    # Disable Django Rest Framework's Browsable API for Deployment
    REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] = [
        "rest_framework.renderers.JSONRenderer"
    ]
