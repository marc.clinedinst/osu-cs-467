import graphene
import graphql_jwt

from graphql_jwt.decorators import login_required

from animals.mutations import AnimalCRUDMutation
from animals.queries import AnimalQuery
from users.mutations import (
    ObtainJSONWebToken,
    ToggleEmailOptInMutation,
    ToggleFavoriteAnimalMutation,
    UserRegistrationMutation,
)
from users.queries import CurrentUserFavoriteAnimals, UserQuery


class Mutation(
    AnimalCRUDMutation,
    UserRegistrationMutation,
    ToggleEmailOptInMutation,
    ToggleFavoriteAnimalMutation,
    graphene.ObjectType,
):
    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Query(AnimalQuery, CurrentUserFavoriteAnimals, UserQuery, graphene.ObjectType):
    protected = graphene.String()

    @login_required
    def resolve_protected(self, info):
        return "This is a protected query."


schema = graphene.Schema(mutation=Mutation, query=Query)
