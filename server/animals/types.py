from graphene_django import DjangoObjectType

from .models import Animal, AnimalDisposition, AnimalPhotograph, AnimalStatusUpdate


class AnimalDispositionType(DjangoObjectType):
    class Meta:
        fields = "__all__"
        model = AnimalDisposition


class AnimalPhotographType(DjangoObjectType):
    class Meta:
        fields = "__all__"
        model = AnimalPhotograph


class AnimalStatusUpdateType(DjangoObjectType):
    class Meta:
        fields = "__all__"
        model = AnimalStatusUpdate


class AnimalType(DjangoObjectType):
    class Meta:
        convert_choices_to_enum = False
        fields = "__all__"
        model = Animal
