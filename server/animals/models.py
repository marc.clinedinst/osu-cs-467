from django.db import models

from .enums import Gender, Kind, Status


class AnimalDisposition(models.Model):
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.description


class Animal(models.Model):
    breed = models.CharField(blank=False, max_length=255)
    date_added = models.DateField(auto_now_add=True)
    date_of_birth = models.DateField()
    description = models.TextField()
    dispositions = models.ManyToManyField(AnimalDisposition)
    gender = models.CharField(max_length=255, choices=Gender.choices)
    kind = models.CharField(max_length=255, choices=Kind.choices)
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=255, choices=Status.choices)

    class Meta:
        ordering = ["-date_added"]

    def __str__(self):
        return self.name


class AnimalPhotograph(models.Model):
    animal = models.ForeignKey(
        Animal, on_delete=models.CASCADE, related_name="photographs"
    )
    url = models.URLField()

    def __str__(self):
        return self.url


class AnimalStatusUpdate(models.Model):
    animal = models.ForeignKey(
        Animal, on_delete=models.CASCADE, related_name="status_updates"
    )
    created = models.DateTimeField(auto_now_add=True)
    status_text = models.TextField()

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return self.status_text
