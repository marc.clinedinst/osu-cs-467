from django.contrib import admin

from .models import Animal, AnimalDisposition, AnimalPhotograph, AnimalStatusUpdate

admin.site.register(Animal)
admin.site.register(AnimalDisposition)
admin.site.register(AnimalPhotograph)
admin.site.register(AnimalStatusUpdate)
