from django.db import models


class Gender(models.TextChoices):
    FEMALE = "Female"
    MALE = "Male"


class Kind(models.TextChoices):
    CAT = "Cat"
    DOG = "Dog"
    OTHER = "Other"


class Status(models.TextChoices):
    ADOPTED = "Adopted"
    AVAILABLE = "Available"
    IN_FOSTER = "In Foster"
    PENDING = "Pending"
