from django.conf import settings
from django.core.mail import get_connection, EmailMultiAlternatives
from django.template.defaultfilters import slugify
from django.template.loader import get_template
from graphql import GraphQLError

from .enums import Gender, Kind, Status
from .models import AnimalDisposition


def check_animal_data_for_errors(
    breed,
    description,
    dispositions,
    gender,
    kind,
    name,
    status,
):
    for value in [
        (breed, "breed"),
        (description, "description"),
        (gender, "gender"),
        (kind, "kind"),
        (name, "name"),
        (status, "status"),
    ]:
        value_is_not_blank_or_throw_graphql_error(*value)

    for value in [
        (gender, Gender.values, "gender", "genders"),
        (kind, Kind.values, "kind", "kinds"),
        (status, Status.values, "status", "statuses"),
    ]:
        value_is_accepted_or_throw_graphql_error(*value)

    for disposition in dispositions:
        try:
            int(disposition)
        except ValueError:
            raise GraphQLError(
                f"Received a non-integer value for disposition: '{disposition}'"
            )

        if not AnimalDisposition.objects.filter(pk=disposition):
            raise GraphQLError(f"A disposition with id {disposition} does not exist.")


def get_accepted_values_string(values):
    return ", ".join([f"'{value}'" for value in values])


def sanitize_file_name(file_name):
    parts = file_name.split(".")
    extension = parts.pop()
    name = "".join(parts)

    return f"{slugify(name)}.{extension}"


def send_new_animal_email(animal, users):
    connection = get_connection(fail_silently=False, password=None, username=None)

    if settings.DEBUG:
        animal_url = f"http://localhost:3001/animal/{animal.id}"
    else:
        animal_url = f"https://www.team-pawswipe.com/animal/{animal.id}"

    emails = []
    html_template = get_template("animals/new_animal.html")
    text_template = get_template("animals/new_animal.txt")

    for user in users:
        email_data = {"animal": animal, "animal_url": animal_url, "user": user}
        email = EmailMultiAlternatives(
            f"New {animal.kind} - {animal.name}",
            text_template.render(email_data),
            "clinedim@oregonstate.edu",
            [user.email],
        )
        email.attach_alternative(html_template.render(email_data), "text/html")
        emails.append(email)

    return connection.send_messages(emails)


def value_is_accepted_or_throw_graphql_error(
    value, accepted_values, value_name, value_name_plural
):
    if value not in accepted_values:
        accepted_values_string = get_accepted_values_string(accepted_values)

        raise GraphQLError(
            f"The provided {value_name} '{value}' is invalid. Accepted {value_name_plural}: {accepted_values_string}"
        )


def value_is_not_blank_or_throw_graphql_error(value, value_name):
    if value == "":
        raise GraphQLError(f"{value_name} cannot be blank.")
