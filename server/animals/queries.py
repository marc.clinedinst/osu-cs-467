import graphene

from datetime import date, timedelta
from graphql import GraphQLError
from graphql_jwt.decorators import login_required

from .enums import Gender, Kind, Status
from .models import Animal, AnimalDisposition, AnimalStatusUpdate
from .types import AnimalType, AnimalDispositionType, AnimalStatusUpdateType
from .utils import value_is_accepted_or_throw_graphql_error


class AnimalQuery(graphene.ObjectType):
    animal_by_id = graphene.Field(AnimalType, id=graphene.ID(required=True))
    animal_dispositions = graphene.List(AnimalDispositionType)
    animal_status_updates = graphene.List(AnimalStatusUpdateType)
    animals = graphene.List(
        AnimalType,
        breed=graphene.String(required=False),
        days_before_today=graphene.Int(required=False),
        dispositions=graphene.List(graphene.ID, required=False),
        gender=graphene.String(required=False),
        kind=graphene.String(required=False),
        status=graphene.String(required=False),
    )

    @login_required
    def resolve_animal_by_id(parent, info, id, **kwargs):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        return animal

    @login_required
    def resolve_animal_dispositions(parent, info, **kwargs):
        return AnimalDisposition.objects.all()

    @login_required
    def resolve_animal_status_updates(parent, info, **kwargs):
        return AnimalStatusUpdate.objects.all()

    @login_required
    def resolve_animals(parent, info, **kwargs):
        animals = Animal.objects.all()

        breed = kwargs.get("breed")
        days_before_today = kwargs.get("days_before_today")
        dispositions = kwargs.get("dispositions")
        gender = kwargs.get("gender")
        kind = kwargs.get("kind")
        status = kwargs.get("status")

        if breed is not None:
            animals = animals.filter(breed__icontains=breed)

        if days_before_today is not None:
            if days_before_today < 0:
                raise GraphQLError(
                    "The value for 'daysBeforeToday' must be greater than or equal to 1."
                )
            elif days_before_today == 0:
                animals = animals.filter(date_added=date.today())
            else:
                animals = animals.filter(
                    date_added__gte=date.today() - timedelta(days=days_before_today)
                )

        if dispositions is not None:
            animals = animals.filter(dispositions__pk__in=dispositions).distinct()

        if gender is not None:
            value_is_accepted_or_throw_graphql_error(
                gender, Gender.values, "gender", "genders"
            )
            animals = animals.filter(gender=gender)

        if kind is not None:
            value_is_accepted_or_throw_graphql_error(kind, Kind.values, "kind", "kinds")
            animals = animals.filter(kind=kind)

        if status is not None:
            value_is_accepted_or_throw_graphql_error(
                status, Status.values, "status", "statuses"
            )
            animals = animals.filter(status=status)

        return animals
