import os
import graphene

from django.contrib.auth import get_user_model
from graphene_file_upload.scalars import Upload
from graphql import GraphQLError
from graphql_jwt.decorators import staff_member_required

from api_project.storage_backends import MediaStorage
from .models import Animal, AnimalPhotograph, AnimalStatusUpdate
from .types import AnimalType, AnimalStatusUpdateType
from .utils import (
    check_animal_data_for_errors,
    sanitize_file_name,
    send_new_animal_email,
)

ACCEPTED_CONTENT_TYPES = ["image/gif", "image/jpeg", "image/png"]

FIVE_MB_FILE_SIZE_LIMIT = 5242880


class AddAnimalStatusUpdate(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        status_text = graphene.String(required=True)

    status_update = graphene.Field(AnimalStatusUpdateType)

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, id, status_text):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        if len(status_text) == 0:
            raise GraphQLError("The value for 'statusText' cannot be blank.")

        status_update = animal.status_updates.create(status_text=status_text)

        return AddAnimalStatusUpdate(status_update=status_update)


class CreateAnimal(graphene.Mutation):
    class Arguments:
        breed = graphene.String(required=True)
        date_of_birth = graphene.Date(required=True)
        description = graphene.String(required=True)
        dispositions = graphene.List(graphene.ID, required=True)
        gender = graphene.String(required=True)
        kind = graphene.String(required=True)
        name = graphene.String(required=True)
        status = graphene.String(required=True)

    animal = graphene.Field(AnimalType)

    @classmethod
    @staff_member_required
    def mutate(
        cls,
        root,
        info,
        breed,
        date_of_birth,
        description,
        dispositions,
        gender,
        kind,
        name,
        status,
    ):
        check_animal_data_for_errors(
            breed, description, dispositions, gender, kind, name, status
        )

        animal = Animal.objects.create(
            breed=breed,
            date_of_birth=date_of_birth,
            description=description,
            gender=gender,
            kind=kind,
            name=name,
            status=status,
        )

        for disposition in dispositions:
            animal.dispositions.add(disposition)

        users = get_user_model().objects.filter(email_opt_in=True)
        send_new_animal_email(animal, users)

        return CreateAnimal(animal=animal)


class DeleteAnimal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    id = graphene.ID()

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, id):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        animal.delete()

        return DeleteAnimal(id=id)


class DeleteAnimalPhotograph(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    id = graphene.ID()

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, id):
        try:
            photograph = AnimalPhotograph.objects.get(pk=id)
        except AnimalPhotograph.DoesNotExist:
            raise GraphQLError(f"Animal photograph with ID {id} does not exist.")

        media_storage = MediaStorage()
        s3_path = "/".join(photograph.url.split("/")[-2:])
        media_storage.delete(s3_path)

        photograph.delete()

        return DeleteAnimalPhotograph(id=id)


class DeleteAnimalStatusUpdate(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    id = graphene.ID()

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, id):
        try:
            status_update = AnimalStatusUpdate.objects.get(pk=id)
        except AnimalStatusUpdate.DoesNotExist:
            raise GraphQLError(f"Animal status update with ID {id} does not exist.")

        status_update.delete()

        return DeleteAnimalStatusUpdate(id=id)


class UpdateAnimal(graphene.Mutation):
    class Arguments:
        breed = graphene.String(required=True)
        date_of_birth = graphene.Date(required=True)
        description = graphene.String(required=True)
        dispositions = graphene.List(graphene.ID, required=True)
        gender = graphene.String(required=True)
        id = graphene.ID(required=True)
        kind = graphene.String(required=True)
        name = graphene.String(required=True)
        status = graphene.String(required=True)

    animal = graphene.Field(AnimalType)

    @classmethod
    @staff_member_required
    def mutate(
        cls,
        root,
        info,
        breed,
        date_of_birth,
        description,
        dispositions,
        gender,
        id,
        kind,
        name,
        status,
    ):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        check_animal_data_for_errors(
            breed, description, dispositions, gender, kind, name, status
        )

        animal.breed = breed
        animal.date_of_birth = date_of_birth
        animal.description = description
        animal.gender = gender
        animal.kind = kind
        animal.name = name
        animal.status = status
        animal.save()

        animal.dispositions.clear()

        for disposition in dispositions:
            animal.dispositions.add(disposition)

        return UpdateAnimal(animal=animal)


class UpdateAnimalStatusUpdate(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        status_text = graphene.String(required=True)

    status_update = graphene.Field(AnimalStatusUpdateType)

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, id, status_text):
        try:
            status_update = AnimalStatusUpdate.objects.get(pk=id)
        except AnimalStatusUpdate.DoesNotExist:
            raise GraphQLError(f"Animal status update with ID {id} does not exist.")

        if len(status_text) == 0:
            raise GraphQLError("The value for 'statusText' cannot be blank.")

        status_update.status_text = status_text
        status_update.save()

        return UpdateAnimalStatusUpdate(status_update=status_update)


class UploadAnimalPhotographMutation(graphene.Mutation):
    class Arguments:
        file = Upload(required=True)
        id = graphene.ID(required=True)

    url = graphene.String()

    @classmethod
    @staff_member_required
    def mutate(cls, root, info, file, id):
        try:
            animal = Animal.objects.get(pk=id)
        except Animal.DoesNotExist:
            raise GraphQLError(f"Animal with ID {id} does not exist.")

        if file.content_type not in ACCEPTED_CONTENT_TYPES:
            accepted_content_types_string = " ".join(ACCEPTED_CONTENT_TYPES)

            raise GraphQLError(
                f"Invalid content type: {file.content_type}. Accepted content types: {accepted_content_types_string}"
            )

        if file.size > FIVE_MB_FILE_SIZE_LIMIT:
            raise GraphQLError("Your image exceeds the file size limit of 5 MB.")

        file_name = sanitize_file_name(file.name)
        file_directory_within_bucket = "photographs"
        file_path_within_bucket = os.path.join(file_directory_within_bucket, file_name)
        media_storage = MediaStorage()
        s3_path = media_storage.save(file_path_within_bucket, file)
        s3_url = media_storage.url(s3_path)

        animal.photographs.create(url=s3_url)

        return UploadAnimalPhotographMutation(url=s3_url)


class AnimalCRUDMutation(graphene.ObjectType):
    add_animal_status_update = AddAnimalStatusUpdate.Field()
    create_animal = CreateAnimal.Field()
    delete_animal = DeleteAnimal.Field()
    delete_animal_photograph = DeleteAnimalPhotograph.Field()
    delete_animal_status_update = DeleteAnimalStatusUpdate.Field()
    update_animal = UpdateAnimal.Field()
    upload_animal_photograph = UploadAnimalPhotographMutation.Field()
    update_animal_status_update = UpdateAnimalStatusUpdate.Field()
